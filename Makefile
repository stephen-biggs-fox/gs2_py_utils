test-fast: test-3d-scan test-single-file test-data-utils test-transform
	@echo "Fast tests done"
	@echo ""
	@echo ""

test-slow: test-3d-scan-theta0 test-2d-scan test-2-scans test-1-scan test-plotting
	@echo "Slow tests done"
	@echo ""
	@echo ""

test-all: test-fast test-slow
	@echo "All tests done"
	@echo ""
	@echo ""

test-3d-scan-theta0:
	@echo "Testing theta0 3D scan..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils.testGs2pyutils3dScanTheta0
	@echo ""
	@echo "Theta0 3D scan test done!"
	@echo ""
	@echo ""

test-3d-scan:
	@echo "Testing 3D scan..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils.testGs2pyutils3dScan
	@echo ""
	@echo "3D scan test done!"
	@echo ""
	@echo ""

test-2d-scan:
	@echo "Testing 2D scan..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils.testGs2pyutilsXpScan
	@echo ""
	@echo "2D scan test done!"
	@echo ""
	@echo ""

test-2-scans:
	@echo "Testing 2 scans..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils.testGs2pyutilsTwoScans
	@echo ""
	@echo "2 scan test done!"
	@echo ""
	@echo ""

test-1-scan:
	@echo "Testing 1 scan..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils.testGs2pyutilsOneScan
	@echo ""
	@echo "1 scan test done!"
	@echo ""
	@echo ""

test-single-file:
	@echo "Testing single file..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils.testGs2pyutilsSingleFile
	@echo ""
	@echo "Single file test done!"
	@echo ""
	@echo ""

test-plotting:
	@echo "Testing plotting..."
	@echo ""
	-python3 -m unittest tests.test_gs2pyutils_plotting
	@echo ""
	@echo "Plotting test done!"
	@echo ""
	@echo ""

test-data-utils:
	@echo "Testing data utils..."
	@echo ""
	-python3 -m unittest tests.test_gs2_data_utils
	@echo ""
	@echo "Data utils test done!"
	@echo ""
	@echo ""

test-transform:
	@echo "Testing Gs2CoordinateTransform..."
	@echo ""
	-python3 -m unittest tests.test_Gs2CoordinateTransform
	@echo ""
	@echo "Data utils test done!"
	@echo ""
	@echo ""



clean:
	find -name '*.pyc' -exec rm {} \;

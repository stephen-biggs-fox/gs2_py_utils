#! /usr/bin/python
"""
Downsamples NetCDF data in time from command line

Copyright 2018 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import sys
import getopt
import gs2_data_utils as dat


def main(argv):
    # Check nargs
    narg = len(argv)
    if narg < 2 or narg > 4:
        usage(argv)
        sys.exit(2)
    # Parse args
    try:
        opts, args = getopt.getopt(argv, 'h', 'help')
    except getopt.GetoptError:
        usage(argv)
        sys.exit(2)
    # Process opts
    for opt, arg in opts:
        if opt in ['-h', '--help']:
            usage(argv)
            sys.exit()
    # Further checks and parsing
    if narg < 3:
        usage(argv)
        sys.exit(2)
    infile = argv[1]
    outfile = argv[2]
    if narg == 4:
        skip = argv[3]
    else:
        skip = None
    # Call function
    if skip is None:
        dat.downsample_and_save(infile, outfile)
    else:
        dat.downsample_and_save(infile, outfile, skip)


def usage(argv):
    print "usage: " + argv[0] + " infile outfile [skip]"


if __name__ == "__main__":
    main(sys.argv)

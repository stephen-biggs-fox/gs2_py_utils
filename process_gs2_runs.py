"""
Python script to plot growth rate and frequency from GS2 data

Copyright 2016 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

# Import general stuff from numpy
from numpy import array

# Import ncdf2dict so data can be read
from ncdf2dict import ncdf2dict

# Import modules to loop over files
from re import compile

# Import relevant functions from myutils
from snbf_py_utils.general import preallocate_based_on
from snbf_py_utils.general import extract_subarrays_via_logical_index

# Import relevant functions from gs2pyutils
from gs2_data_utils import find_results_files
from gs2_data_utils import extract_time, extract_potential_squared
from gs2_data_utils import extract_frequency_and_growth_rate

from gs2_analysis_utils import measure_frequency, measure_growth_rate

from gs2_plot_utils import plot_frequency, plot_growth_rate


# ---------------------------- Functions -------------------------------------

def process_and_make_standard_plots():
    'Executes all the subfunctions in this module in the standard way'
    indie_var_names, indie_var_vals, growth_rate, growth_rate_error, \
                frequency, frequency_error = process()
    plot_variation_with_input_parameter(indie_var_names, indie_var_vals,
            growth_rate, growth_rate_error, frequency, frequency_error)

def process():
    'Finds and processes GS2 data files (but does not plot).'
    files, indie_var_names, indie_var_vals, growth_rate, growth_rate_error, \
            frequency, frequency_error = initialise()
    for i, datafile in enumerate(files):
        growth_rate[i], growth_rate_error[i], frequency[i], frequency_error[i] = \
                analyse_file(datafile, indie_var_names[i], indie_var_vals[i])
    return indie_var_names, indie_var_vals, growth_rate, growth_rate_error, \
            frequency, frequency_error

def initialise():
    'Finds files, sets up arrays, etc.'
    files = find_results_files()
    files, indie_var_names, indie_var_vals = sort_files(files)
    growth_rate, growth_rate_error, frequency, frequency_error = \
            preallocate_arrays_based_on(files)
    print_results(header=True)
    return files, indie_var_names, indie_var_vals, growth_rate, \
            growth_rate_error, frequency, frequency_error

def sort_files(unsorted_files):
    """Sorts data files such that the final order is sorted by independent
    variable names then sub-sorted by independent variable value"""
    unsorted_names, unsorted_vals = extract_file_names_and_values(
            unsorted_files)
    unique_names = set(unsorted_names)
    sorted_files = preallocate_based_on(unsorted_files)
    sorted_names = preallocate_based_on(unsorted_files)
    sorted_vals = preallocate_based_on(unsorted_files)
    i = 0
    for name in unique_names:
        logical_index = array(unsorted_names) == name
        matching_files, matching_names, matching_vals = \
                extract_subarrays_via_logical_index(
                        [unsorted_files, unsorted_names, unsorted_vals],
                        logical_index)
        sort = sorted(zip(matching_vals, matching_files, matching_names))
        sorted_files[i:i+len(matching_files)] = [f for (v, f, n) in sort]
        sorted_names[i:i+len(matching_names)] = [n for (v, f, n) in sort]
        sorted_vals[i:i+len(matching_vals)] = [v for (v, f, n) in sort]
        i = i + len(matching_files)
    return sorted_files, sorted_names, sorted_vals

def extract_file_names_and_values(files):
    'Extracts the independent variable names and values from the filenames'
    pattern = compile('([a-z]+)-(\d+(\.\d+)?)\.out\.nc')
    indie_var_names = preallocate_based_on(files)
    indie_var_vals = preallocate_based_on(files)
    for i, datafile in enumerate(files):
        match = pattern.match(datafile)
        indie_var_names[i] = match.group(1)
        indie_var_vals[i] = float(match.group(2))
    return indie_var_names, indie_var_vals

def preallocate_arrays_based_on(files):
    """Preallocates growth rate and frequency value and uncertainty arrays of the
    same size as files"""
    gamma = preallocate_based_on(files)
    gamma_error = preallocate_based_on(files)
    omega = preallocate_based_on(files)
    omega_error = preallocate_based_on(files)
    return gamma, gamma_error, omega, omega_error

def print_results(header=False, results=[]):
    """Prints the results in the format required for this analysis
    Inputs:
    - header: if false or omitted, this function prints a results line; if
        true, this function prints a column headers line
    - results: a list of the results to be printed in the following order:
      mode number, growth rate, growth rate error, growth rate decimal places,
      frequency, frequency error, frequency decimal places
    """
    if header:
        print('variable, variable value, growth rate, +/- growth rate ' + \
                'error, frequency, +/- frequency error')
    else:
        indie_var_name, indie_var_val, growth_rate, growth_rate_error, growth_rate_dps, frequency, \
                frequency_error, frequency_dps = tuple(results)
        print (indie_var_name + ', ' + str(indie_var_val) + ", %." +
                str(growth_rate_dps) + "f, %." + str(growth_rate_dps) +
                "f, %." + str(frequency_dps) + "f, %." + str(frequency_dps) +
                "f") % (growth_rate, growth_rate_error, frequency,
                        frequency_error)

def analyse_file(datafile, indie_var_name=None, indie_var_val=None,
                 supress_printing=False, plot=False):
    'Performs the required analysis for the given datafile'
    time, phi2, omega, gamma = get_data(datafile)
    growth_rate, growth_rate_error, growth_rate_dps, frequency, \
            frequency_error, frequency_dps = take_measurements(
                    time, phi2, omega, gamma, datafile, supress_printing, plot)
    if not supress_printing:
        output_one_file_results(
                indie_var_name, indie_var_val, growth_rate, growth_rate_error,
                growth_rate_dps, frequency, frequency_error, frequency_dps)
    return growth_rate, growth_rate_error, frequency, frequency_error

def get_data(datafile):
    'Gets the data required for this analysis from the given datafile'
    netcdf_data = ncdf2dict(datafile)
    return extract_required_fields(netcdf_data)

def extract_required_fields(netcdf_data):
    'Extracts the fields from netcdf_data that are required for this analysis'
    time = extract_time(netcdf_data)
    phi2 = extract_potential_squared(netcdf_data)
    omega, gamma = extract_frequency_and_growth_rate(netcdf_data)
    return time, phi2, omega, gamma

def take_measurements(time, phi2, omega, gamma, datafile='',
                      supress_printing=False, plot=False):
    'Takes measurements of growth rate and frequency'
    growth_rate, growth_rate_error, growth_rate_dps = \
            measure_growth_rate(
                    time, phi2, gamma, datafile, plot=plot,
                supress_printing=supress_printing)
    frequency, frequency_error, frequency_dps = measure_frequency(
            time, omega, datafile, plot=plot)
    return growth_rate, growth_rate_error, growth_rate_dps, frequency, \
            frequency_error, frequency_dps

def output_one_file_results(indie_var_name, indie_var_val, growth_rate,
        growth_rate_error, growth_rate_dps, frequency, frequency_error,
        frequency_dps):
    'Prints and plots the results of the analysis of one file'
    print_results(results=[indie_var_name, indie_var_val, growth_rate,
        growth_rate_error, growth_rate_dps, frequency, frequency_error,
        frequency_dps])

def plot_variation_with_input_parameter(indie_var_names, indie_var_vals,
        growth_rate, growth_rate_error, frequency, frequency_error):
    'Plots the results of this analysis against the relevant quantities'
    unique_names = set(indie_var_names)
    for name in unique_names:
        logical_index = array(indie_var_names) == name
        matching_vals, matching_gammas, matching_gamma_errs, matching_freqs, \
                matching_freq_errs = extract_subarrays_via_logical_index(
                        [indie_var_vals, growth_rate, growth_rate_error,
                            frequency, frequency_error], logical_index)
        # In the following, it is correct that name is passed twice. The first
        # is for the xlabel and the second is for the graph filename. In this
        # situation, we use the same string for both but in other situations
        # they may need to be different.
        plot_growth_rate(
                matching_vals, matching_gammas, matching_gamma_errs, name, name)
        plot_frequency(
                matching_vals, matching_freqs, matching_freq_errs, name, name)

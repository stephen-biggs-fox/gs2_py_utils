"""
This class represents a generic plasma equilibrium

Copyright 2020 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np
import os
import xarray as xr
from KineticSpecies import KineticSpecies
from snbf_py_utils import general as gen
from snbf_py_utils import physical_constants as phys


class Equilibrium:
    def __init__(
        self,
        folder,
        filename,
        referenceMassAmu=1.0,
        referenceChargeNumber=1.0,
        referenceSurfaceIndex=0,
        radialCoordinateName="r",
    ):
        self.folder = folder
        self.filename = filename
        self.pathToFile = os.path.join(folder, filename)
        self.referenceMassAmu = referenceMassAmu
        self.referenceChargeNumber = referenceChargeNumber
        self.referenceSurfaceIndex = referenceSurfaceIndex
        self.radialCoordinateName = radialCoordinateName
        self._referenceSpecies = None
        self._tmp = os.path.join(folder, "tmp.nc")
        self._modified = False

    def setReferenceSurfaceIndex(self, referenceSurfaceIndex):
        self.referenceSurfaceIndex = referenceSurfaceIndex
        self._create("nref", force=True)  # nref = n(x = 0)
        self._create("tref", force=True)  # tref = T(x = 0)
        self._create("vref", force=True)  # vref depends on tref
        self._create("rhoref", force=True)  # rhoref depends on vref
        self._create("rhostar", force=True)  # rhostar depends on rhoref
        self._create("vnewk_e", force=True)  # vnewk depends on vref
        self._create("vnewk_i", force=True)  # vnewk depends on vref
        self._create("beta", force=True)  # beta depends on nref and tref
        self._create("beta_prime", force=True)  # beta_prime depends on beta
        self._create("T_norm", force=True)  # T_norm depends on tref
        self._create("n_norm", force=True)  # n_norm depends on nref

    def setReferenceMassAmu(self, referenceMassAmu):
        self.referenceMassAmu = referenceMassAmu
        self._create("mref", force=True)
        self._create("vref", force=True)  # vref depends on mref
        self._create("rhoref", force=True)  # rhoref depends on vref and mref
        self._create("rhostar", force=True)  # rhostar depends on rhoref
        self._create("vnewk_e", force=True)  # vnewk depends on vref
        self._create("vnewk_i", force=True)  # vnewk depends on vref

    def setReferenceChargeNumber(self, referenceChargeNumber):
        self.referenceChargeNumber = referenceChargeNumber
        self._create("zref", force=True)
        self._create("rhoref", force=True)  # rhoref depends on zref
        self._create("rhostar", force=True)  # rhostar depends on rhoref

    def setRadialCoordinate(self, radialCoordinateName):
        self.radialCoordinateName = radialCoordinateName
        self._create("rhoc", force=True)
        self._create("tprim", force=True)  # tprim depends on rhoc
        self._create("fprim", force=True)  # fprim depends on rhoc
        self._create("s_hat", force=True)  # s_hat depends on rhoc
        self._create("beta_prime", force=True)  # beta_prime depends on rhoc

    def _compute(self, quantityNames):
        for name in quantityNames:
            print("Computing {}...".format(name))
            self._create(name)
            # Save after each one in case performance is slow and/or there is a
            # crash or something
            if self._modified:
                self._save()

    def _create(self, quantityName, force=False):
        """
        Creates a DataArray if it does not already exist in the equilibrium
        Inputs:
            - quantityName - string indicating the name of the DataArray and
                the key to this DataArray in the Dataset. The data in the
                DataArray is computed using the method
                self._compute_<quantityName>
        """
        # If not already in Dataset, then compute and add to Dataset
        if force or quantityName not in self.dataset.keys():
            quantity = getattr(self, "_compute_" + quantityName)()
            quantity.name = quantityName
            self._addToDataset(quantity)

    def _compute_r(self):
        "Compute minor radius of each surface"
        r = self._newDataArrayFunctionOfPsi()
        # Start from surface 1 as 0 is axis which has r = 0.0 by definition
        for surfaceIndex in range(1, self.npsi):
            # The following could be in-lined but is kept separate for
            # readability
            R_min_max = self._computeMinMax("R", surfaceIndex)
            r[dict(PSI=surfaceIndex)] = self._computeHalfDistance(*R_min_max)
        return r

    def _newDataArrayFunctionOfPsi(self):
        return xr.DataArray(np.zeros(self.npsi), dims="PSI")

    def _computeMinMax(self, quantityName, surfaceIndex):
        """
        Compute the minimum and maximum values of a quantity
        Inputs:
            - quantityName - string indicating the quantity to use
            - surfaceIndex - integer indicating the surface to use
        Notes:
            - This assumes that quantityName is a function of CHI on the
                surface.
            - This uses a spline fit to get f(CHI) then find the roots of the
                spline's derivatives.
            - This assumes there is a single global maximum and a single global
                minimum.
            - Extrema on the boundary are found by assuming that f(x) is 2 pi
                periodic and wrapping around if necessary.
        """
        # Try to find two extrema from data as is.  If necessary, repeat first
        # two values to emulate periodicity so that we find the maximum on the
        # boundary.
        quantity = self.dataset[quantityName][dict(PSI=surfaceIndex)]
        try:
            minMax = gen.get_extrema(self.CHI, quantity)
            assert len(minMax) == 2
        except (AssertionError):
            minMax = gen.get_extrema(
                np.concatenate((self.CHI, self.CHI[dict(CHI=slice(0, 2))] + 2 * np.pi)),
                np.concatenate((quantity, quantity[dict(CHI=slice(0, 2))])),
            )
            assert len(minMax) == 2
        return minMax

    def _compute_x(self):
        "Compute the radial coordinate with 0 on the reference surface"
        self._create("r")
        self._create("r0")
        self._create("lref")
        return (self.r - self.r0) / self.lref

    def _compute_r0(self):
        "Compute the reference surface radius"
        self._create("r")
        return self.r[dict(PSI=self.referenceSurfaceIndex)]

    def _compute_lref(self):
        "Compute the reference length in m"
        self._create("r")
        return self.r[dict(PSI=-1)]

    def _compute_mref(self):
        "Compute the reference mass in amu"
        return xr.DataArray(self.referenceMassAmu)

    def _compute_nref(self):
        "Compute the reference density in m^(-3)"
        return self.NE[dict(PSI=self.referenceSurfaceIndex)]

    def _compute_rhoref(self):
        "Compute the reference Larmor radius in m"
        self._create("vref")
        self._create("mref")
        self._create("zref")
        self._create("bref")
        return self.vref * self.mref * phys.amu / (self.zref * phys.e * self.bref)

    def _compute_tref(self):
        "Compute the reference temperature in eV"
        return self.TE[dict(PSI=self.referenceSurfaceIndex)]

    def _compute_vref(self):
        "Compute the reference (thermal) velocity in m/s"
        self._createReferenceSpecies()
        return self._referenceSpecies.compute_thermal_velocity()

    def _createReferenceSpecies(self):
        self._create("mref")
        self._create("nref")
        self._create("tref")
        self._create("zref")
        self._referenceSpecies = KineticSpecies(
            self.tref,
            self.nref,
            mass_kg=(self.mref * phys.amu),
            proton_number=self.zref,
        )

    def _compute_zref(self):
        "Compute the reference charge in units of elementary charge"
        return xr.DataArray(self.referenceChargeNumber)

    def _compute_beta(self):
        self._create("tref")
        self._create("nref")
        self._create("bref")
        return 2.0 * phys.mu_0 * self.tref * phys.e * self.nref / self.bref ** 2

    def _compute_beta_prime(self):
        self._create("beta")
        self._create("nspecies")
        self._create("rhoc")
        self._create("p")
        # beta is single species but p is total, hence factor of nspecies
        return self.nspecies * self.beta * (1 / self.p) * np.gradient(self.p, self.rhoc)

    def _compute_rhoc(self):
        if self.radialCoordinateName == "r":
            self._create("r_norm")
            rhoc = self.r_norm.copy()  # Copy so we can rename independently
        else:
            raise NotImplementedError(
                "Radial coordinates other than 'r' not implemented yet"
            )
        return rhoc

    def _compute_r_norm(self):
        self._create("r")
        self._create("lref")
        return self.r / self.lref

    def _compute_s_hat(self):
        self._create("rhoc")
        self._create("q")
        # Doesn't matter if this uses r or r_norm, so long as both are the same
        return (self.rhoc / self.q) * np.gradient(self.q, self.rhoc)

    def _compute_tprim(self):
        # Assumes all species have the same tprim based on whatever T is
        self._create("T")
        self._create("rhoc")
        return -(1 / self.T) * np.gradient(self.T, self.rhoc)

    def _compute_T(self):
        # Assumes all species have the same T based on TE
        return self.dataset["TE"].copy()

    def _compute_fprim(self):
        if (self.DNEDPSI == 0).all():
            fprim = self._newDataArrayFunctionOfPsi()
        else:
            self._create("n")
            self._create("rhoc")
            fprim = -(1 / self.n) * np.gradient(self.n, self.rhoc)
        return fprim

    def _compute_n(self):
        # Assumes all species have the same n based on NE
        return self.dataset["NE"].copy()

    def _compute_rhostar(self):
        self._create("rhoref")
        self._create("lref")
        return self.rhoref / self.lref

    def _compute_T_norm(self):
        self._create("T")
        self._create("tref")
        return self.T / self.tref

    def _compute_n_norm(self):
        self._create("n")
        self._create("nref")
        return self.n / self.nref

    def _compute_vnewk_e(self):
        return self._compute_vnewk(phys.m_e, -1)

    def _compute_vnewk_i(self):
        # Assumes deuterium plasma - needs generalising for other cases
        return self._compute_vnewk(phys.m_d, 1)

    def _compute_vnewk(self, mass_kg, chargeNumber):
        self._create("T")
        self._create("n")
        self._create("vref")
        self._create("lref")
        vnewk = self._newDataArrayFunctionOfPsi()
        for surfaceIndex in range(self.npsi):
            kineticSpecies = KineticSpecies(
                self.T[dict(PSI=surfaceIndex)],
                self.n[dict(PSI=surfaceIndex)],
                mass_kg=mass_kg,
                proton_number=chargeNumber,
            )
            vnewk[
                dict(PSI=surfaceIndex)
            ] = kineticSpecies.normalise_collision_frequency(self.vref, self.lref)
        return vnewk

    def _addToDataset(self, dataArray):
        self.dataset[dataArray.name] = dataArray
        # Set attribute *from dataset*. This is necessary to get correct
        # coordinates.
        setattr(self, dataArray.name, self.dataset[dataArray.name])
        self._modified = True

    def _save(self):
        # Prepare encoding to supress messy NetCDF output
        encoding = {}
        for name in self.dataset.variables.keys():
            encoding[name] = {}
            encoding[name]["_FillValue"] = False
        self.dataset.to_netcdf(self._tmp, encoding=encoding)
        os.rename(self._tmp, self.pathToFile)
        self._modified = False

    def _computeHalfDistance(self, a, b):
        return np.abs(a - b) / 2

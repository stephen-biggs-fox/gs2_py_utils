"""
Python module containing utility functions for extracting GS2 data

Copyright 2016 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

from fnmatch import fnmatch
import operator
from os import walk
from os.path import join
import numpy as np

import snbf_py_utils.general as gen
from ncdf2dict import ncdf2dict as nc


def load(folder='.'):
    'Loads data from current directory'
    return nc(folder + '/input.out.nc')


def load_eig():
    'Loads eigensolver data from current directory'
    return load_eig_data()


def load_eig_data():
    'Loads eigensolver data from current directory'
    return nc('input_eig.out.nc')


def print_keys(data):
    'Print keys with shapes / lengths / values from supplied ncdf2dict output'
    gen.print_keys(data)


def sort_datafiles(datafiles, i=1):
    """Sorts datafiles based on the independent variable value from
    the ith level of the filepaths. All other levels are sorted in reverse
    level order."""
    # Convert list into a list of lists so that sort keys can be appended to
    # the sub-lists later. Do this into a new variable to avoid modifying the
    # original.
    sorted_datafiles = []
    for ifile in range(len(datafiles)):
        sorted_datafiles.append([datafiles[ifile]])
    # Go through levels from last level to first level, get sort keys
    # (indie_var_vals) and append to sub-lists of sorted_datafiles. Skip the
    # client-requested level as this is done last. Skip level 0 if it is '.'.
    # Skip last level as it is the filename.
    example_split = datafiles[0].split('/')
    nlevels = len(example_split)  # Includes '.' level and filename level
    # Work out if level 0 is '.'
    includes_dot_level = example_split[0] == '.'
    if includes_dot_level:
        # stop on 0 to ignore '.' level
        stop = 0
    else:
        # stop on -1 to include 0th level
        stop = -1
    # -2 to ignore filename level, step of -1 to sort farthest-right level
    # first
    range_to_use = range(nlevels - 2, stop, -1)
    for ilevel in range_to_use:
        if not ilevel == i:
            _append_sortkey(sorted_datafiles, ilevel)
    # Finally, do the above for the client-requested level
    _append_sortkey(sorted_datafiles, i)
    if not includes_dot_level:
        # Increment sort colunm indexes by 1 because 0th is the filename itself
        range_to_use = np.array(range_to_use)
        range_to_use += 1
    sorted_datafiles.sort(
        key=operator.itemgetter(*range_to_use))
    # Tidy-up by extracting the values without the keys
    sorted_datafiles = _extract_values_without_keys(sorted_datafiles)
    # Get sorted indie_var_names and indie_var_vals for backward compatibility
    dummy, indie_var_names, indie_var_vals = _old_sort(datafiles, i=i)
    return sorted_datafiles, indie_var_names, indie_var_vals


def _append_sortkey(datafiles_with_sort_keys, ilevel):
    datafiles = _extract_values_without_keys(datafiles_with_sort_keys)
    indie_var_names, indie_var_vals = extract_indie_vars(datafiles,
                                                         i=ilevel)
    for ifile in range(len(datafiles_with_sort_keys)):
        datafiles_with_sort_keys[ifile].append(indie_var_vals[ifile])


def _extract_values_without_keys(datafiles_with_sort_keys):
    datafiles = []
    for ifile in range(len(datafiles_with_sort_keys)):
        datafiles.append(datafiles_with_sort_keys[ifile][0])
    return datafiles


def _old_sort(datafiles, i=1):
    """This is how sorting used to be done which doesn't work in some cases,
    hence this has been replaced by the new version of sort_datafiles that uses
    _append_sortkey. However, this version is retained so that the sorted
    indie_var_names and indie_var_vals can still be returned for backward
    compatibility"""
    indie_var_names, indie_var_vals = extract_indie_vars(datafiles, i=i)
    datafiles = [x for _, x in sorted(zip(indie_var_vals, datafiles))]
    indie_var_names = [x for _, x in sorted(zip(indie_var_vals,
                                                indie_var_names))]
    indie_var_vals.sort()
    return datafiles, indie_var_names, indie_var_vals


def extract_indie_vars(datafiles, i=1):
    """Extracts the independent variable value from the ith level of
    the filepath of each datafile assuming the format
    ./name1_val1/name2_val2/ etc."""
    indie_var_vals = []
    indie_var_names = []
    for f in datafiles:
        name, val = extract_indie_var(f, i=i)
        indie_var_names.append(name)
        indie_var_vals.append(val)
    return indie_var_names, indie_var_vals


def extract_indie_var(filepath, i=1):
    """Extracts the independent variable value from the ith level of
    filepath assuming the format ./name1_val1/name2_val2/ etc."""
    path_level_i = filepath.split('/')[i]  # ith level of filepath
    # 0 is name and 1 is value
    name = path_level_i.split('_')[0]
    val_string = path_level_i.split('_')[1]
    return name, float(val_string)


def find_results_files(search_dir='.', followlinks=False):
    'Finds GS2 results files assuming one directory per run'
    return find_files("*.out.nc", search_dir, followlinks)


def find_eig_files(search_dir='.', followlinks=False):
    'Finds eigensolver data files assuming one directory per run'
    return find_files("*_eig.out.nc", search_dir, followlinks)


def find_files(pattern, search_dir='.', followlinks=False):
    'Finds data files assuming one directory per run'
    datafiles = []
    for pathname, subdir, files in walk(search_dir, followlinks=followlinks):
        for filename in files:
            if fnmatch(filename, pattern):
                datafiles.append(join(pathname, filename))
    return datafiles


def find_netcdf_files():
    'Finds all GS2 output data files in the current directory'
    return gen.find_files_matching_pattern('*.out.nc')


def extract_time(data):
    'Extracts time from GS2 data'
    return data['t']


def extract_potential_squared(data):
    'Extracts potential squared from GS2 data'
    return data['phi2']


def extract_complex_frequency(data):
    """Extracts complex frequency (big_omega) from GS2 data as an array of
    complex numbers
    Outputs:
    - big_omega: complex frequency (frequency + 1j * growth rate)
    """
    return data['omega']


def extract_frequency_and_growth_rate(data):
    """Extracts complex frequency (big_omega) from GS2 data as arrays of real
    (frequency) and imaginary (growth rate) components
    Outputs:
    - omega: frequency
    - gamma: growth rate
    """
    big_omega = extract_complex_frequency(data)
    omega = gen.preallocate_based_on(big_omega)
    gamma = gen.preallocate_based_on(big_omega)
    for i in range(0, len(big_omega)):
        omega[i] = big_omega[i][0][0].real
        gamma[i] = big_omega[i][0][0].imag
    return omega, gamma


def extract_potential(data):
    """Extracts potential from GS2 data. NB: Assumes 1st eigenmode for
    eigensolve data"""
    phi = data['phi']
    ndim = len(phi.shape)
    if ndim == 3:  # i.e. initial value solver data
        phi = phi[0][0]  # [0][0] extracts 3rd dimension
    elif ndim == 2:  # i.e. eigensolver data
        phi = phi[0]  # [0] extracts 2nd dimension
    else:
        raise ValueError('len(data[\'phi\'].shape) must be 3 ' +
                         '(initial value solver data) or 2 ' +
                         '(eigensolver data)')
    return phi


def extract_field_line_coordinate(data):
    'Extracts field line coordinate from GS2 data'
    return data['theta']


def downsample(infile, skip=10):
    """
    Downsamples *in time* GS2 NetCDF data
        useful e.g. if the raw file is too big
        NB: downsampling in time assumes data contains time data in a
            field called 't'
    infile = string path and filename of NetCDF data file to be
        downsampled
    skip = number of data points to skip each iteration, i.e. the
        following indices are retained: 0, 0 + skip, 0 + 2  * skip, ...
    returns: downsampled data as a dict (i.e. as if it had been loaded
        from a NetCDF file)
    """
    data = nc(infile)
    # Return as new dict so clients can still access the old dict if necessary
    new_data = {}
    nt = len(data['t'])
    for k in data.keys():
        old_arr = data[k]
        if not hasattr(old_arr, 'shape') or nt not in old_arr.shape:
            # No shape, therefore not a function of anything (i.e. is
            # a scalar), OR has shape but not a function of time.
            # Either way, not a function of time so skip it.
            continue
        # If we haven't continued to the next key, then process the downsample
        new_arr = process_downsampling(old_arr, skip, nt)
        new_data[k] = new_arr
    return new_data


def assert_time_is_axis_0(old_arr, nt):
    # Determine which axis is to be downsampled
    # Convert to array so that np.where actually works
    axis = np.where(np.array(old_arr.shape) == nt)[0][0]
    # Check assumption (assuming this simplifies later calcs)
    assert axis == 0


def compute_downsampled_indices(old_len, skip):
    new_len = old_len / skip + 1
    indices = np.ndarray(new_len, dtype=int)
    old_ind = 0
    for new_ind in range(new_len):
        indices[new_ind] = old_ind
        old_ind += skip
    return new_len, indices


def process_downsampling(old_arr, skip, nt):
    assert_time_is_axis_0(old_arr, nt)
    # Compute downsampled length and downsampling indices
    new_len, indices = compute_downsampled_indices(nt, skip)
    # Update shape
    new_shape = np.array(old_arr.shape)
    new_shape[0] = new_len  # Assuming axis == 0
    new_arr = perform_downsampling(old_arr, new_shape, indices)
    return new_arr


def perform_downsampling(old_arr, new_shape, indices):
    # Pre-allocate new array
    new_arr = np.ndarray(new_shape, dtype=old_arr.dtype)
    # If-elif block to take appropriate action
    if old_arr.ndim == 1:
        for new_ind, old_ind in enumerate(indices):
            new_arr[new_ind] = old_arr[old_ind]
    elif old_arr.ndim == 2:
        for new_ind, old_ind in enumerate(indices):
            # Assuming axis == 0
            new_arr[new_ind, :] = old_arr[old_ind, :]
    elif old_arr.ndim == 3:
        for new_ind, old_ind in enumerate(indices):
            # Assuming axis == 0
            new_arr[new_ind, :, :] = old_arr[old_ind, :, :]
    elif old_arr.ndim == 4:
        for new_ind, old_ind in enumerate(indices):
            # Assuming axis == 0
            new_arr[new_ind, :, :, :] = old_arr[
                old_ind, :, :, :]
    else:
        raise(NotImplementedError)
    return new_arr


def save_obj(obj, filename):
    'Allows saving of objects, e.g. downsampled data'
    gen.save_obj(obj, filename)


def load_obj(filename):
    'Allows loading of objects, e.g. saved downsampled data'
    return gen.load_obj(filename)


def downsample_and_save(infile, outfile, skip=10):
    data = downsample(infile, skip)
    save_obj(data, outfile)


def ri_to_complex(array):
    """Converts a GS2 array where the last dimension is "ri" (real-imaginary)
    to a complex array
    Inputs:
    - array - the array to be converted
    NB: It is the responsibility of the user to ensure that the last dimension
    is "ri" - no checks are done by this function"""
    output = np.ndarray(array.shape[:-1], dtype=complex)
    output.real = array[..., 0]
    output.imag = array[..., 1]
    return output

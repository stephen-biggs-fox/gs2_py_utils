"""
Public API for gs2pyutils

Copyright 2017 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

from snbf_py_utils.ContourPlotAnimator import ContourPlotAnimator
import gs2_data_utils as dat
import process_gs2_runs as pgr
from IPython.display import display, HTML
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import re
import seaborn as sns
import gs2_axes_labels as gal
from snbf_py_utils import plotting as mpu


np.set_printoptions(precision=15)
sns.set(font_scale=2)


def get(data_name, search_dir, reflect=True, eig=True, debug=False,
        followlinks=False):
    # Ensure search_dir ends in / to prevent error for scans
    search_dir = search_dir if search_dir.endswith('/') else search_dir + '/'
    files, indie_var_vals, indie_var_names = _find_files(search_dir, eig,
                                                         followlinks)
    if debug:
        print("Files: {}".format(files))
    data, names = _get_data(files, data_name, indie_var_vals, indie_var_names,
                            eig)
    data = _assemble_data_structure(data, indie_var_vals, indie_var_names,
                                    names, reflect)
    return {'names': names, 'data': data, 'source': search_dir}


def _find_files(search_dir, eig, followlinks=False):
    if eig:
        files = dat.find_eig_files(search_dir, followlinks)
    else:
        files = dat.find_results_files(search_dir, followlinks)
    files, indie_var_names, indie_var_vals = _sort_files(search_dir, files)
    return files, indie_var_vals, indie_var_names


def _sort_files(dir_name, abs_files):
    if len(abs_files) > 1:
        rel_files = _remove_dir_name_from_paths(dir_name, abs_files)
        indie_var_vals, rel_files, indie_var_names = \
            _sort_on_outer_variable(rel_files)
        _sort_on_inner_variable(rel_files, indie_var_vals, indie_var_names)
        _update_abs_files(dir_name, rel_files, abs_files)
    else:
        indie_var_names = []
        indie_var_vals = []
    return abs_files, indie_var_names, indie_var_vals


def _remove_dir_name_from_paths(dir_name, abs_files):
    if dir_name == './':
        dir_name = r'\./'
    rel_files = []
    for f in abs_files:
        rel_files.append(re.sub(dir_name, '', f))
    return rel_files


def _sort_on_outer_variable(rel_files):
    rel_files, indie_var_names, indie_var_vals = \
        dat.sort_datafiles(rel_files, i=0)
    indie_var_names = [indie_var_names[0]]  # Assume all the same
    indie_var_vals = [np.array(indie_var_vals)]
    return indie_var_vals, rel_files, indie_var_names


def _sort_on_inner_variable(rel_files, indie_var_vals, indie_var_names):
    try:
        ignore, second_var_names, second_var_vals = \
            dat.sort_datafiles(rel_files, i=1)
    except (ValueError, IndexError):
        pass
    if 'second_var_names' in locals():
        # Assume all the same
        indie_var_names.insert(0, second_var_names[0])
        indie_var_vals.insert(0, np.array(second_var_vals))


def _update_abs_files(dir_name, rel_files, abs_files):
    for i, f in enumerate(rel_files):
        abs_files[i] = dir_name + f


def _get_data(files, data_name, indie_var_vals, indie_var_names, eig):
    if len(indie_var_names) > 1:
        # Data array needs to be 2D or 3D so pre-allocate
        indie_var_vals[0] = _get_unique(indie_var_vals[0])
        indie_var_vals[1] = _get_unique(indie_var_vals[1])
        # indie_var_vals[0] is the inner variable, e.g. p, while
        # indie_var_vals[1] is the outer variable, e.g. akappa. We set
        # the inner as y and the outer as x because the shape of the
        # array is then (outer, inner) which makes sense in terms of
        # the way an array might be created in python, i.e. create a
        # load of 1D lists or arrays (the inner ones) and then make a
        # list of them (with the outer length)
        ny = len(indie_var_vals[0])
        nx = len(indie_var_vals[1])
        arr = np.ndarray((nx, ny))
        names, data = fields[data_name](data_name, files, eig, data=arr)
    else:
        names, data = fields[data_name](data_name, files, eig)
    return data, names


def _get_unique(the_list):
    the_list = list(set(the_list))
    the_list.sort()
    return np.array(the_list)


def _get_gamma(data_name, files, eig, data=None):
    data = _get_big_omega(files, eig, data)
    return [data_name], data.imag


def _get_omega(data_name, files, eig, data=None):
    data = _get_big_omega(files, eig, data)
    return [data_name], data.real


def _get_big_omega(files, eig, data=None):
    if data is None:
        data = np.ndarray(len(files), dtype=complex)
        for i, f in enumerate(files):
            if eig:
                data[i] = dat.extract_complex_frequency(dat.nc(f))[0]
            else:
                try:
                    gamma, ignore, omega, ignore = \
                        pgr.analyse_file(f, supress_printing=True)
                except ValueError:
                    gamma = 0.0
                    omega = 0.0
                data[i] = omega + 1j * gamma
    else:
        ix = 0
        iy = 0
        nx, ny = data.shape
        # Re-allocate as complex to store big omega
        data = np.ndarray((nx, ny), dtype=complex)
        for f in files:
            if eig:
                data[ix, iy] = \
                    dat.extract_complex_frequency(dat.nc(f))[0]
            else:
                try:
                    gamma, ignore, omega, ignore = \
                        pgr.analyse_file(f, supress_printing=True)
                except ValueError:
                    gamma = 0.0
                    omega = 0.0
                data[ix, iy] = omega + 1j * gamma
            # Increment inner variable as if we are making the inner
            # lists first and then making a list of lists
            iy += 1
            if iy == ny:
                iy = 0
                ix += 1
    return data


def _get_phi(data_name, files, eig, data=None):
    # Assume all eta are the same
    eta = dat.extract_field_line_coordinate(dat.nc(files[0]))
    if data is None:
        phi = []
        for f in files:
            phi.append(dat.extract_potential(dat.nc(f)))
        phi = np.array(phi)
        shape = phi.shape
        if shape[0] == 1:
            phi = np.reshape(phi, shape[1])
    else:
        ix = 0
        iy = 0
        nx, ny = data.shape
        # Re-allocate as complex to store phi
        phi = np.ndarray((nx, ny, len(eta)), dtype=complex)
        for f in files:
            phi[ix, iy, :] = \
                dat.extract_potential(dat.nc(f))
            # Increment inner variable as if we are making the inner
            # lists first and then making a list of lists
            iy += 1
            if iy == ny:
                iy = 0
                ix += 1
    return [data_name, 'eta'], np.array([phi, eta])


def _assemble_data_structure(data, indie_var_vals, indie_var_names, names,
                             reflect):
    if len(indie_var_names) > 0:
        data = _process_scan(data, indie_var_vals, indie_var_names, names,
                             reflect)
    else:
        data = _process_single_file(data)
    return data


def _process_scan(data, indie_var_vals, indie_var_names, names, reflect):
    _append_indie_var_names(indie_var_names, names)
    data = _reorganise_existing_data(data)  # To make space for indie_var_vals
    _append_indie_var_vals(indie_var_vals, data)
    if reflect:
        _reflect(data, names)
    return data


def _append_indie_var_names(indie_var_names, names):
    for n in indie_var_names:
        names.append(n)


def _reorganise_existing_data(data):
    # In order to append indie_var_vals to the list from
    # fields[data_name], one first has to append the latter to a
    # new list so the two lists are side-by-side rather than
    # nested.
    dep_data = data
    data = []
    shape = dep_data.shape
    if dep_data.ndim > 1 and shape[1] == 1:
        # gamma or omega with extra unnecessary dimension
        data.append(np.reshape(dep_data, shape[0]))
    elif shape[0] == 2:
        # phi and eta in one array so separate
        data.append(dep_data[0])
        data.append(dep_data[1])
    else:
        # gamma or omega already of correct shape
        data.append(dep_data)
    return data


def _append_indie_var_vals(indie_var_vals, data):
    for v in indie_var_vals:
        data.append(v)


def _reflect(data, names):
    for i, name in enumerate(names):
        if name == 'p' or name == 'x' or name == 'theta0':
            data[i], n_old = _reflect_1d(data[i], name,
                                         reflected_data_negative=True)
            # Primary data (gamma, omega or phi) also needs to be
            # reflected. However, phi has an extra dimension so has to
            # be treated differently.
            num_dims = len(names) - 1
            if num_dims == 1:
                data[0], ignore = _reflect_1d(data[0], name)
            elif num_dims > 1:
                data[0] = _reflect_multidim(data[0], num_dims - i, n_old, name,
                                            names[0],
                                            do_reflection_functions[num_dims])


def _reflect_multidim(data_to_reflect, dim, n_old, reflector_name, data_name,
                      reflection_function):
    n, n_minus, num_duplications = \
        _get_derived_lengths(n_old, reflector_name)
    reflected = _prepare_arrays_for_reflection_multidim(data_to_reflect, n,
                                                        dim, data_name)
    reflect_in_eta = data_name == 'phi' and (reflector_name == 'p' or
                                             reflector_name == 'theta0')
    reflection_function(data_to_reflect, n, n_minus, reflected, dim,
                        num_duplications, reflect_in_eta)
    return reflected


def _reflect_1d(data_to_reflect_1d, name, reflected_data_negative=False):
    sign = -1 if reflected_data_negative else 1
    n, n_old, n_minus, num_duplications = \
        _get_lengths(data_to_reflect_1d, name)
    data_to_reflect_1d, reflected = \
        _prepare_arrays_for_reflection_1d(n, data_to_reflect_1d, n_old)
    _do_reflection_1d(data_to_reflect_1d, n, n_minus, reflected, sign,
                      num_duplications)
    reflected = np.reshape(reflected, n)  # Reshape back to 1D
    return reflected, n_old


def _get_lengths(data_to_reflect_1d, name):
    n_old = len(data_to_reflect_1d)
    num_p, np_minus, num_duplications = _get_derived_lengths(n_old, name)
    return num_p, n_old, np_minus, num_duplications


def _get_derived_lengths(n_old, name):
    # Minus num_duplications to avoid duplicating p = 0 and p = pi
    # Call it num_p rather than np to avoid clash with numpy
    # Safe to avoid try, except KeyError as only known names should
    # make it this far anyway
    num_duplications = num_reflected_duplications_to_avoid[name]
    n = 2 * n_old - num_duplications
    n_minus = n - n_old
    return n, n_minus, num_duplications


def _prepare_arrays_for_reflection_multidim(data_to_reflect,
                                            n, dim, data_name):
    # Convert shape (tuple) to list so that item assignment is supported
    shape = list(data_to_reflect.shape)
    shape[dim] = n
    return np.ndarray(shape, dtype=data_to_reflect.dtype)


def _prepare_arrays_for_reflection_1d(n, data_to_reflect_1d, n_old):
    reflected = np.ndarray((1, n))
    # Reshape for use with np.fliplr as it required a 2D array
    data_to_reflect_1d = np.reshape(data_to_reflect_1d, (1, n_old))
    return data_to_reflect_1d, reflected


def _do_reflection_3d(data_to_reflect_3d, n, n_minus, reflected, dim,
                      num_duplications, reflect_in_eta):
    # [1:cutoff] to cut off p = 0 and p = pi or just x = 0
    cutoff = data_to_reflect_3d.shape[dim] - (num_duplications - 1)
    if dim == 0:
        if reflect_in_eta:
            reflected[0:n_minus, :, :] = \
                np.flip(data_to_reflect_3d[1:cutoff, :, ::-1], dim)
        else:
            reflected[0:n_minus, :, :] = \
                np.flip(data_to_reflect_3d[1:cutoff, :, :], dim)
        reflected[n_minus:n, :, :] = data_to_reflect_3d
    elif dim == 1:
        if reflect_in_eta:
            reflected[:, 0:n_minus, :] = \
                np.flip(data_to_reflect_3d[:, 1:cutoff, ::-1], dim)
        else:
            reflected[:, 0:n_minus, :] = \
                np.flip(data_to_reflect_3d[:, 1:cutoff, :], dim)
        reflected[:, n_minus:n, :] = data_to_reflect_3d
    elif dim == 2:
        reflected[:, :, 0:n_minus] = \
            np.flip(data_to_reflect_3d[:, :, 1:cutoff], dim)
        reflected[:, :, n_minus:n] = data_to_reflect_3d


def _do_reflection_2d(data_to_reflect_2d, n, n_minus, reflected, dim,
                      num_duplications, reflect_in_eta):
    # [1:cutoff] to cut off p = 0 and p = pi or just x = 0
    cutoff = data_to_reflect_2d.shape[dim] - (num_duplications - 1)
    if dim == 0:
        if reflect_in_eta:
            reflected[0:n_minus, :] = \
                np.flip(data_to_reflect_2d[1:cutoff, ::-1], dim)
        else:
            reflected[0:n_minus, :] = \
                np.flip(data_to_reflect_2d[1:cutoff, :], dim)
        reflected[n_minus:n, :] = data_to_reflect_2d
    elif dim == 1:
        reflected[:, 0:n_minus] = np.flip(data_to_reflect_2d[:, 1:cutoff], dim)
        reflected[:, n_minus:n] = data_to_reflect_2d


def _do_reflection_1d(data_to_reflect_1d, n, n_minus, reflected, sign,
                      num_duplications):
    # 1:-1 to cut off p = 0 and p = pi
    reflected[:, 0:n_minus] = sign * np.fliplr(data_to_reflect_1d[
        :, 1:data_to_reflect_1d.size - (num_duplications - 1)])
    reflected[:, n_minus:n] = data_to_reflect_1d


def _process_single_file(data):
    if len(data) > 1:
        # phi and eta in one array so separate
        dep_data = data
        data = []
        data.append(dep_data[0])
        data.append(dep_data[1].real)
    return data


def render(data, dir_name=None, show=True, anim_outer=True,
           indicate_marginal=True, eig=True, reflect=True):
    direct_render = _check_parameters(data, dir_name, show, anim_outer,
                                      indicate_marginal)
    if direct_render:
        out = _render(get(data, dir_name, eig=eig, reflect=reflect),
                      anim_outer, indicate_marginal)
    else:
        out = _render(data, anim_outer, indicate_marginal)
    if show:
        if isinstance(out, str):
            print(out)
        elif isinstance(out, mpl.animation.FuncAnimation):
            environ = os.environ['_']
            if environ.endswith('python'):
                plt.show()
            elif environ.endswith('jupyter'):
                display(HTML(out.to_html5_video()))
            else:
                raise EnvironmentError('Unknown execution environment')
        else:
            plt.show()
    else:
        return out


def _check_parameters(data, dir_name, show, anim_outer, indicate_marginal):
    direct_render = isinstance(data, str)
    if direct_render:
        if not isinstance(dir_name, str):
            raise TypeError("dir_name must be specified as a string when "
                            "using direct rendering")
        if not os.path.isdir(dir_name):
            raise ValueError('dir_name must be a valid path')
    else:
        if not isinstance(data, dict):
            raise TypeError('data must be dict or string')
    # TODO - add more checks - for now, will just have to trust user
    return direct_render


def _render(data, anim_outer, indicate_marginal):
    num_dims = len(data['names']) - 1
    scales = _get_scales(data)
    label_fns[num_dims](data, scales, indicate_marginal)
    return show_fns[num_dims](data, scales, anim_outer)


def _print_value(data, scales, anim_outer):
    return data['names'][0] + ' = ' + repr(data['data'][0])


def _plot_line(data, scales, anim_outer):
    return plt.plot(data['data'][1] * scales[1],
                    (data['data'][0] * scales[0]).real)


def _contour_plot(data, scales, anim_outer):
    # Store output of pcolormesh as it needs to be returned but the
    # colorbar can't be added until after pcolormesh is run
    cmap = _get_cmap(data, scales)
    x = mpu.shift_axis_for_pcolormesh(data['data'][1] * scales[1])
    y = mpu.shift_axis_for_pcolormesh(data['data'][2] * scales[2])
    quadMesh = plt.pcolormesh(x, y, (data['data'][0] * scales[0]).real,
                              cmap=cmap)
    plt.colorbar(label=_get_label(data['names'][0]))
    return quadMesh


def _animated_contour_plot(data, scales, anim_outer):
    cpa = ContourPlotAnimator(data['data'][1] * scales[1],
                              data['data'][2] * scales[2],
                              data['data'][3] * scales[3],
                              (data['data'][0] * scales[0]).real,
                              _get_label(data['names'][1]),
                              _get_label(data['names'][2]),
                              _get_label(data['names'][3]),
                              _get_label(data['names'][0]),
                              _get_cmap(data, scales),
                              anim_outer)
    return cpa.animate()


def _do_not_label(data, scales, indicate_marginal):
    pass


def _label_line(data, scales, indicate_marginal):
    _label_axes(data, 0)


def _label_contour(data, scales, indicate_marginal):
    _label_axes(data, 2)
    # Indicate marginal stability boundary
    if indicate_marginal:
        if data['names'][0] == 'gamma':
            _indicate_marginal_stability_boundary(data, scales)
        elif data['names'][0] == 'omega':
            _indicate_marginal_stability_boundary(get('gamma', data['source']),
                                                  scales)


def _indicate_marginal_stability_boundary(gamma, scales, color='black'):
    # Only try to show marginal stability if there is a zero crossing -
    # otherwise we get an unwanted warning that zero is not in the data
    if np.min(gamma['data'][0]) < 0 and np.max(gamma['data'][0]) > 0:
        plt.contour(gamma['data'][1] * scales[1], gamma['data'][2] * scales[2],
                    gamma['data'][0] * scales[0], [0.0], colors=color)


def _label_axes(data, yindex):
    xname = data['names'][1]
    yname = data['names'][yindex]
    xlabel = _get_label(xname)
    ylabel = _get_label(yname)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def _get_label(name):
    try:
        label = axes_labels[name]
    except KeyError:
        label = name
    return label


def _get_scales(data):
    scales = []
    for name in data['names']:
        try:
            scales.append(scale_factors[name](data))
        except KeyError:
            scales.append(1)
    return scales


def _get_1_over_pi(data):
    return 1. / np.pi


def _get_phi_scale(data):
    phi = data['data'][0]
    if phi.ndim == 1:
        scale = _normalise_phi(phi)
    elif phi.ndim == 2:
        scale = np.ndarray((phi.shape[0], 1), dtype=complex)
        for i, ph in enumerate(phi):
            scale[i] = _normalise_phi(ph)
    else:
        num_outer = phi.shape[0]
        num_inner = phi.shape[1]
        scale = np.ndarray((num_outer, num_inner, 1), dtype=complex)
        for outer_index in range(num_outer):
            for inner_index in range(num_inner):
                scale[outer_index, inner_index] = \
                    _normalise_phi(phi[outer_index, inner_index, :])
    return scale


def _normalise_phi(phi):
    index_of_peak = np.where(abs(phi) == max(abs(phi)))[0][0]
    phi_peak = phi[index_of_peak]
    return 1. / phi_peak


def _get_cmap(data, scales):
    try:
        data_name = data['names'][0]
        cmap = cmap_fns[data_name]()
        if data_name == 'gamma' or data_name == 'phi':
            cmap = _shift_color_map(cmap, (data['data'][0] * scales[0]).real)
    except KeyError:
        # Just use seaborn's default greyscale colormap
        cmap = sns.light_palette("grey", as_cmap=True)
    return cmap


def _get_phi_cmap():
    cmap = mpl.cm.get_cmap('PiYG_r')
    return cmap


def _get_gamma_cmap():
    cmap = mpl.cm.get_cmap('RdBu_r')
    return cmap


def _get_omega_cmap():
    # Get raw colormap rotated slightly so pinks / reds are all at
    # high values so only the end needs to be cut off
    n_orig = 256  # Standard high resolution colormap
    cmap = mpl.colors.ListedColormap(sns.husl_palette(n_orig, h=0.07, s=1))
    # Adjust colormap to avoid very similar colors for both extreme
    # lows and extreme highs
    n_after = n_orig - 40  # Determined through trial and error
    trim_amt = n_after / float(n_orig)
    return _crop_cmap(cmap, 0.0, trim_amt, n_after)


def _crop_cmap(cmap, minval=0.0, maxval=1.0, n=256):
    return mpl.colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))


def _shift_color_map(cmap, data):
    # Moved to separate file so just delegate the call
    return mpu.shift_colormap(cmap, data)


# These dictionaries must be at the end of the file so that the
# functions they refer to have been defined

# fields: keys are the keywords used in the API to request data,
# values are the functions that are called to get the data
fields = {
    'gamma': _get_gamma,
    'omega': _get_omega,
    'phi': _get_phi
}

# show_fns: keys are the number of dimensions present in the data,
# values are the functions that are called to show the data
show_fns = {
    0: _print_value,
    1: _plot_line,
    2: _contour_plot,
    3: _animated_contour_plot
}

# label_fns: keys are the number of dimensions present in the data,
# values are the functions that are called to label the axes
label_fns = {
    0: _do_not_label,
    1: _label_line,
    2: _label_contour,
    3: _do_not_label,
}

# scale_factors: keys are fields with scaling factors, values are the
# corresponding functions that return the scaling factors. No entry
# means no scaling.
scale_factors = {
    'p': _get_1_over_pi,
    'theta0': _get_1_over_pi,
    'eta': _get_1_over_pi,
    'phi': _get_phi_scale
}

# cmap_fns: keys are fields with colormaps, values are the
# corresponding functions that return the colormap. No entry means no
# colormap.
cmap_fns = {
    'phi': _get_phi_cmap,
    'gamma': _get_gamma_cmap,
    'omega': _get_omega_cmap
}

# axes_labels: keys are known fields with defined labels, values are
# the corresponding axes labels. No entry means unknown label (so just
# use the name directly).
# Extracted to a separate module for re-use without dependencies
axes_labels = gal.axes_labels

# num_reflected_duplications_to_avoid: keys are fields that can be
# reflected in, values are the number of reflected duplications to
# avoid, e.g. p avoids 0 and pi hence 2, x avoids 0 hence 1
num_reflected_duplications_to_avoid = {
    'p': 2,
    'theta0': 2,
    'x': 1
}

# do_reflection_functions: keys are number of dimensions, values are
# functions to be called for reflecting that many dimensions
do_reflection_functions = {
    2: _do_reflection_2d,
    3: _do_reflection_3d
}

"""
This class represents a CHEASE equilibrium

Copyright 2020 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np
import os
import subprocess
import xarray as xr
from Equilibrium import Equilibrium
from MillerEquilibrium import MillerEquilibrium
from snbf_py_utils import physical_constants as phys


class CheaseEquilibrium(Equilibrium):
    def __init__(self, folder, filename):
        """
        Initialise the CHEASE equilibrium
        Inputs:
            - folder - string indicating the directory where the equilibrium
            data is located
            - filename - string indicating the filename of the equilibrium data
            file. This can be a text file (*.dat) or a NetCDF file (*.nc). If a
            text file is specified, it will be loaded and saved as a NetCDF
            file for quicker loading next time. If a NetCDF file is specified,
            it will be loaded directly.
        """
        super().__init__(folder, filename + ".nc")
        self.inputFilename = filename

    def load(self):
        if self.inputFilename.endswith(".dat"):
            self._parseCheaseTextFile()
        elif self.inputFilename.endswith(".nc"):
            self.equilibrium = xr.open_dataset(
                os.path.join(self.folder, self.inputFilename), autoclose=True
            )
        else:
            raise ValueError(
                "Unrecognised file extension {}".format(self.inputFilename)
            )
        self._extractCommonlyUsedQuantities()
        self._modified = False

    def _parseCheaseTextFile(self):
        self._splitTextFile()
        self._loadCoordinates()
        self._createDataset()
        self._getFileList()
        self._loadTxtFiles()
        self.dataset.to_netcdf(self.pathToFile)

    def _splitTextFile(self):
        # Capture output and get python to print so output is supressed in
        # unittests
        completedProcess = subprocess.run(
            ["chease2txt.sh", os.path.join(self.folder, self.inputFilename)],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        print(completedProcess.stdout.decode("utf-8"))

    def _loadCoordinates(self):
        # Lower-case psi, chi = np.array; upper-case PSI, CHI = xr.DataArray
        self.psi = np.loadtxt(os.path.join(self.folder, "PSI.txt"))
        self.chi = np.loadtxt(os.path.join(self.folder, "CHI.txt"))
        # Lower-case npsi, nchi = int; upper-case NPSI, NCHI = xr.DataArray
        self.npsi = len(self.psi)
        self.nchi = len(self.chi)

    def _createDataset(self):
        self.dataset = xr.Dataset(coords={"PSI": self.psi, "CHI": self.chi})

    def _getFileList(self):
        self.files = os.listdir(self.folder)
        self.files.sort()

    def _loadTxtFiles(self):
        for f in self.files:
            if f.endswith(".txt"):  # Skip other file types
                name = f.split(".")[0]
                self._addToDataset(
                    self._createDataArray(
                        np.loadtxt(
                            os.path.join(self.folder, f), dtype=_determineDtype(name)
                        ),
                        name,
                    )
                )

    def _createDataArray(self, data, name):
        if data.ndim == 0:
            dims = ()
        elif data.ndim == 1:
            if len(data) == self.nchi:
                dims = "CHI"
            elif len(data) == self.npsi:
                dims = "PSI"
            elif len(data) == self.npsi * self.nchi:
                dims = ("CHI", "PSI")
                data = data.reshape(self.nchi, self.npsi)
            else:
                raise (ValueError("Unknown len(data) {}".format(len(data))))
        return xr.DataArray(data, dims=dims, name=name)
        # Supress messy attribute in NetCDF output
        # dataArray.encoding['_FillValue'] = None
        # return dataArray

    def _extractCommonlyUsedQuantities(self):
        self._compute(["CHI", "PSI", "R", "B", "f", "Z", "q", "p", "TE", "NE"])

    def setDensityProfile(self, densityProfileNumpyArray, constant=False):
        # This assumes NE = NI so requires generalisation for other cases
        self._setProfile(densityProfileNumpyArray, "N", constant=constant)

    def setTemperatureProfile(self, temperatureProfileNumpyArray, constant=False):
        # This assumes TE = TI so requires generalisation for other cases
        self._setProfile(temperatureProfileNumpyArray, "T", constant=constant)

    def _setProfile(self, profileNumpyArray, quantityName, constant=False):
        """Sets the profiles of the given quantityName to the given profile
        Inputs:
            - profileNumpyArray - numpy array of the profile to set
            - quantityName - string 'T' or 'N' to indicate that the temperature
            or density profiles (respectively) are to be set
        """
        # This assumes ion profile = electron profile so requires
        # generalisation for other cases
        for species in ["I", "E"]:
            # Set profile
            name = quantityName + species
            self._addToDataset(self._createDataArray(profileNumpyArray, name))
            # Re-compute gradient
            gradientName = "D" + name + "DPSI"
            if constant:
                gradientProfile = np.zeros_like(profileNumpyArray)
            else:
                gradientProfile = np.gradient(self.dataset[name], self.PSI)
            self._addToDataset(self._createDataArray(gradientProfile, gradientName))
        # Re-compute pressure profile and gradient too
        self._computePressureProfile()
        # Update commonly used quantities (do this last)
        self._extractCommonlyUsedQuantities()

    def _computePressureProfile(self):
        # This assumes 2 species only so needs generalisation for other cases
        pressureProfile = phys.e * (
            self.dataset["TE"] * self.dataset["NE"]
            + self.dataset["TI"] * self.dataset["NI"]
        )
        self._addToDataset(self._createDataArray(pressureProfile, "p"))
        pressureGradientProfile = np.gradient(self.dataset["p"], self.PSI)
        self._addToDataset(self._createDataArray(pressureGradientProfile, "dpdpsi"))

    def computeReferenceValues(self):
        self._compute(
            ["lref", "bref", "mref", "nref", "rhoref", "tref", "vref", "zref"]
        )

    def computeInputFileValues(self):
        self._compute(
            [
                "rhoc",
                "beta",
                "beta_prime",
                "s_hat",
                "tprim",
                "fprim",
                "vnewk_e",
                "vnewk_i",
                "T_norm",
                "n_norm",
                "rhostar",
            ]
        )

    def millerFit(self):
        self._prepareForMillerFit()
        millerEquilibrium = MillerEquilibrium(
            self, self.folder, "Miller-" + self.filename + ".nc"
        )
        millerEquilibrium.computeMillerParameters()

    def _prepareForMillerFit(self):
        self._compute(["Bp", "x"])

    def _compute_Bp(self):
        self._create("Bt")
        Bp_squared = self.B ** 2 - self.Bt ** 2
        Bp_squared[dict(PSI=0)] *= 0  # Bp on axis is zero by definition
        return np.sqrt(Bp_squared)

    def _compute_Bt(self):
        # Transpose because dims come out in the wrong order otherwise
        return (self.f / self.R).T

    def _compute_bref(self):
        "Compute the reference magnetic field in Tesla"
        return xr.DataArray(1.0)

    def _compute_nspecies(self):
        # This assumes CHEASE equilibria always have exactly 2 species so this
        # needs generalising for other cases
        return xr.DataArray(2)


def _determineDtype(name):
    return int if name == "NPSI" or name == "NCHI" else float

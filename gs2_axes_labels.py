"""
Module to contain standard GS2 axes labels

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""


# axes_labels: keys are known fields with defined labels, values are
# the corresponding axes labels. No entry means unknown label (so just
# use the name directly).
dollar = r'$'
unit_sep = r'\;/\;'
pi_unit = unit_sep + r'\pi' + dollar
sub_0 = r'_0'
theta = r'\theta'
the = dollar + theta + pi_unit
theta0 = theta + sub_0
the0 = dollar + theta0 + pi_unit
vth = r'v_{th}'
Lref = r'L_{ref}'
over = r'/'
frequency_unit = sub_0 + unit_sep + vth + over + Lref + dollar
time_unit = unit_sep + Lref + over + vth + dollar
delta = r'\delta'
tri = r'\arcsin[' + delta + r']'
hat_phi = r'\hat\phi'
hat_phi_unit = r'T_{ref} \rho_{ref}' + over + r'(q_{ref}' + Lref + r')'
dphi = delta + hat_phi + sub_0
kappa = r'\kappa'
kap = dollar + kappa + dollar
prime = r'^{\prime}'
rhoref = r'\rho_{ref}' + dollar
kunit = rhoref
ky = dollar + r'k_y' + kunit
kx = dollar + r'k_x' + kunit
hat_n = delta + r'\hat n' + sub_0
n_unit = r'n_{ref}' + dollar
axes_labels = {
    'akappa': kap,
    'kappa': kap,
    'akappri': dollar + kappa + prime + dollar,
    'beta': dollar + r'\beta' + dollar,
    'eta': the,
    'theta': the,
    'gamma': dollar + r'\gamma' + frequency_unit,
    'omega': dollar + r'\omega' + frequency_unit,
    'p': the0,
    'theta0': the0,
    'phi': dollar + r'\Re (' + dphi + unit_sep + dphi + r'|_{peak})' + dollar,
    'x': dollar + r'x' + unit_sep + rhoref,
    'y': dollar + r'y' + unit_sep + rhoref,
    'r0-over-a': dollar + r'r' + sub_0 + unit_sep + r'a' + dollar,
    'tri': dollar + tri + dollar,
    'tripri': dollar + tri + prime + dollar,
    'aky': ky,
    'ky': ky,
    'akx': kx,
    'kx': kx,
    'n0': dollar + r'n_0' + dollar,
    'q': dollar + r'q' + dollar,
    't': dollar + r't' + time_unit,
    'phi2': dollar + dphi + r'^2' + unit_sep +
    r'(' + hat_phi_unit + r')^2' + dollar,
    'abs(phi)': dollar + r'|' + dphi + r'|' + unit_sep + hat_phi_unit + dollar,
    'Re[hat n]': dollar + r'\Re[' + hat_n + r']' + n_unit,
    'Im[hat n]': dollar + r'\Im[' + hat_n + r']' + n_unit,
}

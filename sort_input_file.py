#! /usr/bin/env python3
"""
Script to sort a GS2 input file

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

# Acts on input.in in current directory

import re
import shutil

with open('input.in', 'r') as f:
    input_lines = f.readlines()

# Loop over lines to collect params
params = {}
for i, line in enumerate(input_lines):
    if re.match('&', line):  # Start of namelist
        nml = line
        params[nml] = {}
    # Comment line (! or #), blank line or end of namelist
    elif (re.match(' *!', line) or re.match(' *#', line) or
          re.match(' *\n', line) or re.match(' */', line)):
        continue
    else:  # Parameter (so store it)
        match = re.search('([A-Za-z0-9_]*) *= *([A-Za-z0-9_."' + "'-]*) *.*",
                          line)
        name = match.group(1)
        val = match.group(2)
        params[nml][name] = val

# Loop over sorted namelists and parameters to write sorted file
shutil.copy('input.in', 'input.in.bak')
namelists = list(params.keys())
namelists.sort()
with open('input.in', 'w') as f:
    for nml in namelists:
        f.write(nml)
        ps = list(params[nml].keys())
        ps.sort()
        for p in ps:
            f.write("  %s = %s\n" % (p, str(params[nml][p])))
        f.write('/\n')
        f.write('\n')

"""
Python script to plot growth rate and frequency from GS2 data

Copyright 2016 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""
from cmath import phase
from math import cos, sin
from re import compile

from snbf_py_utils.general import preallocate_based_on
from snbf_py_utils.general import extract_subarrays_via_logical_index
from snbf_py_utils.stats import round_val_to_err
from numpy import array, exp

from gs2_data_utils import find_eig_files, sort_datafiles
from gs2_data_utils import extract_complex_frequency
from gs2_plot_utils import plot_frequency, plot_growth_rate
from ncdf2dict import ncdf2dict


# ---------------------------- Functions -----------------------------

def process_and_make_standard_plots():
    'Executes all the subfunctions in this module in the standard way'
    indie_var_names, indie_var_vals, growth_rate, growth_rate_error, \
        frequency, frequency_error = process()
    plot_variation_with_input_parameter(
        indie_var_names, indie_var_vals, growth_rate, growth_rate_error,
        frequency, frequency_error)


def process():
    'Finds and processes GS2 data files (but does not plot).'
    files, indie_var_names, indie_var_vals, growth_rate, growth_rate_error, \
        frequency, frequency_error = initialise()
    for i, datafile in enumerate(files):
        growth_rate[i], growth_rate_error[i], frequency[i], \
            frequency_error[i] = analyse_file(
                datafile, indie_var_names[i], indie_var_vals[i])
    return indie_var_names, indie_var_vals, growth_rate, growth_rate_error, \
        frequency, frequency_error


def initialise():
    'Finds files, sets up arrays, etc.'
    files = find_eig_files()
    files, indie_var_names, indie_var_vals = sort_datafiles(files)
    growth_rate, growth_rate_error, frequency, frequency_error = \
        preallocate_arrays_based_on(files)
    print_results(header=True)
    return files, indie_var_names, indie_var_vals, growth_rate, \
        growth_rate_error, frequency, frequency_error


def preallocate_arrays_based_on(array, dps=False):
    """Preallocates growth rate and frequency value and uncertainty
    arrays of the same size as array"""
    gamma = preallocate_based_on(array)
    gamma_error = preallocate_based_on(array)
    omega = preallocate_based_on(array)
    omega_error = preallocate_based_on(array)
    return gamma, gamma_error, omega, omega_error


def print_results(header=False, results=[]):
    """Prints the results in the format required for this analysis
    Inputs:
    - header: if false or omitted, this function prints a results line; if
        true, this function prints a column headers line
    - results: a list of the results to be printed in the following order:
      mode number, growth rate, growth rate error, growth rate decimal places,
      frequency, frequency error, frequency decimal places
    """
    if header:
        print 'var_name, var_val, eigenmode, gamma, gamma_err, omega, ' + \
            'omega_err'
    else:
        indie_var_name, indie_var_val, mode_id, growth_rate, \
            growth_rate_error, growth_rate_dps, frequency, frequency_error, \
            frequency_dps = tuple(results)
        print (
            indie_var_name + ', ' + str(indie_var_val) + ", " + str(mode_id) +
            ", %." + str(growth_rate_dps) + "f, %." + str(growth_rate_dps) +
            "f, %." + str(frequency_dps) + "f, %." + str(frequency_dps) +
            "f") % (
                growth_rate, growth_rate_error, frequency, frequency_error)


def analyse_file(datafile, indie_var_name, indie_var_val):
    'Performs the required analysis for the given datafile'
    big_omega = get_data(datafile)
    growth_rate, growth_rate_error, growth_rate_dps, frequency, \
        frequency_error, frequency_dps = take_measurements(
                    big_omega, datafile)
    output_one_file_results(
            indie_var_name, indie_var_val, growth_rate, growth_rate_error,
            growth_rate_dps, frequency, frequency_error, frequency_dps)
    return growth_rate, growth_rate_error, frequency, frequency_error


def get_data(datafile):
    'Gets the data required for this analysis from the given datafile'
    netcdf_data = ncdf2dict(datafile)
    return extract_required_fields(netcdf_data)


def extract_required_fields(netcdf_data):
    'Extracts the fields from netcdf_data that are required for this analysis'
    return extract_complex_frequency(netcdf_data)


def take_measurements(big_omegas, datafile):
    'Takes measurements of growth rate and frequency'
    # Get input parameters (same for all eigenvalues in datafile)
    tolerance, delt, nadv = get_input_parameters(datafile)

    # Preallocate
    growth_rates, growth_rate_errs, frequencies, frequency_errs = \
        preallocate_arrays_based_on(big_omegas)
    growth_rate_dps = preallocate_based_on(big_omegas)
    frequency_dps = preallocate_based_on(big_omegas)

    # Loop through eigenmodes
    for i, big_omega in enumerate(big_omegas):
        delta_omega, delta_gamma = estimate_uncertainty(big_omega, tolerance,
                                                        delt, nadv)
        growth_rates[i], growth_rate_errs[i], growth_rate_dps[i] = \
            round_val_to_err(
                big_omega.imag, delta_gamma)
        frequencies[i], frequency_errs[i], frequency_dps[i] = \
            round_val_to_err(
                big_omega.real, delta_omega)

    # Sort by frequency
    # sort = sorted(zip(frequencies, growth_rates, growth_rate_errs,
    #                   growth_rate_dps, frequency_errs, frequency_dps))
    # frequencies = [f for (f, g, ge, gd, fe, fd) in sort]
    # growth_rates = [g for (f, g, ge, gd, fe, fd) in sort]
    # growth_rate_errs = [ge for (f, g, ge, gd, fe, fd) in sort]
    # growth_rate_dps = [gd for (f, g, ge, gd, fe, fd) in sort]
    # frequency_errs = [fe for (f, g, ge, gd, fe, fd) in sort]
    # frequency_dps = [fd for (f, g, ge, gd, fe, fd) in sort]

    return growth_rates, growth_rate_errs, growth_rate_dps, frequencies, \
        frequency_errs, frequency_dps


def get_input_parameters(datafile):
    """Gets the input parameters necessary to estimate the uncertainty
    assuming that there is an input.in file alongside datafile"""
    input_file = datafile[0:-11] + '.in'
    for line in open(input_file):
        if "tolerance" in line:
            pattern = compile(' *tolerance *= *(.+)')
            match = pattern.match(line)
            tolerance_string = match.group(1)
        elif "delt" in line:
            pattern = compile(' *delt *= *(\d\.?\d*).*')
            match = pattern.match(line)
            delt_string = match.group(1)
        elif "nadv" in line:
            pattern = compile(' *nadv *= *(\d+).*')
            match = pattern.match(line)
            nadv_string = match.group(1)
    return float(tolerance_string), float(delt_string), float(nadv_string)


def estimate_uncertainty(big_omega, tolerance, delt, nadv):
    """Estimates the uncertainty on big_omega assuming that it was
    computed using tolerance and delt"""
    dt = delt * nadv
    current_lambda = compute_lambda(big_omega, dt)
    phi_omega = phase(big_omega)
    delta_mod_big_omega = abs(
        -phase(current_lambda) * tolerance / (exp(1j * phi_omega) * dt *
                                              abs(current_lambda)))
    delta_omega = delta_mod_big_omega * cos(phi_omega)
    delta_gamma = delta_mod_big_omega * sin(phi_omega)
    return delta_omega, delta_gamma


def compute_lambda(big_omega, dt):
    'Computes lambda, the eigenvalue computed by the eigensolver'
    return exp(-1j * big_omega * dt)


def output_one_file_results(
        indie_var_name, indie_var_val, growth_rate, growth_rate_error,
        growth_rate_dps, frequency, frequency_error, frequency_dps):
    'Prints and plots the results of the analysis of one file'
    for i in range(len(growth_rate)):
        print_results(
            results=[
                indie_var_name, indie_var_val, i, growth_rate[i],
                growth_rate_error[i], growth_rate_dps[i], frequency[i],
                frequency_error[i], frequency_dps[i]])


def plot_variation_with_input_parameter(
        indie_var_names, indie_var_vals, growth_rate, growth_rate_error,
        frequency, frequency_error):
    'Plots the results of this analysis against the relevant quantities'
    unique_names = set(indie_var_names)
    for name in unique_names:
        logical_index = array(indie_var_names) == name
        matching_vals, matching_gammas, matching_gamma_errs, matching_freqs, \
            matching_freq_errs = extract_subarrays_via_logical_index(
                        [indie_var_vals, growth_rate, growth_rate_error,
                            frequency, frequency_error], logical_index)
        # In the following, it is correct that name is passed twice. The first
        # is for the xlabel and the second is for the graph filename. In this
        # situation, we use the same string for both but in other situations
        # they may need to be different.
        plot_growth_rate(
                matching_vals, matching_gammas, matching_gamma_errs, name, name)
        plot_frequency(
                matching_vals, matching_freqs, matching_freq_errs, name, name)

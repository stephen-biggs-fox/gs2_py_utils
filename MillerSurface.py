"""
The class represents a Miller fit to a surface

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np
from scipy import optimize as sp_optim

parameterNames = ['A', 'tri', 'akappa', 'sk', 'sd', 'dr_R0', 'dr_Psi_norm']


class MillerSurface:

    def __init__(self, theta, R, Z, Bp, initialGuesses):
        """
        Initialise the Miller surface
        Inputs:
            - R - normalised major radius as a function of poloidal angle
            - Z - normalised height as a function of poloidal angle
            - Bp - normalised poloidal magnetic field as a function of poloidal
                    angle
        """
        self.theta = theta
        self.R = R
        self.Z = Z
        self.Bp = Bp
        self.initialGuesses = initialGuesses

    def computeMillerParameters(self):
        """
        Computes the Miller fit parameters for this surface
        Returns:
            - params - tuple representing the Miller parameters in the
            following order: (A, kappa, tri, dr_psi,
        """
        isOk = True
        try:
            popt, pcov = sp_optim.curve_fit(
                fitMiller, self.theta,
                np.concatenate((self.R, self.Z, self.Bp)),
                p0=self.initialGuesses)
        except(RuntimeError):
            isOk = False
            popt = self.initialGuesses
        return popt, isOk


def fitMiller(theta, A, tri, kappa, sk, sd, dr_R0, dr_psi):
    R = R_miller(theta, A, tri)
    Z = Z_miller(theta, kappa)
    Bp = Bp_miller(theta, tri, kappa, sk, sd, dr_R0, dr_psi, R)
    return np.concatenate((R, Z, Bp))


def R_miller(theta, A, tri):
    return A + np.cos(theta + tri * np.sin(theta))


def Z_miller(theta, kappa):
    return kappa * np.sin(theta)


def Bp_miller(theta, tri, kappa, sk, sd, dr_R0, dr_psi, R):
    sin_theta = np.sin(theta)
    cos_theta = np.cos(theta)
    tri_sin_theta = tri * sin_theta
    tri_cos_theta = tri * cos_theta
    numerator = dr_psi * kappa**(-1) * R**(-1) * (
        (np.sin(theta + tri_sin_theta))**2 * (1 + tri_cos_theta)**2 +
        kappa**2 * cos_theta**2)**(1 / 2)
    denominator = np.cos(tri_sin_theta) + dr_R0 * cos_theta + (
        sk - sd * cos_theta + (1 + sk) * tri_cos_theta) * sin_theta * np.sin(
        theta + tri_sin_theta)
    Bp = numerator / denominator
    return Bp

"""
Functions for transforming a spectral field to real-space

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import f90nml
import gs2_data_utils as dat
import gs2nl
import netCDF4 as nc
import numpy as np
import os
import shutil
import snbf_py_utils.general as gen


class Gs2CoordinateTransform:

    def __init__(self, folder='.', runname="input", n0=None):
        """Initilizes the Gs2CoordinateTransform object
        Inputs:
        - folder - string specifying directory where files are kept
        - runname - string specifying filename prefix of data files
        - n0 - int specifying toroidal mode number (see also compute_n0)"""
        self.folder_slash_runname = os.path.join(folder, runname)
        # Set up variables for lazy loading
        self.dataset = None  # .out.nc file
        self.geometry = None  # .g file
        self.input_file = None  # .in file
        self.output_file = None  # .transform.nc file
        self.dimensions = None
        self.transforms = None
        self.n0 = n0
        self.rho_star = self.compute_rho_star(n0) if n0 is not None else None

    def compute_n0(self, rho_star):
        """Computes n0 for a given rho_star = rho_ref / a
        Inputs:
        - rho_star - float specifying rho_ref / a
        Output:
        - n0 - float specifying toroidal mode number
        NB: n0 should be an integer but the output of this function, in
        general, is not an integer. Therefore, one should consider this to be a
        utility function for working out the approximate n0 to use when
        rho_star is known, e.g. when simulating a particular experimental shot.
        In this case, the result should be rounded to the nearest integer and a
        corrected rho_star calculated using compute_rho_star."""
        self.load_dataset()
        return self.get_k_min('y') / (rho_star *
                                      self.dataset['drhodpsi'].values)

    def compute_rho_star(self, n0):
        """Computes rho_star = rho_ref / a for a given toroidal mode number, n0
        Inputs:
        - n0 - int specifying toroidal mode number
        Outputs:
        - rho_star - float specifying rho_ref / a"""
        self.load_dataset()
        return self.get_k_min('y') / (n0 * self.dataset['drhodpsi'].values)

    def set_n0(self, n0):
        """Sets the values of n0 and rho_star such that they are consistent
        with each other
        Inputs:
        - n0 - int specifying toroidal mode number"""
        self.n0 = n0
        self.rho_star = self.compute_rho_star(n0)

    def transform(self, fields):
        """Transforms 'fields' from spectral flux coordinates to real-space
        cylindrical and Cartesian coordinates
        Inputs:
        - fields - string or list of strings indicating which field(s) to
        transform - each field must be a function of ky, kx and ri - may also
        be a function of other dimensions (species, theta, t, etc)
        NB: this calculation uses the value currently stored in self.n0 and
        self.rho_star so ensure they are set to something either by passing a
        value of n0 to the constructor or by using self.set_n0"""
        self.compute_dimensions()
        self.write_dimensions()
        self.transform_fields(fields)
        self.write_transformed_fields()
        self.compute_last_closed_flux_surface()
        self.write_last_closed_flux_surface()

    def compute_dimensions(self):
        """Computes the dimensions of the transformed field: Lx, Ly, x, y, R,
        Z, zeta, X, Y"""
        if self.dimensions is None:
            print("Computing dimensions...")
            self.dimensions = {}
            self.compute_perpendicular_coordinates()
            self.compute_cylindrical_coordinates()
            self.compute_cartesian_coordinates()

    def compute_perpendicular_coordinates(self):
        "Computes the perpendicular coordinates: x and y"
        self.dimensions['Lx'] = self.compute_L('x')
        self.dimensions['Ly'] = self.compute_L('y')
        self.dimensions['nx'], self.dimensions['x'] = self.compute_axis('x')
        self.dimensions['ny'], self.dimensions['y'] = self.compute_axis('y')

    def compute_L(self, axis):
        """Computes the length of the box in the specified direction
        Inputs:
        - axis - string with value 'x' or 'y' to indicate which length to
        compute
        Output:
        - the length of the box in the specified direction, normalised to
        rho_ref"""
        return 2. * np.pi / self.get_k_min(axis)

    def get_k_min(self, axis):
        """Gets the smallest non-zero wavenumber in the specified direciton
        Inputs:
        - axis - string with value 'x' or 'y' to indicate which wavenumber to
        get
        Output:
        - the smallest non-zero wavenumber in the specified direction,
        normalised to rho_ref"""
        self.load_dataset()
        return self.dataset['k' + axis].values[1]

    def load_dataset(self):
        "Loads the dataset"
        if self.dataset is None:
            self.dataset = gs2nl.NonlinearGs2File(
                self.get_dataset_file_name()).dataset

    def get_dataset_file_name(self):
        "Gets the dataset filename"
        return self.folder_slash_runname + ".out.nc"

    def compute_axis(self, axis):
        """Computes the perpendicular coordinate array in the specified
        direction
        Inputs:
        - axis - string with value 'x' or 'y' top indicate which axis to
        compute
        Output:
        - array of coordinate values"""
        L = self.compute_L(axis)
        self.load_input_file()
        n = self.input_file['kt_grids_box_parameters']['n' + axis]
        return n, np.linspace(-L / 2, L / 2, num=n)

    def load_input_file(self):
        "Loads the input file as a dictionary"
        if self.input_file is None:
            self.input_file = f90nml.read(self.folder_slash_runname + ".in")

    def compute_cylindrical_coordinates(self):
        "Computes the cylindrical coordinates of each (x, y, theta) point"
        # self.dimensions['x'] and self.dimensions['y'] are multiplied by
        # self.rho_star * self.dataset['drhodpsi'].values throughout this
        # function to convert from perpendicular units (rho_ref) to the same
        # length units as everything else (L_ref)
        self.compute_perpendicular_coordinates()
        self.check_caveats()
        self.load_dataset()
        self.dimensions['ntheta'] = len(self.dataset['theta'].values)
        # Repeat R_2d and Z_2d along the 3rd dimension after the loop
        R_2d = np.ndarray((self.dimensions['nx'], self.dimensions['ntheta']))
        Z_2d = np.ndarray((self.dimensions['nx'], self.dimensions['ntheta']))
        zeta = np.ndarray((self.dimensions['ny'], self.dimensions['nx'],
                           self.dimensions['ntheta']))
        self.load_geometry()
        for itheta in range(self.dimensions['ntheta']):
            R_2d[:, itheta] = self.geometry['R'][itheta] + \
                self.geometry['R_prime'][itheta] * self.dimensions['x'] * \
                self.rho_star * self.dataset['drhodpsi'].values
            Z_2d[:, itheta] = self.geometry['Z'][itheta] + \
                self.geometry['Z_prime'][itheta] * self.dimensions['x'] * \
                self.rho_star * self.dataset['drhodpsi'].values
            alpha = self.geometry['alpha_new'][itheta] + \
                self.geometry['alpha_prime_new'][itheta] * \
                self.dimensions['x'] * self.rho_star * \
                self.dataset['drhodpsi'].values
            # Loop over y values becuase alpha is a function of x so looping
            # avoids having to use meshgrids
            for iy in range(self.dimensions['ny']):
                zeta[iy, :, itheta] = self.dimensions['y'][iy] * \
                    self.rho_star * self.dataset['drhodpsi'].values - alpha
        R_3d = np.repeat(R_2d[np.newaxis, :, :], self.dimensions['ny'], axis=0)
        Z_3d = np.repeat(Z_2d[np.newaxis, :, :], self.dimensions['ny'], axis=0)
        self.dimensions['R'] = R_3d
        self.dimensions['Z'] = Z_3d
        self.dimensions['zeta'] = zeta

    def check_caveats(self):
        """Checks the caveats listed in Applegate and Roach (2009)
        'GS2 Coordinate Transformation for Visualisations'"""
        self.check_magnetic_field_direction()
        self.ensure_safety_factor_is_rational()
        self.correct_alpha_against_q_rat()
        self.correct_alpha_prime_against_q_rat()

    def check_magnetic_field_direction(self):
        """Checks if the magnetic field is in the reverse direciton and, if so,
        puts it into the forward direction"""
        self.load_geometry()
        # Find where theta = -pi
        theta_equals_minus_pi_index = \
            np.argmin(np.abs(self.geometry['theta'] - (-np.pi)))
        # If magnetic field is in reverse direction (alpha(theta=-pi) < 0),
        # then put it into the forward direction (by multiplying by -1)
        alpha_ref = self.geometry['alpha'][theta_equals_minus_pi_index]
        # The calculation we need to do is:
        #   alpha_new = (-1.0 * alpha) if (alpha_ref < 0) else (+1.0 * alpha)
        # This is achieved using:
        #   alpha_new = factor * alpha
        # where factor is -1.0 or +1.0 as required.
        # This in turn is achieved using:
        #   factor = alpha_ref / abs(alpha_ref)
        # so that abs(alpha_ref) is always > 0 so alpha_ref > 0 results in
        # factor = +1.0 while alpha_ref < 0 results in factor = -1.0
        self.geometry['alpha_new'] = (alpha_ref / abs(alpha_ref)) * \
            self.geometry['alpha']

    def load_geometry(self):
        "Loads the geometry data"
        if self.geometry is None:
            self.geometry = {}
            geom = np.loadtxt(self.folder_slash_runname + ".g")
            names = ["theta", "R", "Z", "alpha", "R_prime", "Z_prime",
                     "alpha_prime"]
            for i, n in enumerate(names):
                self.geometry[n] = geom[:, i]

    def ensure_safety_factor_is_rational(self):
        """Ensures that the safety factor q = m / n is a rational number for
        the given integer toroidal mode number, n, and poloidal mode number, m,
        that is the integer nearest to the m corresponding to the (possibly
        irrational) equilibrium q
        Inputs:
        - n0 - int specifying the toroidal mode number
        Output:
        - q - float specifying the rational safety factor"""
        self.load_dataset()
        q = self.dataset['q'].values
        m = int(np.round(q * self.n0))
        self.load_geometry()
        self.geometry['q_rat'] = m / self.n0

    def correct_alpha_against_q_rat(self):
        "Ensures that alpha is consistent with the rational value of q"
        self.load_dataset()
        self.load_geometry()
        self.check_magnetic_field_direction()
        self.ensure_safety_factor_is_rational()
        self.geometry['alpha_new'] = self.geometry['alpha'] + \
            (self.dataset['q'].values - self.geometry['q_rat']) * \
            self.geometry['theta']

    def compute_consistent_shat(self):
        "Ensures that shat is consistent"
        # TODO - check calculation with DD
        self.load_dataset()
        self.load_input_file()
        self.load_geometry()
        self.geometry['shat_new'] = \
            self.input_file['kt_grids_box_parameters']['jtwist'] / \
            (self.n0 * self.compute_L('x') * self.rho_star *
             self.dataset['drhodpsi'].values)

    def correct_alpha_prime_against_q_rat(self):
        "Ensures that alpha_prime is consistent with the rational value of q"
        self.load_dataset()
        self.load_input_file()
        self.load_geometry()
        self.compute_consistent_shat()
        self.ensure_safety_factor_is_rational()
        q_prime_gs2 = self.dataset['shat'].values * \
            self.dataset['q'].values / \
            self.input_file['theta_grid_parameters']['rhoc']
        q_prime_new = self.geometry['shat_new'] * self.geometry['q_rat'] / \
            self.input_file['theta_grid_parameters']['rhoc']
        self.geometry['alpha_prime_new'] = self.geometry['alpha_prime'] + \
            (q_prime_gs2 - q_prime_new) * self.geometry['theta']

    def compute_cartesian_coordinates(self):
        "Computes the Cartesian coordinates of each (x, y, theta) point"
        self.compute_cylindrical_coordinates()
        X = np.ndarray((self.dimensions['ny'], self.dimensions['nx'],
                        self.dimensions['ntheta']))
        Y = np.ndarray((self.dimensions['ny'], self.dimensions['nx'],
                        self.dimensions['ntheta']))
        for iy in range(self.dimensions['ny']):
            for ix in range(self.dimensions['nx']):
                for itheta in range(self.dimensions['ntheta']):
                    R = self.dimensions['R'][iy, ix, itheta]
                    zeta = self.dimensions['zeta'][iy, ix, itheta]
                    X[iy, ix, itheta] = R * np.cos(zeta)
                    Y[iy, ix, itheta] = R * np.sin(zeta)
        self.dimensions['X'] = X
        self.dimensions['Y'] = Y

    def write_dimensions(self):
        """Writes newly computed dimensions to file"""
        print("Writing dimensions...")
        self.compute_dimensions()
        self.open_output_file_for_writing()
        # Lx
        print("Lx", end="")
        self.output_file.createVariable('Lx', 'd')  # 'd' = double
        self.output_file.variables['Lx'].assignValue(self.dimensions['Lx'])
        self.output_file.variables['Lx'].units = 'rho_ref'
        self.output_file.variables['Lx'].description = 'Extent of the ' + \
            'flux tube in the x direction'
        # Ly
        print(", Ly", end="")
        self.output_file.createVariable('Ly', 'd')  # 'd' = double
        self.output_file.variables['Ly'].assignValue(self.dimensions['Ly'])
        self.output_file.variables['Ly'].units = 'rho_ref'
        self.output_file.variables['Ly'].description = 'Extent of the ' + \
            'flux tube in the y direction'
        # nx
        print(", nx", end="")
        self.output_file.createVariable('nx', 'i')  # 'i' = int
        self.output_file.variables['nx'].assignValue(self.dimensions['nx'])
        self.output_file.variables['nx'].description = 'Number of grid ' + \
            'points along the coordinate perpendicular to the flux surface'
        # x
        print(", x", end="")
        self.output_file.createDimension('x', len(self.dimensions['x']))
        self.output_file.createVariable('x', 'd',  # 'd' = double
                                        dimensions='x')
        self.output_file.variables['x'][:] = self.dimensions['x']
        self.output_file.variables['x'].units = 'rho_ref'
        self.output_file.variables['x'].description = 'Coordinate ' + \
            'perpendicular to the flux surface'
        # ny
        print(", ny", end="")
        self.output_file.createVariable('ny', 'i')  # 'i' = int
        self.output_file.variables['ny'].assignValue(self.dimensions['ny'])
        self.output_file.variables['ny'].description = 'Number of grid ' + \
            'points along the coordinate perpendicular to the field line ' + \
            'but on the flux surface'
        # y
        print(", y", end="")
        self.output_file.createDimension('y', len(self.dimensions['y']))
        self.output_file.createVariable('y', 'd',  # 'd' = double
                                        dimensions='y')
        self.output_file.variables['y'][:] = self.dimensions['y']
        self.output_file.variables['y'].units = 'rho_ref'
        self.output_file.variables['y'].description = 'Coordinate ' + \
            'perpendicular to the field line but on the flux surface'
        # ntheta
        print(", ntheta", end="")
        self.output_file.createVariable('ntheta', 'i')  # 'i' = int
        self.output_file.variables['ntheta'].assignValue(
            self.dimensions['ntheta'])
        self.output_file.variables['ntheta'].description = \
            'Number of grid points along the coordinate parallel to the ' + \
            'field line'
        # R
        print(", R", end="")
        self.output_file.createVariable('R', 'd',  # 'd' = double
                                        dimensions=('y', 'x', 'theta'))
        self.output_file.variables['R'][:, :, :] = self.dimensions['R']
        self.output_file.variables['R'].units = 'L_ref'
        self.output_file.variables['R'].description = 'Major radius coordinate'
        # Z
        print(", Z", end="")
        self.output_file.createVariable('Z', 'd',  # 'd' = double
                                        dimensions=('y', 'x', 'theta'))
        self.output_file.variables['Z'][:, :, :] = self.dimensions['Z']
        self.output_file.variables['Z'].units = 'L_ref'
        self.output_file.variables['Z'].description = 'Height coordinate'
        # zeta
        print(", zeta", end="")
        self.output_file.createVariable('zeta', 'd',  # 'd' = double
                                        dimensions=('y', 'x', 'theta'))
        self.output_file.variables['zeta'][:, :, :] = self.dimensions['zeta']
        self.output_file.variables['zeta'].units = 'L_ref'
        self.output_file.variables['zeta'].description = 'Toroidal angle' + \
            'coordinate'
        # X
        print(", X", end="")
        self.output_file.createVariable('X', 'd',  # 'd' = double
                                        dimensions=('y', 'x', 'theta'))
        self.output_file.variables['X'][:, :, :] = self.dimensions['X']
        self.output_file.variables['X'].units = 'L_ref'
        self.output_file.variables['X'].description = 'Cartesian x ' + \
            'coordinate (e.g. East-West coordinate)'
        # Y
        print(", Y")
        self.output_file.createVariable('Y', 'd',  # 'd' = double
                                        dimensions=('y', 'x', 'theta'))
        self.output_file.variables['Y'][:, :, :] = self.dimensions['Y']
        self.output_file.variables['Y'].units = 'L_ref'
        self.output_file.variables['Y'].description = 'Cartesian y ' + \
            'coordinate (e.g. North-South coordinate)'
        # Close file
        self.close_output_file()

    def open_output_file_for_writing(self):
        "Opens the output file for appending newly computed fields to"
        output_file_name = self.folder_slash_runname + ".transform.nc"
        if self.output_file is None:
            shutil.copyfile(self.get_dataset_file_name(), output_file_name)
            # 'r+' = append
        self.output_file = nc.Dataset(output_file_name, mode='r+')

    def close_output_file(self):
        "Closes the output file"
        # Check if output_file is None because None.close() would raise an
        # error. No need for an else block becuase output_file is None means
        # that it was never opened.
        if self.output_file is not None:
            self.output_file.close()

    def transform_fields(self, fields):
        """Transforms 'fields' to real-space
        Inputs:
        - fields - string or list of strings indicating which fields are to be
        transformed"""
        self.load_dataset()
        if isinstance(fields, str):
            fields = [fields]
        # Initialise self.transforms as an empty list
        if self.transforms is None:
            self.transforms = []
        for f in fields:
            # Don't use self.dataset[f].values as we need the metadata to
            # extract the correct dimensions
            print("Transfrorming " + f + "...")
            array, dims = self.transform_field(self.dataset[f])
            self.transforms.append(Transform(f + '_inv', array, dims))

    def transform_field(self, dataarray):
        """Transform one field to real-space
        Inputs:
        - dataarray - an xarray dataarray that is a function of at least ky, kx
        and ri, plus maybe other dimensions such as species, theta, t, etc."""
        # Check that dataarray is a function of ky and kx
        if 'ky' in dataarray.dims and 'kx' in dataarray.dims \
                and 'ri' in dataarray.dims:
            ky_dim_index = dataarray.dims.index('ky')
            kx_dim_index = dataarray.dims.index('kx')
            ri_dim_index = dataarray.dims.index('ri')
        else:
            raise ValueError(dataarray.name +
                             ' is not a function of ky, kx and ri')
        # We assume ri is last dim so check that assumption
        assert(ri_dim_index == len(dataarray.dims) - 1)
        # Convert from real function of ri to complex numbers
        array = dat.ri_to_complex(dataarray.values)
        # Prepare list of dimensions to loop over - all except ri (already
        # removed), and ky and kx (required for IFFT)
        loop_dims = np.delete(np.arange(len(array.shape)),
                              [ky_dim_index, kx_dim_index])
        # Loop over those dimensions and compute 2D (x, y) IFFT for each combo
        transformed_array = gen.iterate_axes(array, loop_dims, self.ifft)
        new_dims = list(dataarray.dims)
        new_dims[ky_dim_index] = 'y'
        # TODO - check if this is always correct - I think it is always at
        # ky_dim_index + 1 rather than at kx_dim_index (although, these are
        # usually the same). In either case, worth checking that 'x', and 'y'
        # for that matter, appear in the correct positions.
        assert(kx_dim_index == ky_dim_index + 1)
        new_dims[kx_dim_index] = 'x'
        # Remove 'ri' dimension as output function is purely real
        del new_dims[ri_dim_index]
        return transformed_array, new_dims

    def ifft(self, array):
        """Computes IFFT from complex to real
        Input:
        - array - array to be ITTF'd, with dimensions (ky, kx) and complex
        data-type
        NB: array must be in the unshifted order (i.e. do not use fftshift on
        it)
        Output:
        - IFFT'd array"""
        # Get output shape
        self.compute_dimensions()
        shape = (self.dimensions['ny'], self.dimensions['nx'])
        # Manual pad with zeros in x direction as the automatic method does it
        # wrong
        padded_input = np.zeros((array.shape[0], shape[1]), dtype=complex)
        nakx = array.shape[1]
        assert(nakx % 2 == 1)  # assert naky is odd
        padded_input[:, :nakx // 2 + 1] = array[:, :nakx // 2 + 1]
        padded_input[:, -nakx // 2:] = array[:, -nakx // 2:]
        # Pre-allocate intermediate storage array
        tmp = np.ndarray((array.shape[0], shape[1]), dtype=complex)
        # F.T. in x-direction
        for iky in range(array.shape[0]):
            # Using array is wrong as zero padding goes in the wrong place
            # Don't do this
            # tmp[iky, :] = np.fft.ifft(array[iky, :], n=shape[1])
            # This is the correct way
            tmp[iky, :] = np.fft.ifft(padded_input[iky, :])
        # F.T. in y-direction using automatic padding (works for
        # complex-to-real IFFT)
        output = np.ndarray(shape, dtype=float)
        for ikx in range(tmp.shape[1]):
            output[:, ikx] = np.fft.irfft(tmp[:, ikx], n=shape[0])
        # Return output
        return output

    def write_transformed_fields(self):
        """Writes transformed fields to file"""
        self.compute_dimensions()
        self.open_output_file_for_writing()
        for t in self.transforms:
            print("Writing " + t.name + "...")
            self.output_file.createVariable(t.name, 'd',  # 'd' = double
                                            dimensions=t.dims)
            # Check we have at least x and y
            assert(len(t.dims) >= 2)
            # Due to the way value assignment is implemented, we need to set a
            # limit on the maximum number of dimensions. The most I can think
            # of is: species, x, y, t, theta. This limit of 5 can always be
            # increased if needed
            assert(len(t.dims) <= 5)
            # TODO - find a generic way to do this for an arbitrary number of
            # dimensions (hopefully in only one line of code)
            if len(t.dims) == 2:
                self.output_file.variables[t.name][:, :] = t.array
            elif len(t.dims) == 3:
                self.output_file.variables[t.name][:, :, :] = t.array
            elif len(t.dims) == 4:
                self.output_file.variables[t.name][:, :, :, :] = t.array
            elif len(t.dims) == 5:
                self.output_file.variables[t.name][:, :, :, :, :] = t.array
            # TODO - find a way of adding units and descriptions to each
            # variable
        self.close_output_file()

    def compute_last_closed_flux_surface(self):
        "Computes the coordinates of the last closed flux surface"
        print("Computing LCFS...")
        self.load_dataset()
        self.load_input_file()
        # Default num=50 is too much data to fit in memory so use smaller
        # number, e.g. 20
        theta = np.linspace(-np.pi, np.pi, num=20)
        zeta = theta  # Toroidal angle (we just need a grid so re-use theta)
        rmaj = self.input_file['theta_grid_parameters']['rmaj']  # Major radius
        a = 1.  # By definition since we normalise to 'a' (minor radius)
        Theta, Zeta = np.meshgrid(theta, zeta)
        # Reshape to 3D so can easily output to VTK
        shape = list(Theta.shape)  # Convert to list so we can append 1
        shape.append(1)
        Theta = Theta.reshape(tuple(shape))  # Convert to tuple for rehsape
        shape = list(Zeta.shape)  # Convert to list so we can append 1
        shape.append(1)
        Zeta = Zeta.reshape(tuple(shape))  # Convert to tuple for rehsape
        # Compute coordinates of torus
        R_torus = rmaj + a * np.cos(Theta)
        X_torus = R_torus * np.cos(Zeta)
        Y_torus = R_torus * np.sin(Zeta)
        Z_torus = a * np.sin(Theta)
        return theta, zeta, R_torus, X_torus, Y_torus, Z_torus

    def write_last_closed_flux_surface(self):
        """Writes the coordinates of the last closed flux surface to the NetCDF
        file"""
        print("Writing LCFS...")
        self.open_output_file_for_writing()
        # TODO - avoid doing this twice!
        theta, zeta, R_torus, X_torus, Y_torus, Z_torus = \
            self.compute_last_closed_flux_surface()
        # Create dimensions / vazriables and set values / attributes
        # Assign values using [...] notation as that is what netCDF4 requires
        # in order to correctly set / get values of a variable.
        # theta_torus
        print("theta_torus", end="")
        self.output_file.createDimension('theta_torus', len(theta))
        self.output_file.createVariable('theta_torus', 'd',  # 'd' = double
                                        dimensions=('theta_torus'))
        self.output_file.variables['theta_torus'][:] = theta
        self.output_file.variables['theta_torus'].description = "Poloidal " + \
            "angle grid used to compute points on the last closed flux " + \
            "surface"
        # zeta_torus
        print(", zeta_torus", end="")
        self.output_file.createDimension('zeta_torus', len(zeta))
        self.output_file.createVariable('zeta_torus', 'd',  # 'd' = double
                                        dimensions=('zeta_torus'))
        self.output_file.variables['zeta_torus'][:] = zeta
        self.output_file.variables['zeta_torus'].description = "Toroidal " + \
            "angle grid used to compute points on the last closed flux " + \
            "surface"
        # r_torus
        print(", r_torus", end="")
        self.output_file.createDimension('r_torus', 1)
        self.output_file.createVariable('r_torus', 'd',  # 'd' = double
                                        dimensions=('r_torus'))
        self.output_file.variables['r_torus'][:] = 1.0
        self.output_file.variables['r_torus'].units = 'L_ref'
        self.output_file.variables['r_torus'].description = "Minor radius " + \
            "of the last closed flux surface"
        # R_torus
        print(", R_torus", end="")
        self.output_file.createVariable('R_torus', 'd',  # 'd' = double
                                        dimensions=('theta_torus',
                                                    'zeta_torus', 'r_torus'))
        self.output_file.variables['R_torus'][:, :] = R_torus
        self.output_file.variables['R_torus'].units = 'L_ref'
        self.output_file.variables['R_torus'].description = "Major radius " + \
            "positions of points on the last closed flux surface"
        # X_torus
        print(", X_torus", end="")
        self.output_file.createVariable('X_torus', 'd',  # 'd' = double
                                        dimensions=('theta_torus',
                                                    'zeta_torus', 'r_torus'))
        self.output_file.variables['X_torus'][:, :] = X_torus
        self.output_file.variables['X_torus'].units = 'L_ref'
        self.output_file.variables['X_torus'].description = "Cartesian x " + \
            "positions (e.g. East-West position) of points on the last " + \
            "closed flux surface"
        # Y_torus
        print(", Y_torus", end="")
        self.output_file.createVariable('Y_torus', 'd',  # 'd' = double
                                        dimensions=('theta_torus',
                                                    'zeta_torus', 'r_torus'))
        self.output_file.variables['Y_torus'][:, :] = Y_torus
        self.output_file.variables['Y_torus'].units = 'L_ref'
        self.output_file.variables['Y_torus'].description = "Cartesian y " + \
            "positions (e.g. North-South position) of points on the last " + \
            "closed flux surface"
        # Z_torus
        print(", Z_torus")
        self.output_file.createVariable('Z_torus', 'd',  # 'd' = double
                                        dimensions=('theta_torus',
                                                    'zeta_torus', 'r_torus'))
        self.output_file.variables['Z_torus'][:, :] = Z_torus
        self.output_file.variables['Z_torus'].units = 'L_ref'
        self.output_file.variables['Z_torus'].description = "Height of " + \
            "points on the last closed flux surface"
        self.close_output_file()


class Transform:
    "Class to hold data about a transform"

    def __init__(self, name, array, dims):
        """Initialise the transform
        Inputs:
            - name - string - the name of transform, e.g. for saving to NetCDF
            - array - numpy array - the data of the transform
            - dims - list of strings - the names of the dimensions of the data
        NB: len(dims) must equal len(array.shape)"""
        self.name = name
        self.array = array
        self.dims = dims

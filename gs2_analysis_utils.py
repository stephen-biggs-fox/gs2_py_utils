"""
Python module containing utility functions for analysing GS2 data

Copyright 2016 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np

from snbf_py_utils.general import remove_inf_and_nan
from snbf_py_utils.general import extract_subarray_via_logical_index

from gs2_plot_utils import plot_frequency_vs_time
from gs2_plot_utils import plot_basic_growth_rate_fit

from GrowthRateMeasurer import GrowthRateMeasurer  # import class
# import utility function from class GrowthRateMeasurer
from GrowthRateMeasurer import _measure_growth_rate_via_fit
# import as GRM_... to avoid function name clash
from GrowthRateMeasurer import _gradient_to_growth_rate as GRM_gradient_to_growth_rate
from GrowthRateMeasurer import _get_gs2_growth_rate as GRM_get_gs2_growth_rate
from GrowthRateMeasurer import _extract_final_20_percent as GRM_extract_final_20_percent
from GrowthRateMeasurer import _measure_gs2_value as GRM_measure_gs2_value


# ------------------------ General -----------------------------------

def extract_final_20_percent(array):
    'Extracts the final 20% of an array'
    # Delegate to GrowthRateMeasurer to prevent circular dependency
    return GRM_extract_final_20_percent(array)


def measure_gs2_value(array):
    """Measures the value of the given data assuming that it is a
    converging time history from GS2 and thus averaging over the final
    20%"""
    # Delegate to GrowthRateMeasurer to prevent circular dependency
    return GRM_measure_gs2_value(array)

# ------------------------ Frequency ---------------------------------


def measure_frequency(time, gs2_frequency_array, run_name, plot=False):
    'Measures frequency with errors from the gs2 measured values'
    time, gs2_frequency_array = remove_inf_and_nan_both(
        time, gs2_frequency_array)
    frequency, frequency_error, frequency_dps = measure_gs2_value(
            gs2_frequency_array)
    if plot:
        plot_frequency_vs_time(
            time, gs2_frequency_array, frequency, frequency_error,
            frequency_dps, run_name)
    return frequency, frequency_error, frequency_dps

# ------------------------ Growth rate -------------------------------


def measure_growth_rate(time, phi2, gs2_gamma_array, run_name='', plot=False,
                        supress_printing=False):
    """Measures the growth rate including error via linear fit between
    time and log(phi2).
    Inputs:
    - time: array of time data
    - phi2: array of potential squared data (same size as time)
    - gs2_gamma_array: array of growth rate data as measured directly
            by GS2 (same size as time)
    - run_name: the name of this run (used for labelling plots and
            output files)
    - plot: if false or omitted, no plots are produced; if true,
            intermediate plots of the basic fit and any GS2 value or
            improved fits are produced
    Outputs:
    - gamma: growth rate
    - gamma_error: estimate of uncertainty in the value of gamma
    - gamma_dps: the number of decimal places in gamma
    """
    # Remove any inf and nan elements first
    time, phi2, gs2_gamma_array = remove_inf_and_nan_all(
            time, phi2, gs2_gamma_array)
    # Compute log(phi2) here to avoid repeated computation
    try:
        with np.errstate(divide='raise'):
            log_phi2 = np.log(phi2)
    except FloatingPointError:
        return 0., 0., 1
    # Check array is long enough for analysis to continue
    if len(time) > 4:
        # Start with a basic fit that includes dodgy data at the start
        gamma, gamma_error, gamma_dps = fit_including_all_data(
                time, log_phi2, run_name, plot)
        # Try to improve the fit if possible
        growing = phi2[0] < phi2[-1]  # Final value larger than initial value
        if growing:
            measurer = GrowthRateMeasurer(
                time, phi2, log_phi2, gs2_gamma_array, run_name, plot,
                supress_printing)
            gamma, gamma_error, gamma_dps = \
                measurer.fit_excluding_initial_data()
        else:
            if not supress_printing:
                print('WARNING: Dealing with decaying mode amplitudes' + \
                    'not implemented yet. Using rough fit to all data.')
    else:
        if not supress_printing:
            print('WARNING: Not enough data to perform reliable fit.' + \
                ' Fit error estimated manually from fit to all data.')
        gamma, gamma_error, gamma_dps = fit_including_all_data(
                time, log_phi2, run_name, plot)
    return gamma, gamma_error, gamma_dps


def remove_inf_and_nan_both(time, gs2_omega_array):
    """Removes inf and nan elements from gs2_omega_array and trims
    time accordingly"""
    gs2_omega_array, logical_index = remove_inf_and_nan(gs2_omega_array)
    time = extract_subarray_via_logical_index(time, logical_index)
    return time, gs2_omega_array


def remove_inf_and_nan_all(time, phi2, gs2_gamma_array):
    """Removes inf and nan elements from phi2 and trims other arrays
    accordingly"""
    phi2, logical_index = remove_inf_and_nan(phi2)
    time = extract_subarray_via_logical_index(time, logical_index)
    gs2_gamma_array = extract_subarray_via_logical_index(
            gs2_gamma_array, logical_index)
    return time, phi2, gs2_gamma_array


def fit_including_all_data(time, log_phi2, run_name='', plot=False):
    """Performs a basic fit that includes all data and plots it if
    requested"""
    gamma, gamma_error, gamma_dps, coeffs, coeffs_errors, coeffs_dps = \
        _measure_growth_rate_via_fit(time, log_phi2)
    if plot:
        plot_basic_growth_rate_fit(
                time, log_phi2, coeffs, coeffs_errors, coeffs_dps, run_name)
    return gamma, gamma_error, gamma_dps


def gradient_to_growth_rate(in_val, reverse=False):
    """Converts a log(phi2) vs time gradient to a growth rate or visa
    versa using growth rate = gradient / 2.
    Inputs:
    - in_val: gradient or growth rate
    - reverse: if False or omitted, in_val is gradient and answer is
      growth rate; if True, in_val is growth rate and answer is
      gradient
    Outputs:
    - answer: growth rate or gradient"""
    # Delegate to GrowthRateMeasurer to prevent circular dependency
    return GRM_gradient_to_growth_rate(in_val, reverse)


def get_gs2_growth_rate(time, gs2_gamma_array, run_name='', plot=False):
    'Gets the GS2 value of growth rate and plots it if required'
    # Delegate to GrowthRateMeasurer to prevent circular dependency
    return GRM_get_gs2_growth_rate(time, gs2_gamma_array, run_name, plot)

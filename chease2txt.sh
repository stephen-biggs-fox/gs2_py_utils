#! /bin/bash
# Script to turn elite text file into separate text files per field

# Check input file specified
if [ -z "$1" ]
then
    echo "You must specify an input file"
    exit 1
fi
infile=${1}.unix

# Convert file format
dos2unix -n $1 $infile

# Remove trailing whitespace - necessary to ensure correct matching later
echo "Removing trailing whitespace..."
sed -i 's/[[:blank:]]*$//' $infile

# Find field names
i=0
for f in $(grep '^[A-Za-z]' $infile)
do
    fields[$i]=$f  # zero-based array
    i=$((i+1))
done

# Add an end of file (EOF) marker so that final field can be handled correctly
eof_string="deleteme"
fields[$i]=$eof_string
echo $eof_string >> $infile

# Get folder
folder=$(dirname $1)

# Loop over fields
for j in $(seq 1 1 $i)  # next field
do
    k=$((j-1))  # current field
    # Print something for monitoring purposes
    echo "Getting values for ${fields[$k]}"
    # Get start and end line numbers
    # This is necessary as we can't delete directly from match becuase, if the match is on the same
    # line as the other end of the delete range, then the whole file gets deleted
    begin=$(cat $infile | grep -n "^${fields[$k]}$" | cut -d ':' -f 1)
    finish=$(cat $infile | grep -n "^${fields[$j]}$" | cut -d ':' -f 1)
    # Collect values for current field
    outfile=$folder/${fields[$k]}.txt
    sed -e "1,${begin}d" -e "${finish},"'$d' $infile > $outfile
    # Put them all in one column for easy reading by numpy.loadtxt
    perl -pe "s/[ ]+/\n/g" $outfile | sed -e '/^$/d' > tmp
    cat tmp > $outfile
done
rm tmp

#! /usr/bin/env python3
"""
Script / module for producing standard plots from nonlinear GS2 runs

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2_py_utils.

gs2_py_utils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2_py_utils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2_py_utils.  If not, see http://www.gnu.org/licenses/.
"""


# Imports
# matplotlib first to set backend before other imports
import warnings
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import argparse
import os
from Gs2Dataset import Gs2Dataset
import scipy.integrate
import snbf_py_utils.plotting as mpu
import gs2_axes_labels as gal
import seaborn as sns
sns.set(font_scale=2)


class NonlinearGs2File:

    def __init__(self, filenames, write=False, show=True,
                 averagingStartTime=None):
        self.dataset = Gs2Dataset(filenames)
        # Shift kx and ky sfor use with pcolormesh as kx and ky are centres of
        # pixes but pcolormesh required corners
        self.kxShifted = mpu.shift_axis_for_pcolormesh(
            np.fft.fftshift(self.dataset['kx']))
        self.kyShifted = mpu.shift_axis_for_pcolormesh(self.dataset['ky'])
        self.defaultWrite = write
        self.defaultShow = show
        self.finalSnapshotTitle = 't = %.1f' % self.dataset['t'][-1]
        self.defaultAveragingStartTime = averagingStartTime

    def setDisplayOptions(self, write, show):
        self.currentWrite = write
        self.currentShow = show

    def displayPlot(self, filename):
        if self.currentWrite is None:
            write = self.defaultWrite
        else:
            write = self.currentWrite
        if write:
            plt.savefig(filename, bbox_inches='tight')
        if self.currentShow is None:
            show = self.defaultShow
        else:
            show = self.currentShow
        if show:
            plt.tight_layout()
            plt.show()
        if write and not show:
            plt.clf()

    def plot_phi2_vs_t(self, ylimmax=None, marker=None, write=None, show=None,
                       isLogScale=False, averagingStartTime=None):
        meanPhi2 = None
        self.setDisplayOptions(write, show)
        self.setupAveraging(averagingStartTime)
        # Linear scale
        plt.plot(self.dataset['t'], self.dataset['phi2'], marker=marker)
        if self.doAveraging:
            timeForAveragingPeriod = \
                self.dataset['t'][self.averagingStartIndex:]
            meanPhi2 = (scipy.integrate.simps(
                self.dataset['phi2'][self.averagingStartIndex:],
                x=timeForAveragingPeriod) /
                (timeForAveragingPeriod[-1] - timeForAveragingPeriod[0])).data
            meanPhi2Array = np.ones_like(timeForAveragingPeriod) * meanPhi2
            plt.plot(timeForAveragingPeriod, meanPhi2Array)
            plt.title(r'$\bar\phi^2$ = %.1f' % meanPhi2)
        plt.xlabel(gal.axes_labels['t'])
        plt.ylabel(gal.axes_labels['phi2'])
        if ylimmax is not None:
            ylim = list(plt.gca().get_ylim())
            ylim[1] = ylimmax
            plt.ylim(ylim)
        filename = 'phi2-vs-t.png'
        if isLogScale:
            ax = plt.gca()
            ax.set_yscale('log')
            filename = 'log-' + filename
        self.displayPlot(filename)
        return meanPhi2

    def setupAveraging(self, averagingStartTime):
        averagingDisabledKeyword = 'off'
        self.doAveraging = (averagingStartTime != averagingDisabledKeyword and
                            not (averagingStartTime is None and
                                 self.defaultAveragingStartTime ==
                                 averagingDisabledKeyword))
        if self.doAveraging:
            isDefault = (averagingStartTime is None and
                         self.defaultAveragingStartTime is None)
            if isDefault:
                self.averagingStartIndex = -10
            else:
                if averagingStartTime is None:
                    averagingStartTime = self.defaultAveragingStartTime
                self.averagingStartIndex = np.argmin(np.abs(
                    self.dataset['t'] - averagingStartTime)).data
            self.smoothedTitle = \
                't = %.1f to %.1f' % (
                    self.dataset['t'][self.averagingStartIndex],
                    self.dataset['t'][-1])

    def plot_phi2_by_mode(self, write=None, show=None,
                          averagingStartTime=None, plot_log=True,
                          plot_linear=False):
        """Plot phi2_by_mode
        Inputs:
            - write - boolean indicating whether to save the figure to disk. If
            None, use the default option set when the object was instantiated.
            - show - boolean indicating whether to display the figure on
            screen. If None, use the default option set when the object was
            instantiated.
            - averagingStartTime - simulation time at which to start averaging.
            If None, use the default option set when the object was
            instantiated. If 'off', then do not do any averaging - just plot
            the unsmoothed spectrum (final snapshot).
            - plot_log - boolean indicating whether to include the log scale
            plot
            - plot_linear - boolean indicating whether to include the linear
            scale plot
            """
        self.setupAveraging(averagingStartTime)
        self.setDisplayOptions(write, show)
        # Re-order kx from FFTW order to ascending order
        phi2_by_mode = np.fft.fftshift(self.dataset['phi2_by_mode'], axes=2)
        # Last time snapshot
        if plot_linear:
            self.plotSequentialDataByMode(phi2_by_mode[-1, :, :],
                                          gal.axes_labels['phi2'],
                                          self.finalSnapshotTitle,
                                          'phi2_by_mode.png')
            # Average over last few time snapshots
            if self.doAveraging:
                phi2_by_mode_smoothed = np.mean(
                    phi2_by_mode[self.averagingStartIndex:, :, :], axis=0)
                self.plotSequentialDataByMode(phi2_by_mode_smoothed,
                                              gal.axes_labels['phi2'],
                                              self.smoothedTitle,
                                              'smooth-phi2_by_mode.png')
        # log(phi2) versions of the above
        # Ignore warnings for log versions as (kx, ky) = (0, 0) = 0 and log(0)
        # raises a warning
        if plot_log:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                log_phi2_by_mode = np.log(phi2_by_mode[-1, :, :])
            logPhi2Label = r'$\ln[' + gal.dphi + r'^2]$'
            self.plotSequentialDataByMode(log_phi2_by_mode, logPhi2Label,
                                          self.finalSnapshotTitle,
                                          'log-phi2_by_mode.png')
            if self.doAveraging:
                with warnings.catch_warnings():
                    log_smooth_phi2_by_mode = np.log(phi2_by_mode_smoothed)
                self.plotSequentialDataByMode(log_smooth_phi2_by_mode,
                                              logPhi2Label,
                                              self.smoothedTitle,
                                              'log-smooth-phi2_by_mode.png')

    def plotSequentialDataByMode(self, data, label, title, filename):
        self.plotSequentialData(self.kxShifted, self.kyShifted, data,
                                gal.axes_labels['kx'], gal.axes_labels['ky'],
                                label, title, filename)

    def plotSequentialData(self, x, y, z, xlabel, ylabel, zlabel, title,
                           filename):
        self.makeContourPlot(x, y, z, 'Reds', xlabel, ylabel, zlabel, title,
                             filename)

    def plot_phi_vs_theta(self, kx0=False, write=None, show=None):
        self.setDisplayOptions(write, show)
        # Find largest phi2 from phi2_by_mode (hence probably [one of the]
        # fastest growing mode[s])
        if kx0:
            ikx = 0
            final_phi2_by_mode = self.dataset['phi2_by_mode'][-1, :, ikx]
            iky = np.argmax(final_phi2_by_mode)
        else:
            final_phi2_by_mode = self.dataset['phi2_by_mode'][-1, :, :]
            iphi2max = np.argmax(final_phi2_by_mode)
            iky, ikx = np.unravel_index(iphi2max, final_phi2_by_mode.shape)
        # Plot phi vs theta
        theta = self.dataset['theta']
        phi = np.ndarray(len(theta), dtype=complex)
        phi.real = self.dataset['phi'][iky, ikx, :, 0]
        phi.imag = self.dataset['phi'][iky, ikx, :, 1]
        plt.plot(theta / np.pi, np.abs(phi))
        plt.xlabel(gal.axes_labels['theta'])
        plt.ylabel(gal.axes_labels['abs(phi)'])
        plt.title('kx = %6.2f, ky = %6.2f' % (self.dataset['kx'][ikx],
                                              self.dataset['ky'][iky]))
        self.displayPlot('phi-vs-theta.png')

    def plotDensity(self):
        # Plot density in k-space
        # FIXME - density plot does not correctly project onto poloidal plane
        # (ask DD how to fix this)
        warnings.warn("WARNING: density plot not implemented correctly. " +
                      "Do not use.")
        # Prepare to use kx and ky
        # Don't fftshift kx here as this is only used to get nx and Delta_kx so
        # needs to be in FFTW order. The ks used for the plots is in
        # self.kxShifted which is already subject to fftshift.
        kx = self.dataset['kx']
        ky = self.dataset['ky']
        nx = len(kx)
        ny = len(ky)
        # Plot real and imag density and log(density) by mode
        density_raw = self.dataset['density0'][-1, 0, :, :, :]
        density = np.ndarray(density_raw[:, :, 0].shape, dtype=complex)
        density.real = density_raw[:, :, 0]
        density.imag = density_raw[:, :, 1]
        self.plotDivergingDataByMode(np.fft.fftshift(density.real, axes=1),
                                     gal.axes_labels['Re[hat n]'],
                                     self.finalSnapshotTitle,
                                     'density_by_mode_real.png')
        self.plotDivergingDataByMode(np.fft.fftshift(density.imag, axes=1),
                                     gal.axes_labels['Im[hat n]'],
                                     self.finalSnapshotTitle,
                                     'density_by_mode_imag.png')
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            log_density_real = np.fft.fftshift(
                np.log(np.square(density.real)), axes=1)
            log_density_imag = np.fft.fftshift(
                np.log(np.square(density.imag)), axes=1)
        self.plotSequentialDataByMode(log_density_real,
                                      r'$\ln[\Re[' + gal.hat_n + r']^2]$',
                                      self.finalSnapshotTitle,
                                      'log_density_by_mode_real.png')
        self.plotSequentialDataByMode(log_density_imag,
                                      r'$\ln[\Im[' + gal.hat_n + r']^2]$',
                                      self.finalSnapshotTitle,
                                      'log_density_by_mode_imag.png')
        # Inverse FFT to plot density in real space
        # Fill in negative ky with complex conjugate of positive ky values
        # (which I believe is the correct symmetry condition that allows us to
        # only store positive kys in teh first place)
        density_for_fft = np.ndarray((nx, 2 * ny - 1), dtype=complex)
        # itheta0 not used so commented out. Can't remember why I thought I
        # needed that...
        # itheta0 = np.argmin(np.abs(data['theta']))
        # GS2 order is ky, kx but I want kx, ky, so transpose
        density_for_fft[:, :ny] = density.T
        # Compute complex conjugates
        density_for_fft[:, ny:] = -1j * (density[-1:0:-1, :]).T
        # Do inverse FFT
        density_real_space = np.fft.ifft2(density_for_fft)
        # Compute real space x and y grids
        Delta_kx = kx[1]
        Lx = 2. * np.pi / Delta_kx
        x = np.linspace(-Lx / 2., Lx / 2., num=nx)
        Delta_ky = ky[1]
        Ly = 2. * np.pi / Delta_ky
        y = np.linspace(-Ly / 2., Ly / 2., num=(2 * ny - 1))
        x = mpu.shift_axis_for_pcolormesh(x)
        y = mpu.shift_axis_for_pcolormesh(y)
        # Real component plot
        self.plotDivergingDataRealSpace(x, y, density_real_space.real,
                                        r'$\Re[n]\;/\;n_{ref}$',
                                        self.finalSnapshotTitle,
                                        'density_real_space_real.png')
        self.plotDivergingDataRealSpace(x, y, density_real_space.imag,
                                        r'$\Im[n]\;/\;n_{ref}$',
                                        self.finalSnapshotTitle,
                                        'density_real_space_imag.png')

    def plotDivergingDataByMode(self, data, label, title, filename):
        self.plotDivergingData(self.kxShifted, self.kyShifted, data,
                               gal.axes_labels['kx'], gal.axes_labels['ky'],
                               label, title, filename)

    def plotDivergingDataRealSpace(self, x, y, data, label, title, filename):
        self.plotDivergingData(x, y, data, gal.axes_labels['x'],
                               gal.axes_labels['y'], label, title, filename)

    def plotDivergingData(self, x, y, z, xlabel, ylabel, zlabel, title,
                          filename):
        # Fit diverging colormap to data
        cmap = cm.get_cmap('RdBu_r')
        cmap = mpu.shift_colormap(cmap, z)
        # Make plot
        self.makeContourPlot(x, y, z, cmap, xlabel, ylabel, zlabel, title,
                             filename)

    def makeContourPlot(self, x, y, z, cmap, xlabel, ylabel, zlabel, title,
                        filename):
        plt.pcolormesh(x, y, z, cmap=cmap)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.colorbar(label=zlabel)
        plt.title(title)
        self.displayPlot(filename)


class NonlinearAnalysisSessionArgumentParser:

    def __init__(self):
        self.createParser()
        self.addArguments()
        self.parseArguments()
        self.setDefaultOptions()

    def createParser(self):
        self.parser = argparse.ArgumentParser(
            description="Analyse a nonliear GS2 run")

    def addArguments(self):
        self.parser.add_argument("-d", "--directory",
                                 help="Directory in which the data file " +
                                 "to check can be found")
        self.parser.add_argument("-f", "--filename",
                                 help="Filename to operate on")
        self.parser.add_argument("-Y", "--ylimmax", type=float,
                                 help="Maximum y-axis limit")
        self.parser.add_argument("-m", "--marker", action='store_true',
                                 help="Plot markers")
        self.parser.add_argument("-k", "--kx0", action='store_true',
                                 help="Enforce kx = 0 for mode " +
                                 "structure selection")
        self.parser.add_argument("-r", "--restart",
                                 help="Folder prefix of restart data " +
                                 "(e.g. part => part-1, part-2, etc)")
        self.parser.add_argument("-v", "--verbose", action='store_true',
                                 help="Print extra information")
        self.parser.add_argument("-n", "--density", action='store_true',
                                 help="Plot density perturbation in real " +
                                 "space (currently incorrect)")
        self.parser.add_argument("-s", "--show", action='store_true',
                                 help="Display plots on screen")
        self.parser.add_argument("-w", "--write", action='store_false',
                                 help="Save images to file")
        self.parser.add_argument("-a", "--avg_time",
                                 help="Time from which to average " +
                                 "(None = default, 'off' = disable)")

    def parseArguments(self):
        self.arguments = self.parser.parse_args()

    def setDefaultOptions(self):
        if self.arguments.directory is None:
            self.arguments.directory = '.'
        if self.arguments.filename is None:
            self.arguments.filename = 'input.out.nc'
        if self.arguments.marker:
            self.arguments.marker = '.'
        else:
            self.arguments.marker = None

    def getArguments(self):
        return self.arguments


class NonlinearAnalysisSession:

    def run(self, **kwargs):
        self.parseArguments(**kwargs)
        self.readData()
        self.data.plot_phi2_vs_t(ylimmax=self.arguments.ylimmax,
                                 marker=self.arguments.marker)
        self.data.plot_phi2_vs_t(ylimmax=self.arguments.ylimmax,
                                 marker=self.arguments.marker, isLogScale=True)
        self.data.plot_phi2_by_mode()
        self.data.plot_phi_vs_theta(kx0=self.arguments.kx0)
        if self.arguments.density:
            self.data.plotDensity()

    def parseArguments(self, **kwargs):
        argumentParser = NonlinearAnalysisSessionArgumentParser()
        self.arguments = argumentParser.getArguments()
        if kwargs is not None:
            for key in kwargs:
                try:
                    self.arguments.__dict__[key] = kwargs[key]
                except(KeyError):
                    warnings.warn('Unrecognized kwarg "' + key + '"')

    def readData(self):
        if self.arguments.restart is None:
            filenames = os.path.join(self.arguments.directory,
                                     self.arguments.filename)
        else:
            warnings.warn("WARNING: restart option currently " +
                          "only deals with a single restart")
            filenames = [os.path.join(self.arguments.directory,
                                      self.arguments.restart,
                                      self.arguments.filename),
                         os.path.join(self.arguments.directory,
                                      self.arguments.filename)]
        self.data = \
            NonlinearGs2File(filenames, show=self.arguments.show,
                             write=self.arguments.write,
                             averagingStartTime=self.arguments.avg_time)


if __name__ == "__main__":
    session = NonlinearAnalysisSession()
    session.run()

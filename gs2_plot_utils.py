"""
Python module containing utility functions for plotting GS2 data

Copyright 2016 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

from matplotlib.pyplot import clf, plot, legend, ylabel, xlabel, gca
from matplotlib.pyplot import errorbar

from numpy import where, array, pi

from snbf_py_utils.plotting import my_savefig, plot_data_fit_and_errors

from snbf_py_utils.general import get_sign

# --------------------- Module variables -----------------------------

unit_sep = r'\;/\;'
frequency_units = r'v_{th}/L_{ref}'
frequency_label = r'$\omega_0' + unit_sep + frequency_units + '$'
growth_rate_label = r'$\gamma_0' + unit_sep + frequency_units + '$'
toroidal_mode_number_label = r'$n$'
xi_units = r'(T_{ref}\rho_{ref})/(q_{ref}L_{ref})'
xi_label = r'$|\delta\phi_0|$'

label_size = 30
# tick_size = 18  # set elsewhere, not required here
# legend_size = 12  # default so not required

# --------------------- Functions ------------------------------------


def plot_basic_growth_rate_fit(time, log_phi2, coeffs, coeffs_errors,
                               coeffs_dps, run_name):
    'Plot fit between time and log(phi2) including all data'
    plot_log_phi2_vs_time(time, log_phi2, coeffs, coeffs_errors, coeffs_dps)
    my_savefig(run_name + '-rough-fit.png')
    clf()


def plot_improved_growth_rate_fit(time, log_phi2, coeffs, coeffs_errors,
                                  coeffs_dps, run_name, first_index,
                                  is_end_fit):
    'Plot fit between time and log(phi2) marking data inclusion cut-offs'
    plot_log_phi2_vs_time(time, log_phi2, coeffs, coeffs_errors, coeffs_dps)
    if not is_end_fit:
        _mark_data_inclusion_cutoffs(time, log_phi2, first_index)
    _mark_final_20_percent(time, log_phi2)
    my_savefig(run_name + '-improved-fit.png')
    clf()


def _mark_data_inclusion_cutoffs(time, log_phi2, first_index):
    'Marks the data inclusion cutoffs, e.g. initial value, +100%, etc.'
    # Mark first value
    plot([min(time), max(time)], [log_phi2[0], log_phi2[0]], 'm--')
    # Mark first value plus (j+1)*100%
    plot(
        [min(time), max(time)], [log_phi2[first_index],
                                 log_phi2[first_index]], 'k--')
    # Mark data inclusion cut-off
    plot(
        [time[first_index], time[first_index]],
        [min(log_phi2), max(log_phi2)], 'k--')


def plot_log_phi2_vs_time(time, log_phi2, coeffs, coeffs_errors, coeffs_dps):
    'Plot fit between time and log(phi2)'
    plot_data_fit_and_errors(time, log_phi2, coeffs, coeffs_errors)
    _add_fit_legend(coeffs, coeffs_errors, coeffs_dps)
    _label_log_phi2_vs_time()


def _add_fit_legend(coeffs, coeffs_errors, coeffs_dps):  # private
    """Adds a legend that contains the fit details assuming a fit
    between time and log(phi2)"""
    # Get sign of y-intercept for use in legend string
    sign = get_sign(coeffs[1])
    # Get gradient (m) and y-intercept (c)
    m, m_err, m_dps = coeffs[0], coeffs_errors[0], coeffs_dps[0]
    c, c_err, c_dps = coeffs[1], coeffs_errors[1], coeffs_dps[1]
    # Add legend
    legend(['data', (
        'fit: ln[$\delta\phi_0^2$] = (%.' + str(m_dps) + 'f $\pm$ %.' +
        str(m_dps) + r'f)$t$ ' + sign + ' (%.' + str(c_dps) + 'f $\pm$ %.' +
        str(c_dps) + 'f)') % (m, m_err, abs(c), c_err)], 'best')


def _label_log_phi2_vs_time():  # private
    'Labels the axes as log(phi2) vs time'
    label_time()
    ylabel(r'$\ln[\delta\phi_0^2]$', fontsize=label_size)


def label_time():
    'Labels the x-axis as time'
    xlabel(r'$t\;/\;L_{ref}/v_{th}$', fontsize=label_size)


def _mark_final_20_percent(time, ydata):  # private
    'Marks on the plot the cut-off for the final 20% region'
    plot([time[-len(time)//5], time[-len(time)//5]], [min(ydata),
         max(ydata)], 'g--')


def plot_gs2_value(time, ydata, mean, error, dps, ylabel_string, ydata_name,
                   run_name):
    """Plots the given data and the given mean value with error bounds assuming
    that it is a converging time history from GS2"""
    # Plot data and fit
    plot_data_fit_and_errors(time, ydata, array([mean]), array([error]))
    label_time()
    ylabel(ylabel_string, fontsize=label_size)
    _mark_final_20_percent(time, ydata)
    legend(['data', ('fit: $\\' + ydata_name + '_0$ = (%.' + str(dps) +
            'f $\pm$ %.' + str(dps) + 'f)') % (mean, error)], loc='best')
    my_savefig(run_name + '-gs2-' + ydata_name + '-convergence.png')
    clf()


def plot_normalised_phi_vs_theta(phi, theta, run_name):
    'Plots normalised phi against theta.'
    index_of_peak = where(abs(phi) == max(abs(phi)))[0][0]
    phi_norm = phi[index_of_peak]
    # Plot (the real component of) complex phi divided by complex
    # phi_norm so that the plotted value at the peak is exactly 1 and
    # exactly real (maybe allowing for valid comparison between
    # simulations.)
    plot(theta, (phi/phi_norm).real, '-kx')
    label_theta()
    ylabel(r'$\Re (\delta\phi_0 / \delta\phi_0|_{peak})$', fontsize=label_size)
    my_savefig(run_name + '-phi-vs-theta-norm.png')
    clf()


def label_theta():
    'Labels the x-axis as theta'
    xlabel(r'$\eta$', fontsize=label_size)


def plot_abs_phi_vs_theta(phi, theta, run_name):
    'Plots magnitude of phi against theta.'
    plot(theta, abs(phi)/max(abs(phi)), '-kx')
    label_theta()
    ylabel(xi_label, fontsize=label_size)
    ax = gca()
    ax.set_xticks([-6*pi, -4*pi, -2*pi, 0., 2*pi, 4*pi, 6*pi])
    ax.set_xticklabels([r'$-6\pi$', r'$-4\pi$', r'$-2\pi$', r'$0$',
                        r'$2\pi$', r'$4\pi$', r'$6\pi$'])
    plot([-abs(2. * min(theta)), abs(2. * max(theta))], [0., 0.])
    ax.set_xlim((-6*pi, 6*pi))
    ax.set_ylim((0., 1.))
    my_savefig(run_name + '-phi-vs-theta-abs.png')
    clf()


def plot_frequency_vs_time(time, gs2_frequency_array, frequency,
                           frequency_error, frequency_dps, run_name):
    'Plots frequency convergence history and measured value with error bounds'
    plot_gs2_value(
        time, gs2_frequency_array, frequency, frequency_error, frequency_dps,
        frequency_label, 'omega', run_name)


def plot_frequency_vs_mode_num(mode_num, frequency, frequency_error):
    'Plots frequency against mode number with error bars'
    plot_against_mode_num(
            mode_num, frequency, frequency_error,
            frequency_label, 'omega')


def plot_growth_rate_vs_mode_num(mode_num, growth_rate, growth_rate_error):
    'Plots growth rate against mode number with error bars'
    plot_against_mode_num(
            mode_num, growth_rate, growth_rate_error,
            growth_rate_label, 'gamma')


def plot_against_mode_num(mode_num, ydata, yerr, ylabel_string, ydata_name):
    'Plots the given ydata against mode number with error bars'
    plot_multiple_runs(
        mode_num, ydata, yerr, toroidal_mode_number_label, 'n0', ylabel_string,
        ydata_name)


def plot_multiple_runs(xdata, ydata, yerr, xlabel_string, xdata_name,
                       ylabel_string, ydata_name):
    'Plots the given ydata against the given xdata with error bars'
    errorbar(xdata, ydata, yerr=yerr, fmt='-kx')
    xlabel(xlabel_string, fontsize=label_size)
    ylabel(ylabel_string, fontsize=label_size)
    my_savefig(ydata_name + '-vs-' + xdata_name + '.png')
    clf()


def plot_frequency(xdata, frequency, frequency_error, xlabel_string,
                   xdata_name):
    'Plots frequency against the given xdata with errorbars'
    plot_multiple_runs(
            xdata, frequency, frequency_error, xlabel_string, xdata_name,
            frequency_label, 'omega')


def plot_growth_rate(xdata, growth_rate, growth_rate_error, xlabel_string,
                     xdata_name):
    'Plots growth rate against the given xdata with errorbars'
    plot_multiple_runs(
            xdata, growth_rate, growth_rate_error, xlabel_string, xdata_name,
            growth_rate_label, 'gamma')

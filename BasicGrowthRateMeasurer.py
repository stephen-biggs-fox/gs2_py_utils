"""
This module contains the GrowthRateMeasurer class

Copyright 2016 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np

from snbf_py_utils.stats import polyfit_with_errors, round_val_to_err
from snbf_py_utils.stats import compute_mean_and_std_err

from gs2_plot_utils import plot_improved_growth_rate_fit

# ----------------------- Utility functions ---------------------------


def _extract_final_20_percent(array):  # package-private
    'Extracts the final 20% of an array'
    # Ensure a minimum of 5 data points to prevent divide by zero error when
    # computing the covariance matrix during polyfit
    return _extract_subarray_min_5_points(array, -int(len(array)/5))


def _extract_subarray_min_5_points(array, negative_first_index):  # private
    """Extracts a subarray from the end of array starting at first_index or
    earlier if necessary to ensure a minumum of 5 data points.
    Inputs:
    - array = the array from which to extract the subarray
    - negative_first_index = the number of points minus 1 to be extracted (i.e.
      the number of points back in from the end to just before the start of the
      subarray)"""
    return array[min(-6, negative_first_index):len(array)]


def _measure_gs2_value(array):  # package-private
    """Measures the value of the given data assuming that it is a converging time
    history from GS2 and thus averaging over the final 20%"""
    # Take sample, compute stats
    sample = _extract_final_20_percent(array)
    mean, error, dps = compute_mean_and_std_err(sample)
    # Re-do using all data if error is too big (indicating non-convergence)
    if error > 0.1:
        mean, error, dps = compute_mean_and_std_err(array)
    return mean, error, dps


def _measure_growth_rate_via_fit(time, log_phi2):  # package-private
    """Measures the growth rate with error via a polynomial fit between time
    and log(phi2)
    Inputs:
    - time: array of time data
    - log_phi2: array of log of potential squared data (same size as time)
    - plot: if false or omitted, no plot is generated; if true, data and fit
    are plotted
    Outputs:
    - gamma: growth rate
    - gamma_error: estimated uncertainty in the value of gamma
    - gamma_dps: number of decimal places in gamma (useful for printing)
    - coeffs: fit coefficients (useful for plotting)
    - coeffs_errors: estimated uncertainty in the fit coefficients (useful for
    plotting)
    """
    coeffs, coeffs_errors, coeffs_dps = polyfit_with_errors(time, log_phi2, 1)
    gamma, gamma_error, gamma_dps = round_val_to_err(
        _gradient_to_growth_rate(coeffs[0]),
        _gradient_to_growth_rate(coeffs_errors[0]))
    return gamma, gamma_error, gamma_dps, coeffs, coeffs_errors, coeffs_dps


def _gradient_to_growth_rate(in_val, reverse=False):  # package-private
    """Converts a log(phi2) vs time gradient to a growth rate or visa versa
    using growth rate = gradient / 2.
    Inputs:
    - in_val: gradient or growth rate
    - reverse: if False or omitted, in_val is gradient and answer is growth
      rate; if True, in_val is growth rate and answer is gradient
    Outputs:
    - answer: growth rate or gradient"""
    factor = 2.
    if reverse:
        answer = in_val * factor
    else:
        answer = in_val / factor
    return answer

# --------------------End utility functions ---------------------------


# ----------------------- Class definition ----------------------------

class BasicGrowthRateMeasurer:
    """This class is responsible for measuring the growth rate of the given GS2
    data"""

    def __init__(self, time, phi2):
        'Initialises this GrowthRateMeasurer'
        self.time = time
        self.phi2 = phi2
        self.log_phi2 = np.log(phi2)
        self.supress_printing = True
        self.plot = False

    def fit_excluding_initial_data(self):
        """Attempts to improve the fit accuracy by excluding the initial unreliable
        data"""
        self._get_growth_rate_comparison_values()
        self._match_gradient_within_tolerance()
        return self.gamma, self.gamma_error, self.gamma_dps

    def _get_growth_rate_comparison_values(self):  # private
        """Gets growth rate values from GS2 and the end region for comparison with
        the primary value"""
        self.end_gamma, self.end_gamma_error, _, self.end_coeffs, \
            self.end_coeffs_errors, self.end_coeffs_dps = \
            _measure_growth_rate_via_fit(
                _extract_final_20_percent(self.time),
                _extract_final_20_percent(self.log_phi2))

    def _match_gradient_within_tolerance(self):  # private
        """Computes a fit with the initial unreliable data excluded and
        matches it to the end region gradient within a tolerance."""
        initial_tolerance = 0.0001
        max_tolerance_attempts = 2
        self.done = False
        for i in range(0, max_tolerance_attempts):
            self.gamma_tolerance = initial_tolerance * (10.**i)
            self.final_tolerance = (i == max_tolerance_attempts - 1)
            self._match_fit_and_end_gradients()
            if self.done:
                break

    def _match_fit_and_end_gradients(self):  # private
        'Matches the gradient of the main fit region to that of the end region.'
        max_region_attempts = 50
        # Loop until a good gradient is measured or max_attempts is reached,
        # cutting off more dodgy data at small self.times each attempt
        for i in range(0, max_region_attempts):
            self.region_index = i
            self.final_region = (i == max_region_attempts - 1)
            self._attempt_to_match_gradients()
            if self.done:
                break

    def _attempt_to_match_gradients(self):  # private
        """Attempts to match the main and end gradients within the specified
        tolerance"""
        self._extract_main_fit_regions()
        self.fit_gamma, self.fit_gamma_error, _, self.coeffs, \
            self.coeffs_errors, self.coeffs_dps = \
            _measure_growth_rate_via_fit(self.main_time_region,
                                         self.main_log_phi2_region)
        if abs(self.fit_gamma - self.end_gamma) < self.gamma_tolerance:
            self._process_matched_gradients()
        else:
            self._process_unmatched_gradients()

    def _extract_main_fit_regions(self):  # private
        """Excludes initial unreliable data from self.time and log(phi2) data
        for fitting"""
        self._find_first_index_above_cutoff()
        self.main_time_region = _extract_subarray_min_5_points(
                self.time, len(self.time) - self.first_index - 1)
        self.main_log_phi2_region = _extract_subarray_min_5_points(
                self.log_phi2, len(self.log_phi2) - self.first_index - 1)

    def _find_first_index_above_cutoff(self):  # private
        'Finds the index of the first data point that is above (i+1)*100%'
        self.first_index = 0
        for j in range(0, len(self.phi2)):
            # Find where value exceeds initial value + (i+1)*100%
            if self.phi2[j] > self.phi2[0] + (self.region_index + 1.) * \
                    abs(self.phi2[0]):
                self.first_index = j
                break

    def _process_matched_gradients(self):  # private
        'Gets growth rate with uncertainty from matched gradients'
        self.is_end_fit = False
        self._record_growth_rate_and_uncertainty(
                self.fit_gamma, self.fit_gamma_error, self.coeffs,
                self.coeffs_errors, self.coeffs_dps)
        self.done = True

    def _process_unmatched_gradients(self):  # private
        'Gets growth rate with uncertainty from unmatched gradients'
        if self.final_tolerance and self.final_region:
            if not self.supress_printing:
                print('WARNING: Cannot improve fit for ' +
                      self.run_name + '. Using ' + 'gradient from end region.')
            # Increase intercept uncertainty to account for poorly matched
            # gradient.
            self.end_coeffs[1], self.end_coeffs_errors[1], \
                self.end_coeffs_dps[1] = \
                round_val_to_err(self.end_coeffs[1], 2.)
            # Not matched within gamma_tolerance so double it (as well as
            # conversion from gamma to gradient) to account for larger
            # uncertainty in this case.
            self.gamma_tolerance *= 2.
            # first_index = 0
            self.is_end_fit = True
            self._record_growth_rate_and_uncertainty(
                    self.end_gamma, self.end_gamma_error, self.end_coeffs,
                    self.end_coeffs_errors, self.end_coeffs_dps)
            self.done = True

    def _record_growth_rate_and_uncertainty(self, gamma, gamma_error,
                                            coeffs, coeffs_errors,
                                            coeffs_dps):  # private
        'Records the final value of growth rate and the associated uncertainty'
        self._determine_max_error(gamma, gamma_error)
        if self.plot:
            # Update gradient
            coeffs[0], coeffs_errors[0], coeffs_dps[0] = \
                round_val_to_err(coeffs[0],
                                 _gradient_to_growth_rate(
                                     self.gamma_error, reverse=True))
            plot_improved_growth_rate_fit(
                    self.time, self.log_phi2, coeffs, coeffs_errors,
                    coeffs_dps, self.run_name, self.first_index,
                    self.is_end_fit)

    def _determine_max_error(self, gamma, gamma_error):  # private
        """Determines the error to use, that being the maximum of the error
        computed from the fit, the gamma tolerance and the difference
        between the fit gamma and the gs2 gamma.
        Inputs:
        - self.fit_gamma: the growth rate determined via fitting
        - self.fit_gamma_error: the estimated uncertainty on self.fit_gamma
        - gamma_tolerance = the estimated uncertainty due to matching of
            self.fit_gamma against that from the end region
        - self.gs2_gamma: the growth rate determined by GS2
        - self.gs2_gamma_error: the estimated uncertainty in self.gs2_gamma
        Outputs:
        - self.fit_gamma, rounded to error_to_use (see round_val_to_err for
        details)
        NB: The difference between self.fit_gamma and self.gs2_gamma is only
        included if self.gs2_gamma_error is sensible.
        NB: This function operates in terms of growth rate rather than
        gradient. Therefore, inputs should be converted from gradients to
        growth rate before being passed into this function if necessary and
        outputs should be converted from growth rate to gradient afterwawrds
        if required."""
        error_to_use = max(2. * gamma_error, self.gamma_tolerance)
        self.gamma, self.gamma_error, self.gamma_dps = round_val_to_err(
                gamma, error_to_use)

"""
Test of public API for gs2pyutils

Copyright 2017 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""


import unittest
import numpy.testing as nptest
import numpy as np

# add parent directory to python search path so that the modules being
# tested can be found for import
import os
import sys
sys.path.insert(0,
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)),
                        os.pardir)))

# import the modules to be tested
import gs2pyutils as gs2


class testGs2pyutils3dScanTheta0(unittest.TestCase):

    def setUp(self):
        self.test_dir = (
            "tests/phi3d-theta0/"
        )

    def test_phiTwoScansReflectsTheta0(self):
        'Tests that get_phi reflects theta0 by default'
        data_name = 'phi'
        # Specify expected result
        expected = 6  # 2 * 4 - 2
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectsX(self):
        'Tests that get_phi reflects x by default'
        data_name = 'phi'
        # Specify expected result
        expected = 9  # 2 * 5 - 1
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][3])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectedTheta0(self):
        'Tests that get_phi reflects theta0 correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.array([-0.106494666223, -0.0532473331117, 0.,
                             0.0532473331117, 0.106494666223, 0.159741999335])
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiTwoScansReflectedX(self):
        'Tests that get_phi reflects x correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.array([-0.02, -0.015, -0.01, -0.005, 0., 0.005, 0.01,
                             0.015, 0.02])
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][3]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiTwoScansReflectsPhiInTheta0(self):
        'Tests that get_phi reflects phi in theta0 by default'
        data_name = 'phi'
        # Specify expected result
        expected = 6  # 2 * 4 - 2
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape[1]
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectsPhiInX(self):
        'Tests that get_phi reflects phi in x by default'
        data_name = 'phi'
        # Specify expected result
        expected = 9  # 2 * 5 - 1
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape[0]
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectedPhi(self):
        'Tests that get_phi reflects phi correctly'
        data_name = 'phi'
        # Specify expected result
        phi = np.ndarray((9, 6, 225), dtype=complex)
        for ip in range(6):
            phi[:, ip, :].real = np.loadtxt('tests/phi3d-real' + str(ip) +
                                            '.txt')
            phi[:, ip, :].imag = np.loadtxt('tests/phi3d-imag' + str(ip) +
                                            '.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_equal(actual, phi)


class testGs2pyutils3dScan(unittest.TestCase):

    def setUp(self):
        self.test_dir = (
            "tests/phi3d/"
        )

    def test_phiTwoScansReflectsP(self):
        'Tests that get_phi reflects p by default'
        data_name = 'phi'
        # Specify expected result
        expected = 6  # 2 * 4 - 2
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectsX(self):
        'Tests that get_phi reflects x by default'
        data_name = 'phi'
        # Specify expected result
        expected = 9  # 2 * 5 - 1
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][3])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectedP(self):
        'Tests that get_phi reflects p correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.array([-0.106494666223, -0.0532473331117, 0.,
                             0.0532473331117, 0.106494666223, 0.159741999335])
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiTwoScansReflectedX(self):
        'Tests that get_phi reflects x correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.array([-0.02, -0.015, -0.01, -0.005, 0., 0.005, 0.01,
                             0.015, 0.02])
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][3]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiTwoScansReflectsPhiInP(self):
        'Tests that get_phi reflects phi in p by default'
        data_name = 'phi'
        # Specify expected result
        expected = 6  # 2 * 4 - 2
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape[1]
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectsPhiInX(self):
        'Tests that get_phi reflects phi in x by default'
        data_name = 'phi'
        # Specify expected result
        expected = 9  # 2 * 5 - 1
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape[0]
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiTwoScansReflectedPhi(self):
        'Tests that get_phi reflects phi correctly'
        data_name = 'phi'
        # Specify expected result
        phi = np.ndarray((9, 6, 225), dtype=complex)
        for ip in range(6):
            phi[:, ip, :].real = np.loadtxt('tests/phi3d-real' + str(ip) +
                                            '.txt')
            phi[:, ip, :].imag = np.loadtxt('tests/phi3d-imag' + str(ip) +
                                            '.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_equal(actual, phi)


class testGs2pyutilsXpScan(unittest.TestCase):

    def setUp(self):
        self.test_dir = (
            "tests/fbpar_1.0/"
        )

    def test_omegaTwoScansReflectsP(self):
        'Tests that get_omega reflects p by default'
        data_name = 'omega'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaTwoScansReflectsX(self):
        'Tests that get_omega reflects x by default'
        data_name = 'omega'
        # Specify expected result
        expected = 2 * len(self.get_x()) - 1  # -1 to skip x=0
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaTwoScansReflectedP(self):
        'Tests that get_omega reflects p correctly'
        data_name = 'omega'
        # Specify expected result
        p = get_p()
        num_p = 2 * len(p) - 2  # -2 to skip p=0 & p=pi
        expected = np.linspace(-p[-2], p[-1], num=num_p)
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaTwoScansReflectedX(self):
        'Tests that get_omega reflects x correctly'
        data_name = 'omega'
        # Specify expected result
        x = self.get_x()
        nx = 2 * len(x) - 1  # -1 to skip x=0
        expected = np.linspace(-x[-1], x[-1], num=nx)
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaTwoScansReflectsOmegaInP(self):
        'Tests that get_omega reflects omega in p by default'
        data_name = 'omega'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape[1]
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaTwoScansReflectsOmegaInX(self):
        'Tests that get_omega reflects omega in x by default'
        data_name = 'omega'
        # Specify expected result
        expected = 2 * len(self.get_x()) - 1  # -1 to skip x=0
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape[0]
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaTwoScansReflectedOmega(self):
        'Tests that get_omega reflects omega correctly'
        data_name = 'omega'
        # Specify expected result
        omega = np.loadtxt('tests/omega-xp.txt')
        np_old = len(get_p())
        num_p = 2 * np_old - 2  # -2 to skip p=0 & p=pi
        nx_old = len(self.get_x())
        nx = 2 * nx_old - 1  # -1 to skip x=0
        expected = np.ones((nx, num_p)) * np.mean(omega)
        for ip in range(num_p - np_old):
            expected[nx-nx_old:nx, ip] = omega[:, -(ip+2)]
        for ip in range(num_p - np_old, num_p):
            expected[nx-nx_old:nx, ip] = omega[:, ip-(np_old-2)]
        for ix in range(nx-nx_old):
            expected[ix, :] = expected[nx-ix-1, :]
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_equal(actual, expected)

    def get_x(self):
        return np.array([0., 0.005, 0.01, 0.015, 0.02, 0.025, 0.03, 0.035,
                         0.04, 0.045, 0.05, 0.055, 0.06, 0.065, 0.07, 0.075,
                         0.08, 0.085, 0.09, 0.095, 0.1, 0.105, 0.11, 0.115,
                         0.12, 0.125, 0.13, 0.135, 0.14, 0.145, 0.15, 0.155,
                         0.16, 0.165, 0.17, 0.175, 0.18, 0.185, 0.19, 0.195,
                         0.2])


class testGs2pyutilsTwoScans(unittest.TestCase):

    def setUp(self):
        self.test_dir = (
            "tests/p-scans/"
        )

    def test_gammaResultLengthTwoScans(self):
        'Tests that get_gamma returns correctly sized result'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_gammaNameLengthTwoScans(self):
        'Tests that get_gamma returns the correct number of names'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_gammaNameLengthNoReflect(self):
        'Tests that get_gamma returns the correct number of names'
        data_name = 'gamma'
        test_dir = (
            "tests/tri/"
        )
        # Exercise system under test
        actual = len(gs2.get(data_name, test_dir, reflect=False)['names'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_gammaNameTwoScans(self):
        'Tests that get_gamma returns the correct name for gamma'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_gammaVar1NameTwoScans(self):
        'Tests that get_gamma returns the correct name for p'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'p')

    def test_gammaVar2NameTwoScans(self):
        'Tests that get_gamma returns the correct name for akappa'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][2]
        # Verify result
        self.assertEqual(actual, 'akappa')

    def test_gammaDataLengthTwoScans(self):
        'Tests that get_gamma returns gamma, p and akappa only'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_gammaShapeTwoScans(self):
        'Tests that get_gamma returns gamma of the correct shape'
        data_name = 'gamma'
        # Specify expected result
        expected = (len(self.get_akappa()), len(get_p()))
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0].shape
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaVar1LengthTwoScans(self):
        'Tests that get_gamma returns the correct number of p values'
        data_name = 'gamma'
        # Specify expected result
        expected = len(get_p())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaVar2LengthTwoScans(self):
        'Tests that get_gamma returns the correct number of akappa values'
        data_name = 'gamma'
        # Specify expected result
        expected = len(self.get_akappa())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaDataTwoScans(self):
        'Tests that get_gamma returns gamma correctly'
        data_name = 'gamma'
        # Specify expected result
        expected = np.loadtxt('tests/gamma-2d.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][0]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_gammaVar1DataTwoScans(self):
        'Tests that get_gamma returns p correctly'
        data_name = 'gamma'
        # Specify expected result
        expected = get_p()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_gammaVar2DataTwoScans(self):
        'Tests that get_gamma returns akappa correctly'
        data_name = 'gamma'
        # Specify expected result
        expected = self.get_akappa()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaResultLengthTwoScans(self):
        'Tests that get_omega returns correctly sized result'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_omegaNameLengthTwoScans(self):
        'Tests that get_omega returns the correct number of names'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_omegaNameTwoScans(self):
        'Tests that get_omega returns the correct name for omega'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_omegaVar1NameTwoScans(self):
        'Tests that get_omega returns the correct name for p'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'p')

    def test_omegaVar2NameTwoScans(self):
        'Tests that get_omega returns the correct name for akappa'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][2]
        # Verify result
        self.assertEqual(actual, 'akappa')

    def test_omegaDataLengthTwoScans(self):
        'Tests that get_omega returns omega, p and akappa only'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_omegaShapeTwoScans(self):
        'Tests that get_omega returns omega of the correct shape'
        data_name = 'omega'
        # Specify expected result
        expected = (len(self.get_akappa()), len(get_p()))
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0].shape
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaVar1LengthTwoScans(self):
        'Tests that get_omega returns the correct number of p values'
        data_name = 'omega'
        # Specify expected result
        expected = len(get_p())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaVar2LengthTwoScans(self):
        'Tests that get_omega returns the correct number of akappa values'
        data_name = 'omega'
        # Specify expected result
        expected = len(self.get_akappa())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaDataTwoScans(self):
        'Tests that get_omega returns omega correctly'
        data_name = 'omega'
        # Specify expected result
        expected = np.loadtxt('tests/omega-2d.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][0]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaTypeTwoScans(self):
        'Tests that get_omega returns omega as an array of floats'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        self.assertIs(actual.dtype, np.dtype(float))

    def test_omegaVar1DataTwoScans(self):
        'Tests that get_omega returns p correctly'
        data_name = 'omega'
        # Specify expected result
        expected = get_p()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaVar2DataTwoScans(self):
        'Tests that get_omega returns akappa correctly'
        data_name = 'omega'
        # Specify expected result
        expected = self.get_akappa()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiResultLengthTwoScans(self):
        'Tests that get_phi returns correctly sized result'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_phiNameLengthTwoScans(self):
        'Tests that get_phi returns the correct number of names'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 4)

    def test_phiNameTwoScans(self):
        'Tests that get_phi returns the correct name for phi'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_phiEtaNameTwoScans(self):
        'Tests that get_phi returns the correct name for eta'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'eta')

    def test_phiVar1NameTwoScans(self):
        'Tests that get_phi returns the correct name for p'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][2]
        # Verify result
        self.assertEqual(actual, 'p')

    def test_phiVar2NameTwoScans(self):
        'Tests that get_phi returns the correct name for akappa'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][3]
        # Verify result
        self.assertEqual(actual, 'akappa')

    def test_phiDataLengthTwoScans(self):
        'Tests that get_phi returns phi, eta and p only'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 4)

    def test_phiShapeTwoScans(self):
        'Tests that get_phi returns phi of the correct shape'
        data_name = 'phi'
        # Specify expected result
        expected = (len(self.get_akappa()), len(get_p()), len(get_eta()))
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0].shape
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiEtaLengthTwoScans(self):
        'Tests that get_phi returns eta of the correct length'
        data_name = 'phi'
        # Specify expected result
        expected = len(get_eta())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiVar1LengthTwoScans(self):
        'Tests that get_phi returns p of the correct length'
        data_name = 'phi'
        # Specify expected result
        expected = len(get_p())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiVar2LengthTwoScans(self):
        'Tests that get_phi returns p of the correct length'
        data_name = 'phi'
        # Specify expected result
        expected = len(self.get_akappa())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][3])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiVarIsArray(self):
        'Tests that get_phi returns p as an array'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        self.assertIsInstance(actual, np.ndarray)

    def test_phiFirstRealTwoScans(self):
        'Tests that get_phi returns first real slice of phi correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.loadtxt('tests/phi-2d-real0.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0][0, :, :].real
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiFirstImagTwoScans(self):
        'Tests that get_phi returns first imag slice of phi correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.loadtxt('tests/phi-2d-imag0.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0][0, :, :].imag
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiLastRealTwoScans(self):
        'Tests that get_phi returns last real slice of phi correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.loadtxt('tests/phi-2d-real20.txt')
        # Exercise system under test
        actual = \
            gs2.get(data_name, self.test_dir,
                    reflect=False)['data'][0][-1, :, :].real
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiLastImagTwoScans(self):
        'Tests that get_phi returns last imag slice of phi correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.loadtxt('tests/phi-2d-imag20.txt')
        # Exercise system under test
        actual = \
            gs2.get(data_name, self.test_dir,
                    reflect=False)['data'][0][-1, :, :].imag
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiEtaDataTwoScans(self):
        'Tests that get_phi returns eta correctly'
        data_name = 'phi'
        # Specify expected result
        expected = get_eta()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiVar1DataTwoScans(self):
        'Tests that get_phi returns p correctly'
        data_name = 'phi'
        # Specify expected result
        expected = get_p()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiVar2DataTwoScans(self):
        'Tests that get_phi returns akappa correctly'
        data_name = 'phi'
        # Specify expected result
        expected = self.get_akappa()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][3]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def get_akappa(self):
        return [1.0, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5,
                1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2.0]


class testGs2pyutilsOneScan(unittest.TestCase):
    'Test the gs2pyutils API for a directory containing a 1D scan'

    def setUp(self):
        self.test_dir = (
            "tests/p-scans/akappa_1.00/"
        )

    def test_gammaResultLengthOneScan(self):
        'Tests that get_gamma returns correctly sized result'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_getWorksWithoutTrailingSlash(self):
        'Tests that get_gamma returns correctly sized result'
        data_name = 'gamma'
        test_dir_mod = self.test_dir[0:-1]  # Cut off trailing slash
        # Exercise system under test
        actual = len(gs2.get(data_name, test_dir_mod))
        # Verify result
        self.assertEqual(actual, 3)

    def test_gammaNameLengthOneScan(self):
        'Tests that get_gamma returns the correct number of names'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_getNonEigDataNames(self):
        'Tests that get_gamma returns correct number of names for non-eig data'
        data_name = 'gamma'
        test_dir_mod = "/home/steve/Work/phd/nonlinear/linear"
        # Exercise system under test
        actual = len(gs2.get(data_name, test_dir_mod, eig=False)['names'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_getNonEigFewData(self):
        'Tests that get_gamma returns correct number of names for non-eig data'
        data_name = 'gamma'
        test_dir_mod = "/home/steve/Work/phd/nonlinear/linear/aky_0.00"
        # Exercise system under test
        actual = gs2.get(data_name, test_dir_mod, eig=False)['data'][0]
        # Verify result
        self.assertAlmostEqual(actual, 0.0000, places=4)

    def test_getNonEigDataValues(self):
        'Tests that get_gamma returns correct number of names for non-eig data'
        data_name = 'gamma'
        test_dir_mod = "/home/steve/Work/phd/nonlinear/linear/aky_0.20"
        # Exercise system under test
        actual = gs2.get(data_name, test_dir_mod, eig=False)['data'][0]
        # Verify result
        self.assertAlmostEqual(actual, 0.183095253376272, places=4)

    def test_gammaNameOneScan(self):
        'Tests that get_gamma returns the correct name for gamma'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_gammaVarNameOneScan(self):
        'Tests that get_gamma returns the correct name for p'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'p')

    def test_gammaDataLengthOneScan(self):
        'Tests that get_gamma returns gamma and p only'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_gammaLengthOneScan(self):
        'Tests that get_gamma returns correct number of gamma values'
        data_name = 'gamma'
        # Specify expected result
        expected = len(self.get_gamma())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][0])
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaVarLengthOneScan(self):
        'Tests that get_gamma returns the correct number of p values'
        data_name = 'gamma'
        # Specify expected result
        expected = len(get_p())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaOneScanReflectsP(self):
        'Tests that get_gamma reflects p by default'
        data_name = 'gamma'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaOneScanReflectedP(self):
        'Tests that get_gamma reflects p correctly'
        data_name = 'gamma'
        # Specify expected result
        p = get_p()
        num_p = 2 * len(p) - 2  # -2 to skip p=0 & p=pi
        expected = np.linspace(-p[-2], p[-1], num=num_p)
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_gammaOneScanReflectsGamma(self):
        'Tests that get_gamma reflects gamma by default'
        data_name = 'gamma'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][0])
        # Verify result
        self.assertEqual(actual, expected)

    def test_gammaOneScanReflectedGamma(self):
        'Tests that get_gamma reflects gamma correctly'
        data_name = 'gamma'
        # Specify expected result
        gamma = self.get_gamma()
        num_p = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        expected = np.ndarray(num_p)
        for i in range(len(get_p()) - 2):
            expected[i] = gamma[-(i+2)]
        for i in range(len(get_p()) - 2, len(expected)):
            expected[i] = gamma[i - (len(get_p()) - 2)]
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_equal(actual, expected)

    def test_gammaVarIsArray(self):
        'Tests that get_gamma returns p as an array'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        self.assertIsInstance(actual, np.ndarray)

    def test_gammaDataOneScan(self):
        'Tests that get_gamma returns the correct gamma values'
        data_name = 'gamma'
        # Specify expected result
        expected = self.get_gamma()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][0]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_gammaDataBugAllZeros(self):
        'Tests a bug that was found when applied to r_0/a = 0.5 scan'
        data_name = 'gamma'
        test_dir = (
            "tests/r0-over-a_0.50/"
        )
        # Specify expected result
        expected = self.get_gamma_r0_over_a()
        # Exercise system under test
        actual = gs2.get(data_name, test_dir, reflect=False)['data'][0]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_gammaVarDataOneScan(self):
        'Tests that get_gamma returns the correct gamma values'
        data_name = 'gamma'
        # Specify expected result
        expected = get_p()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaResultLengthOneScan(self):
        'Tests that get_omega returns correctly sized result'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_omegaNameLengthOneScan(self):
        'Tests that get_omega returns the correct names only'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_omegaNameOneScan(self):
        'Tests that get_omega returns the correct name for omega'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_omegaVarNameOneScan(self):
        'Tests that get_omega returns the correct name for p'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'p')

    def test_omegaDataLengthOneScan(self):
        'Tests that get_omega returns omega and p only'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_omegaLengthOneScan(self):
        'Tests that get_omega returns correct number of omega values'
        data_name = 'omega'
        # Specify expected result
        expected = len(self.get_omega())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][0])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaVarLengthOneScan(self):
        'Tests that get_omega returns the correct number of p values'
        data_name = 'omega'
        # Specify expected result
        expected = len(get_p())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaOneScanReflectsP(self):
        'Tests that get_omega reflects p by default'
        data_name = 'omega'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaOneScanReflectedP(self):
        'Tests that get_omega reflects p correctly'
        data_name = 'omega'
        # Specify expected result
        p = get_p()
        num_p = 2 * len(p) - 2  # -2 to skip p=0 & p=pi
        expected = np.linspace(-p[-2], p[-1], num=num_p)
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaOneScanReflectsGamma(self):
        'Tests that get_omega reflects omega by default'
        data_name = 'omega'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][0])
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaOneScanReflectedOmega(self):
        'Tests that get_omega reflects omega correctly'
        data_name = 'omega'
        # Specify expected result
        omega = self.get_omega()
        num_p = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        expected = np.ndarray(num_p)
        for i in range(len(get_p()) - 2):
            expected[i] = omega[-(i+2)]
        for i in range(len(get_p()) - 2, len(expected)):
            expected[i] = omega[i - (len(get_p()) - 2)]
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_equal(actual, expected)

    def test_omegaDataOneScan(self):
        'Tests that get_omega returns the correct omega values'
        data_name = 'omega'
        # Specify expected result
        expected = self.get_omega()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][0]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_omegaVarIsArray(self):
        'Tests that get_omega returns p as an array'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        self.assertIsInstance(actual, np.ndarray)

    def test_omegaVarDataOneScan(self):
        'Tests that get_omega returns the correct omega values'
        data_name = 'omega'
        # Specify expected result
        expected = get_p()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiResultLengthOneScan(self):
        'Tests that get_phi returns correctly sized result'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_phiNameLengthOneScan(self):
        'Tests that get_phi returns the correct number of names'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_phiNameOneScan(self):
        'Tests that get_phi returns the correct name for phi'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_phiEtaNameOneScan(self):
        'Tests that get_phi returns the correct name for eta'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'eta')

    def test_phiVarNameOneScan(self):
        'Tests that get_phi returns the correct name for p'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][2]
        # Verify result
        self.assertEqual(actual, 'p')

    def test_phiDataLengthOneScan(self):
        'Tests that get_phi returns phi, eta and p only'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 3)

    def test_phiShapeOneScan(self):
        'Tests that get_phi returns phi of the correct shape'
        data_name = 'phi'
        # Specify expected result
        expected = (len(get_p()), len(get_eta()))
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0].shape
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiEtaLengthOneScan(self):
        'Tests that get_phi returns eta of the correct length'
        data_name = 'phi'
        # Specify expected result
        expected = len(get_eta())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiVarLengthOneScan(self):
        'Tests that get_phi returns p of the correct length'
        data_name = 'phi'
        # Specify expected result
        expected = len(get_p())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir,
                             reflect=False)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiOneScanReflectsP(self):
        'Tests that get_phi reflects p by default'
        data_name = 'phi'
        # Specify expected result
        expected = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][2])
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiOneScanReflectedP(self):
        'Tests that get_phi reflects p correctly'
        data_name = 'phi'
        # Specify expected result
        p = get_p()
        num_p = 2 * len(p) - 2  # -2 to skip p=0 & p=pi
        expected = np.linspace(-p[-2], p[-1], num=num_p)
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiOneScanReflectsPhi(self):
        'Tests that get_phi reflects phi by default'
        data_name = 'phi'
        # Specify expected result
        expected = (2 * len(get_p()) - 2, len(get_eta()))
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0].shape
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiOneScanReflectedPhi(self):
        'Tests that get_phi reflects phi correctly'
        data_name = 'phi'
        # Specify expected result
        phi_real = np.loadtxt('tests/phi-1d-real.txt')
        phi = np.ndarray(phi_real.shape, dtype=complex)
        phi.real = phi_real
        phi.imag = np.loadtxt('tests/phi-1d-imag.txt')
        num_p = 2 * len(get_p()) - 2  # -2 to skip p=0 & p=pi
        expected = np.ndarray((num_p, len(get_eta())), dtype=complex)
        for i in range(len(get_p()) - 2):
            expected[i, :] = phi[-(i+2), ::-1]
        for i in range(len(get_p()) - 2, num_p):
            expected[i, :] = phi[i - (len(get_p()) - 2), :]
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_equal(actual, expected)

    def test_phiVarIsArray(self):
        'Tests that get_phi returns p as an array'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][2]
        # Verify result
        self.assertIsInstance(actual, np.ndarray)

    def test_phiRealDataOneScan(self):
        'Tests that get_phi returns phi correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.loadtxt('tests/phi-1d-real.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0].real
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiImagDataOneScan(self):
        'Tests that get_phi returns phi correctly'
        data_name = 'phi'
        # Specify expected result
        expected = np.loadtxt('tests/phi-1d-imag.txt')
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir,
                         reflect=False)['data'][0].imag
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiEtaDataOneScan(self):
        'Tests that get_phi returns eta correctly'
        data_name = 'phi'
        # Specify expected result
        expected = get_eta()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiVarDataOneScan(self):
        'Tests that get_phi returns eta correctly'
        data_name = 'phi'
        # Specify expected result
        expected = get_p()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir, reflect=False)['data'][2]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def get_gamma(self):
        return [1.061208387114414, 1.056884931507508, 1.0439541171293323,
                1.0225157583024524, 0.99273635152937312, 0.95484992310918004,
                0.90916009779822182, 0.8560442222334127, 0.7959604476935338,
                0.72945831927284532, 0.65719249547457304, 0.57993765804757624,
                0.49860107248521457, 0.41422899016131115, 0.32800540785161109,
                0.24124522721340516, 0.15538232960749909, 0.071939301589857105,
                -0.007556335851760302, -0.081744523119449838,
                -0.14966854759354434, -0.21095596452759749,
                -0.26582078974124779, -0.31489093153937181,
                -0.35898013609366825, -0.3989112048191415,
                -0.43542038655085036, -0.46912364515523408,
                -0.50051701596816855, -0.52999152572753505,
                -0.55785194826549767, -0.58433443534128859,
                -0.60962130169634376, -0.63385313883312477,
                -0.65713908243524366, -0.67956612939970407,
                -0.70120769421929519, -0.72213118953216071,
                -0.74240367132154361, -0.76209384372985955,
                -0.78127141176883785, -0.80000344594408512,
                -0.81834818907654572, -0.83634973345352215,
                -0.85403556945919279, -0.87142013744336388,
                -0.88849999058316664, -0.90526482012912013,
                -0.92170105757123588, -0.93779730058371025,
                -0.95354189280558266, -0.9689227380219646,
                -0.98393003860728323, -0.99855069018895593,
                -1.0127585397563872, -1.0265264528846303, -1.0398367555168975,
                -1.052702561741812, -1.0651903176108795, -1.0774288322420331]

    def get_gamma_r0_over_a(self):
        return [1.05866855663, 1.05434176552, 1.04140110061, 1.01994643484,
                0.990144297261, 0.952228624539, 0.906502704605, 0.85334316476,
                0.793206950262, 0.72664192019, 0.654300818427, 0.576956863062,
                0.495517576298, 0.411032996659, 0.324696235962, 0.237837185522,
                0.15190835702, 0.0684491979594, -0.0110035389193,
                -0.0850891219195, -0.152853139472, -0.213924043412,
                -0.26851963574, -0.317282020835, -0.361050320285,
                -0.400677064251, -0.436924102308, -0.470423364359,
                -0.501675690849, -0.531067222008, -0.558891331247,
                -0.585370225626, -0.610673440851, -0.634932822034,
                -0.658253832021, -0.68072399119, -0.702419847108,
                -0.723411721063, -0.743767078076, -0.763551949117,
                -0.782830426578, -0.801663014681, -0.820101233563,
                -0.838184069443, -0.855935360583, -0.873361939149,
                -0.890463040532, -0.907225327325, -0.923634012333,
                -0.939676448315, -0.955343719157, -0.970629557066,
                -0.985530536623, -1.00003910235, -1.0141414387, -1.02781835377,
                -1.04105731914, -1.05387606659, -1.06634513254, -1.07859633303]

    def get_omega(self):
        return [2.013256162968402, 2.0130899204831527, 2.0126385568553276,
                2.0119694907250136, 2.0111989018053715, 2.0104965627532629,
                2.0100913594895777, 2.0102761716098794, 2.0114103035924322,
                2.0139171861289582, 2.0182748287979524, 2.0249969439750237,
                2.0346042596453704, 2.0475880911238558, 2.0643697917655115,
                2.0852568039155615, 2.1103877297009013, 2.1396532453988217,
                2.1725950199173627, 2.208331124014594, 2.2455977561328093,
                2.2829538313003641, 2.3190648932094438, 2.352912754509338,
                2.383855623175902, 2.4115773777043068, 2.4359992036176901,
                2.4571992693068903, 2.4753535446082555, 2.4906951208509311,
                2.5034862917774738, 2.5139991560887633, 2.5225026550615248,
                2.5292547497949642, 2.5344984651377271, 2.5384603389192981,
                2.5413497151723665, 2.5433576232828417, 2.5446542792633409,
                2.5453867640861962, 2.5456763250519705, 2.5456164712468241,
                2.5452762549296328, 2.5447052576836149, 2.5439431745629153,
                2.5430205852248884, 2.5419743483805322, 2.5408452954027694,
                2.5396806955013393, 2.538530344877592, 2.5374427620929021,
                2.5364652452298948, 2.5356378002125735, 2.5350017445314736,
                2.5345944277111569, 2.5344746396437632, 2.5347232412104961,
                2.5354454251021363, 2.5367547266903623, 2.5387402952081763]


class testGs2pyutilsSingleFile(unittest.TestCase):
    'Test the gs2pyutils API for a directory containing a single file'

    def setUp(self):
        self.test_dir = (
            "tests/p-scans/akappa_1.00/p_0.000000000/"
        )

    def test_gammaResultLengthSingleFile(self):
        'Tests that get_gamma returns correctly sized result'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_gammaNameLengthSingleFile(self):
        'Tests that get_gamma returns the correct name only'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 1)

    def test_gammaNameSingleFile(self):
        'Tests that get_gamma returns the correct name'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_gammaDataLengthSingleFile(self):
        'Tests that get_gamma returns gamma only'
        data_name = 'gamma'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 1)

    def test_gammaLengthSingleFile(self):
        'Tests that get_gamma returns one value for gamma only'
        data_name = 'gamma'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        self.assertIsInstance(actual, float)

    def test_gammaDataSingleFile(self):
        'Tests that get_gamma returns the correct value'
        data_name = 'gamma'
        # Specify expected result
        expected = 1.061208387114414
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        self.assertEqual(actual, expected)

    def test_omegaResultLengthSingleFile(self):
        'Tests that get_omega returns a correctly sized result'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_omegaNameLengthSingleFile(self):
        'Tests that get_omega returns the correct name only'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 1)

    def test_omegaNameSingleFile(self):
        'Tests that get_omega returns the correct name'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_omegaDataLengthSingleFile(self):
        'Tests that get_omega returns omega only'
        data_name = 'omega'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 1)

    def test_omegaLengthSingleFile(self):
        'Tests that get_omega returns one value for omega only'
        data_name = 'omega'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        self.assertIsInstance(actual, float)

    def test_omegaDataSingleFile(self):
        'Tests that get_omega returns the correct value'
        data_name = 'omega'
        # Specify expected result
        expected = 2.013256162968402
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        self.assertEqual(actual, expected)

    def test_phiResultLengthSingleFile(self):
        'Tests that get_phi returns a correctly sized result'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir))
        # Verify result
        self.assertEqual(actual, 3)

    def test_phiNameLengthSingleFile(self):
        'Tests that get_phi returns the correct number of names'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['names'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_phiNameSingleFile(self):
        'Tests that get_phi returns the correct name for phi'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][0]
        # Verify result
        self.assertEqual(actual, data_name)

    def test_phiEtaNameSingleFile(self):
        'Tests that get_phi returns the correct name for eta'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['names'][1]
        # Verify result
        self.assertEqual(actual, 'eta')

    def test_phiDataLengthSingleFile(self):
        'Tests that get_phi returns the correct number of data lists'
        data_name = 'phi'
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'])
        # Verify result
        self.assertEqual(actual, 2)

    def test_phiLengthSingleFile(self):
        'Tests that get_phi returns a correctly sized list for phi'
        data_name = 'phi'
        # Specify expected result
        expected = len(self.get_phi())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][0])
        # Verify result
        self.assertEqual(actual, expected)

    def test_etaLengthSingleFile(self):
        'Tests that get_phi returns a correctly sized list for eta'
        data_name = 'phi'
        # Specify expected result
        expected = len(get_eta())
        # Exercise system under test
        actual = len(gs2.get(data_name, self.test_dir)['data'][1])
        # Verify result
        self.assertEqual(actual, expected)

    def test_etaTypeSingleFile(self):
        'Tests that get_phi returns eta as floats (not complex)'
        data_name = 'phi'
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        self.assertIs(actual.dtype, np.dtype(float))

    def test_phiDataSingleFile(self):
        'Tests that get_phi returns a correctly sized list for phi'
        data_name = 'phi'
        # Specify expected result
        expected = self.get_phi()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][0]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def test_phiEtaDataSingleFile(self):
        'Tests that get_phi returns a correctly sized list for phi'
        data_name = 'phi'
        # Specify expected result
        expected = get_eta()
        # Exercise system under test
        actual = gs2.get(data_name, self.test_dir)['data'][1]
        # Verify result
        nptest.assert_array_almost_equal(actual, expected)

    def get_phi(self):
        return [
            -8.982617402910636e-06 - 1.434082384879747e-05j,
            -9.481012377873923e-06 - 1.497476387029945e-05j,
            -9.778623264434033e-06 - 1.516433317543154e-05j,
            -1.011545201345856e-05 - 1.521057796918182e-05j,
            -1.043924954573178e-05 - 1.510816554390211e-05j,
            -1.112106908935583e-05 - 1.443161152372933e-05j,
            -1.154018392675683e-05 - 1.360125598343257e-05j,
            -1.201804913947613e-05 - 1.213661085535286e-05j,
            -1.225225045788517e-05 - 1.087349902953772e-05j,
            -1.235815170260678e-05 - 9.989681222309149e-06j,
            -1.246036094158997e-05 - 8.672984261913363e-06j,
            -1.248712102195852e-05 - 7.016396523601651e-06j,
            -1.246660366650089e-05 - 6.238863477224185e-06j,
            -1.239730331085298e-05 - 4.865902765860065e-06j,
            -1.234874229241516e-05 - 3.985214749428150e-06j,
            -1.232124586687061e-05 - 3.032813734224270e-06j,
            -1.235283438196637e-05 - 2.477228076120388e-06j,
            -1.245000279712404e-05 - 2.066871506849652e-06j,
            -1.269702869018813e-05 - 1.730418844376035e-06j,
            -1.297984538694421e-05 - 1.646636180027377e-06j,
            -1.349004823613495e-05 - 1.738281495860952e-06j,
            -1.381538364251707e-05 - 1.898709190750670e-06j,
            -1.457052787893310e-05 - 2.374774405680409e-06j,
            -1.522872375046704e-05 - 2.889119488352799e-06j,
            -1.574751791663724e-05 - 3.327322472705812e-06j,
            -1.660474295636322e-05 - 4.087373692904028e-06j,
            -1.781951966345856e-05 - 5.234381675506687e-06j,
            -1.886694833275949e-05 - 6.230661863845358e-06j,
            -2.023050120857620e-05 - 7.530175510002921e-06j,
            -2.098007354567079e-05 - 8.208924826924007e-06j,
            -2.174074764785527e-05 - 8.865318351907770e-06j,
            -2.252131012934571e-05 - 9.509527225494465e-06j,
            -2.360216411207892e-05 - 1.010029480907589e-05j,
            -2.478341165174476e-05 - 1.055025840121042e-05j,
            -2.555204117060756e-05 - 1.068410608282211e-05j,
            -2.626059048718413e-05 - 1.070222361816323e-05j,
            -2.688819703644378e-05 - 1.057506544992912e-05j,
            -2.787846707430724e-05 - 9.827492769270315e-06j,
            -2.831123497063393e-05 - 9.005583974991603e-06j,
            -2.859290281257663e-05 - 7.525068318955313e-06j,
            -2.855019231428354e-05 - 6.258260984448137e-06j,
            -2.843251055491474e-05 - 5.366502202777262e-06j,
            -2.820875167009132e-05 - 3.993292639582542e-06j,
            -2.780196599222210e-05 - 2.203264738959601e-06j,
            -2.755975050913656e-05 - 1.337644414622279e-06j,
            -2.711473052717630e-05 + 3.316250602858559e-07j,
            -2.681060936711043e-05 + 1.507025712078090e-06j,
            -2.649202227716657e-05 + 2.988000193851654e-06j,
            -2.634147916110403e-05 + 4.041387797389451e-06j,
            -2.630858028519396e-05 + 5.066156721041720e-06j,
            -2.646214915214682e-05 + 6.382027404638032e-06j,
            -2.676086462903219e-05 + 7.339189180389727e-06j,
            -2.742775474140781e-05 + 8.575243169346153e-06j,
            -2.791652959080299e-05 + 9.180686655255054e-06j,
            -2.915933140091138e-05 + 1.036856774282811e-05j,
            -3.036716214717060e-05 + 1.124390785901997e-05j,
            -3.137689693784385e-05 + 1.182079743545847e-05j,
            -3.314563162646579e-05 + 1.266988012728102e-05j,
            -3.590343854897085e-05 + 1.378519317153728e-05j,
            -3.844732311702497e-05 + 1.460252159304728e-05j,
            -4.206293568725536e-05 + 1.576840486942389e-05j,
            -4.412848222698545e-05 + 1.634897686539185e-05j,
            -4.630848861861516e-05 + 1.695555512876038e-05j,
            -4.861739124668801e-05 + 1.757437536341659e-05j,
            -5.178971357384491e-05 + 1.900689537949395e-05j,
            -5.512189757080549e-05 + 2.061134503921294e-05j,
            -5.725308662976429e-05 + 2.185056429901372e-05j,
            -5.923079189220700e-05 + 2.313395158311310e-05j,
            -6.099195616527503e-05 + 2.449647974195573e-05j,
            -6.369243587484652e-05 + 2.754678781496796e-05j,
            -6.499377035620441e-05 + 2.970654631757307e-05j,
            -6.594084658027882e-05 + 3.271859823800476e-05j,
            -6.612884781919902e-05 + 3.489207847168670e-05j,
            -6.604815946922014e-05 + 3.635993285339973e-05j,
            -6.569501443855131e-05 + 3.876837768200755e-05j,
            -6.480185315856721e-05 + 4.207452488514824e-05j,
            -6.420731427805442e-05 + 4.369568701748003e-05j,
            -6.280575744318981e-05 + 4.722866129676338e-05j,
            -6.153203280330926e-05 + 4.988979454301115e-05j,
            -5.952024858720696e-05 + 5.373130115506175e-05j,
            -5.784766131294648e-05 + 5.684772742057741e-05j,
            -5.608430529766103e-05 + 6.035814478810155e-05j,
            -5.367349794503943e-05 + 6.558233611687544e-05j,
            -5.194413259089524e-05 + 7.010308755236206e-05j,
            -5.002447736873564e-05 + 7.719573450085382e-05j,
            -4.924298973710134e-05 + 8.154805608455913e-05j,
            -4.762859152683309e-05 + 9.145520493334907e-05j,
            -4.649364201512594e-05 + 9.982409066920984e-05j,
            -4.631543232871940e-05 + 1.059691008987436e-04j,
            -4.751038677442686e-05 + 1.170989103192038e-04j,
            -5.014889604785242e-05 + 1.376162956744168e-04j,
            -5.123544416354165e-05 + 1.579081797302417e-04j,
            -5.096432638499310e-05 + 1.885423358722427e-04j,
            -5.156770534448969e-05 + 2.052886020743928e-04j,
            -5.407840844060325e-05 + 2.237615619180247e-04j,
            -5.912840311851290e-05 + 2.451988804954051e-04j,
            -6.993384064735453e-05 + 2.860742832444889e-04j,
            -8.557455906693384e-05 + 3.412279613116024e-04j,
            -9.824337216742471e-05 + 3.877096432775777e-04j,
            -1.124432597321129e-04 + 4.428704553060715e-04j,
            -1.280499919505506e-04 + 5.073992323804571e-04j,
            -1.611200585346421e-04 + 6.685099277017275e-04j,
            -1.881470592770082e-04 + 8.188555880756665e-04j,
            -2.229554892383237e-04 + 1.060291694027680e-03j,
            -2.477843463759612e-04 + 1.269696464751789e-03j,
            -2.630452997281751e-04 + 1.420824648152285e-03j,
            -2.815486036584305e-04 + 1.660039323048517e-03j,
            -2.986973326466379e-04 + 1.986538518251293e-03j,
            -3.049742514265736e-04 + 2.146051741245911e-03j,
            -3.102488913124270e-04 + 2.432714222054766e-03j,
            -3.115045640918647e-04 + 2.604541586573034e-03j,
            -3.113339755446578e-04 + 2.748539170086885e-03j,
            -3.114472726877738e-04 + 2.782425541012212e-03j,
            -3.111818539275464e-04 + 2.748373400891259e-03j,
            -3.111625341118500e-04 + 2.604167811313572e-03j,
            -3.097816568596861e-04 + 2.432201528623078e-03j,
            -3.043723998513992e-04 + 2.145385802946531e-03j,
            -2.980437379818176e-04 + 1.985810846261113e-03j,
            -2.808220550589035e-04 + 1.659218538201540e-03j,
            -2.622895921798804e-04 + 1.419959968567647e-03j,
            -2.470189724918119e-04 + 1.268813136548943e-03j,
            -2.221849707648386e-04 + 1.059394508377128e-03j,
            -1.873780122583872e-04 + 8.179623163836443e-04j,
            -1.603440743027550e-04 + 6.676331400091682e-04j,
            -1.272350310033958e-04 + 5.065463415924466e-04j,
            -1.115722358558807e-04 + 4.420243578163442e-04j,
            -9.725209030230057e-05 + 3.868396652139404e-04j,
            -8.412788250697599e-05 + 3.403083843801170e-04j,
            -6.916683472161172e-05 + 2.852838055223845e-04j,
            -5.780428968453956e-05 + 2.444519215404607e-04j,
            -5.327628809207173e-05 + 2.231600845867191e-04j,
            -5.093794004417135e-05 + 2.047880150999553e-04j,
            -5.043222377248656e-05 + 1.881053190806529e-04j,
            -5.080624965409215e-05 + 1.575697426307776e-04j,
            -4.976845633181052e-05 + 1.373387916393163e-04j,
            -4.717412388708437e-05 + 1.168881859270840e-04j,
            -4.600457753575455e-05 + 1.057975109889934e-04j,
            -4.619551885416790e-05 + 9.967470336477858e-05j,
            -4.734361692498462e-05 + 9.133513341593469e-05j,
            -4.897025111151842e-05 + 8.146205672822680e-05j,
            -4.975645511899285e-05 + 7.712484002959930e-05j,
            -5.168071337105209e-05 + 7.005953020302640e-05j,
            -5.341124656875876e-05 + 6.555740777973028e-05j,
            -5.582106052341207e-05 + 6.035620822198759e-05j,
            -5.758251213447567e-05 + 5.686192421539933e-05j,
            -5.925202589523588e-05 + 5.376077834778319e-05j,
            -6.125891020253696e-05 + 4.993849199819394e-05j,
            -6.252833383337844e-05 + 4.729085605944379e-05j,
            -6.392413813488865e-05 + 4.377476392563309e-05j,
            -6.451495127752679e-05 + 4.216160554177408e-05j,
            -6.540130626158763e-05 + 3.887012605300942e-05j,
            -6.574928920030236e-05 + 3.647135944330141e-05j,
            -6.582580356646273e-05 + 3.500932163302723e-05j,
            -6.563203010523807e-05 + 3.284375387952150e-05j,
            -6.467965118186466e-05 + 2.984114748613180e-05j,
            -6.337430311102512e-05 + 2.768956317602014e-05j,
            -6.066963032189852e-05 + 2.465286029492491e-05j,
            -5.890046213117601e-05 + 2.330424764181537e-05j,
            -5.689798755401680e-05 + 2.205010885783884e-05j,
            -5.464123095584286e-05 + 2.094217564007996e-05j,
            -5.150933541476881e-05 + 1.914298884094075e-05j,
            -4.816208363130672e-05 + 1.788520453262904e-05j,
            -4.600298671583425e-05 + 1.713097964109194e-05j,
            -4.386835716460691e-05 + 1.649017644494233e-05j,
            -4.182938089449336e-05 + 1.589172243971020e-05j,
            -3.824814512616776e-05 + 1.470821295142881e-05j,
            -3.572554198712331e-05 + 1.388167360779371e-05j,
            -3.298896359328298e-05 + 1.275897960134310e-05j,
            -3.123374213828158e-05 + 1.190569207913697e-05j,
            -3.023179815953694e-05 + 1.132665203789002e-05j,
            -2.903341025502703e-05 + 1.044942057552318e-05j,
            -2.780099471446666e-05 + 9.260505483113038e-06j,
            -2.731674803417909e-05 + 8.654965624156827e-06j,
            -2.665710190891601e-05 + 7.420561581657520e-06j,
            -2.636292696695940e-05 + 6.465647197630530e-06j,
            -2.621431746715099e-05 + 5.154350152985437e-06j,
            -2.625035302222486e-05 + 4.134028421202272e-06j,
            -2.640351026639096e-05 + 3.086081309266058e-06j,
            -2.672517514983664e-05 + 1.613587150857749e-06j,
            -2.703132863706002e-05 + 4.455242266816481e-07j,
            -2.747890401707197e-05 - 1.213039275622022e-06j,
            -2.772213200726691e-05 - 2.072651627149712e-06j,
            -2.813108577707218e-05 - 3.850697880953119e-06j,
            -2.835639120060978e-05 - 5.214828201399029e-06j,
            -2.847481015729231e-05 - 6.100359358974979e-06j,
            -2.851865829383232e-05 - 7.358397292783764e-06j,
            -2.823875399439070e-05 - 8.829229284922040e-06j,
            -2.780666980646326e-05 - 9.644292163700028e-06j,
            -2.681692426170573e-05 - 1.038323757834268e-05j,
            -2.618786541871530e-05 - 1.050198106033433e-05j,
            -2.547401521352053e-05 - 1.046444131083950e-05j,
            -2.467641201344287e-05 - 1.023587059706473e-05j,
            -2.353517778118928e-05 - 9.933231657733791e-06j,
            -2.240545347833631e-05 - 9.217992277027748e-06j,
            -2.165885796804766e-05 - 8.679423786565772e-06j,
            -2.090688331222611e-05 - 8.053072127380258e-06j,
            -2.016191542262381e-05 - 7.391992905679197e-06j,
            -1.880359405914914e-05 - 6.114533843097273e-06j,
            -1.775973633318298e-05 - 5.131627835965860e-06j,
            -1.654831538294924e-05 - 3.997915346005112e-06j,
            -1.569347099785943e-05 - 3.246296225035524e-06j,
            -1.517609000595974e-05 - 2.812922116091582e-06j,
            -1.451960974193865e-05 - 2.304369759835879e-06j,
            -1.376670706932843e-05 - 1.834301822257983e-06j,
            -1.344249628592230e-05 - 1.676253830759086e-06j,
            -1.293450136747425e-05 - 1.587543233171827e-06j,
            -1.265341070814347e-05 - 1.672352311234598e-06j,
            -1.240886339267213e-05 - 2.008490987097255e-06j,
            -1.231373360434332e-05 - 2.417460706570173e-06j,
            -1.228439831160516e-05 - 2.970639754648230e-06j,
            -1.231538118742901e-05 - 3.918469352425087e-06j,
            -1.236703660258972e-05 - 4.794734052193895e-06j,
            -1.244118061595605e-05 - 6.160754714897242e-06j,
            -1.246448304789232e-05 - 6.934342897426341e-06j,
            -1.244397858081834e-05 - 8.582840845380213e-06j,
            -1.234705307591315e-05 - 9.893424883655162e-06j,
            -1.224481629954605e-05 - 1.077328050581192e-05j,
            -1.201617673228198e-05 - 1.203119049385138e-05j,
            -1.154559138936477e-05 - 1.349108378859009e-05j,
            -1.113106181361502e-05 - 1.431983798341762e-05j,
            -1.045430997535841e-05 - 1.499757513097968e-05j,
            -1.013198825469773e-05 - 1.510194123311607e-05j,
            -9.796079704277491e-06 - 1.505861343068292e-05j,
            -9.498397216531454e-06 - 1.487301339357780e-05j,
            -9.000316859614905e-06 - 1.424501064611783e-05j]


class unittests(unittest.TestCase):

    def test_FindInCurrentDirectoryTwoLevels(self):
        # Setup
        dir_name = './'
        abs_files = ['./shat_8.0/theta0_0.0']
        expected = ['shat_8.0/theta0_0.0']
        # Exercise SUT
        actual = gs2._remove_dir_name_from_paths(dir_name, abs_files)
        # Verify
        self.assertEqual(actual, expected)

    def test_FindInCurrentDirectoryOneLevel(self):
        # Setup
        dir_name = './'
        abs_files = ['./theta0_0.0']
        expected = ['theta0_0.0']
        # Exercise SUT
        actual = gs2._remove_dir_name_from_paths(dir_name, abs_files)
        # Verify
        self.assertEqual(actual, expected)


def get_p():
    return [0.0, 0.053247333, 0.106494666, 0.159741999, 0.212989332,
            0.266236666, 0.319483999, 0.372731332, 0.425978665,
            0.479225998, 0.532473331, 0.585720664, 0.638967997, 0.69221533,
            0.745462664, 0.798709997, 0.85195733, 0.905204663, 0.958451996,
            1.011699329, 1.064946662, 1.118193995, 1.171441328,
            1.224688662, 1.277935995, 1.331183328, 1.384430661,
            1.437677994, 1.490925327, 1.54417266, 1.597419993, 1.650667326,
            1.70391466, 1.757161993, 1.810409326, 1.863656659, 1.916903992,
            1.970151325, 2.023398658, 2.076645991, 2.129893324,
            2.183140658, 2.236387991, 2.289635324, 2.342882657, 2.39612999,
            2.449377323, 2.502624656, 2.555871989, 2.609119322,
            2.662366656, 2.715613989, 2.768861322, 2.822108655,
            2.875355988, 2.928603321, 2.981850654, 3.035097987, 3.08834532,
            3.141592654]


def get_eta():
    return [
        -2.199114857512855e+01,  -2.179479903427918e+01,
        -2.166389934037960e+01,  -2.153299964648003e+01,
        -2.140209995258045e+01,  -2.114030056478129e+01,
        -2.094395102393193e+01,  -2.068215163613278e+01,
        -2.048580209528341e+01,  -2.035490240138384e+01,
        -2.015855286053447e+01,  -1.989675347273532e+01,
        -1.976585377883574e+01,  -1.950405439103659e+01,
        -1.930770485018724e+01,  -1.904590546238811e+01,
        -1.884955592153876e+01,  -1.865320638068941e+01,
        -1.839140699289028e+01,  -1.819505745204092e+01,
        -1.793325806424178e+01,  -1.780235837034220e+01,
        -1.754055898254304e+01,  -1.734420944169368e+01,
        -1.721330974779411e+01,  -1.701696020694474e+01,
        -1.675516081914559e+01,  -1.655881127829623e+01,
        -1.629701189049707e+01,  -1.616611219659749e+01,
        -1.603521250269791e+01,  -1.590431280879833e+01,
        -1.570796326794897e+01,  -1.551161372709960e+01,
        -1.538071403320002e+01,  -1.524981433930044e+01,
        -1.511891464540086e+01,  -1.485711525760171e+01,
        -1.466076571675234e+01,  -1.439896632895319e+01,
        -1.420261678810383e+01,  -1.407171709420425e+01,
        -1.387536755335489e+01,  -1.361356816555574e+01,
        -1.348266847165616e+01,  -1.322086908385701e+01,
        -1.302451954300765e+01,  -1.276272015520852e+01,
        -1.256637061435917e+01,  -1.237002107350983e+01,
        -1.210822168571069e+01,  -1.191187214486134e+01,
        -1.165007275706219e+01,  -1.151917306316261e+01,
        -1.125737367536346e+01,  -1.106102413451409e+01,
        -1.093012444061452e+01,  -1.073377489976516e+01,
        -1.047197551196600e+01,  -1.027562597111664e+01,
        -1.001382658331748e+01,  -9.882926889417908e+00,
        -9.752027195518327e+00,  -9.621127501618748e+00,
        -9.424777960769379e+00,  -9.228428419920011e+00,
        -9.097528726020432e+00,  -8.966629032120853e+00,
        -8.835729338221274e+00,  -8.573929950422119e+00,
        -8.377580409572754e+00,  -8.115781021773603e+00,
        -7.919431480924240e+00,  -7.788531787024667e+00,
        -7.592182246175302e+00,  -7.330382858376149e+00,
        -7.199483164476570e+00,  -6.937683776677421e+00,
        -6.741334235828067e+00,  -6.479534848028933e+00,
        -6.283185307179586e+00,  -6.086835766330241e+00,
        -5.825036378531106e+00,  -5.628686837681752e+00,
        -5.366887449882602e+00,  -5.235987755983024e+00,
        -4.974188368183871e+00,  -4.777838827334508e+00,
        -4.646939133434933e+00,  -4.450589592585569e+00,
        -4.188790204786418e+00,  -3.992440663937054e+00,
        -3.730641276137899e+00,  -3.599741582238321e+00,
        -3.468841888338742e+00,  -3.337942194439161e+00,
        -3.141592653589793e+00,  -2.945243112740425e+00,
        -2.814343418840846e+00,  -2.683443724941266e+00,
        -2.552544031041688e+00,  -2.290744643242533e+00,
        -2.094395102393169e+00,  -1.832595714594018e+00,
        -1.636246173744654e+00,  -1.505346479845080e+00,
        -1.308996938995716e+00,  -1.047197551196562e+00,
        -9.162978572969835e-01,  -6.544984694978351e-01,
        -4.581489286484802e-01,  -1.963495408493467e-01,
        -4.440892098500626e-16,   1.963495408493459e-01,
        4.581489286484804e-01,   6.544984694978342e-01,
        9.162978572969835e-01,   1.047197551196562e+00,
        1.308996938995715e+00,   1.505346479845078e+00,
        1.636246173744653e+00,   1.832595714594017e+00,
        2.094395102393169e+00,   2.290744643242533e+00,
        2.552544031041688e+00,   2.683443724941265e+00,
        2.814343418840845e+00,   2.945243112740425e+00,
        3.141592653589793e+00,   3.337942194439162e+00,
        3.468841888338741e+00,   3.599741582238321e+00,
        3.730641276137898e+00,   3.992440663937053e+00,
        4.188790204786417e+00,   4.450589592585569e+00,
        4.646939133434932e+00,   4.777838827334506e+00,
        4.974188368183871e+00,   5.235987755983023e+00,
        5.366887449882602e+00,   5.628686837681752e+00,
        5.825036378531106e+00,   6.086835766330239e+00,
        6.283185307179586e+00,   6.479534848028932e+00,
        6.741334235828067e+00,   6.937683776677421e+00,
        7.199483164476570e+00,   7.330382858376148e+00,
        7.592182246175302e+00,   7.788531787024664e+00,
        7.919431480924239e+00,   8.115781021773603e+00,
        8.377580409572754e+00,   8.573929950422119e+00,
        8.835729338221274e+00,   8.966629032120851e+00,
        9.097528726020432e+00,   9.228428419920011e+00,
        9.424777960769379e+00,   9.621127501618748e+00,
        9.752027195518327e+00,   9.882926889417906e+00,
        1.001382658331748e+01,   1.027562597111664e+01,
        1.047197551196600e+01,   1.073377489976516e+01,
        1.093012444061452e+01,   1.106102413451409e+01,
        1.125737367536346e+01,   1.151917306316261e+01,
        1.165007275706219e+01,   1.191187214486134e+01,
        1.210822168571069e+01,   1.237002107350983e+01,
        1.256637061435917e+01,   1.276272015520852e+01,
        1.302451954300765e+01,   1.322086908385701e+01,
        1.348266847165616e+01,   1.361356816555573e+01,
        1.387536755335489e+01,   1.407171709420425e+01,
        1.420261678810382e+01,   1.439896632895319e+01,
        1.466076571675234e+01,   1.485711525760171e+01,
        1.511891464540086e+01,   1.524981433930044e+01,
        1.538071403320002e+01,   1.551161372709960e+01,
        1.570796326794897e+01,   1.590431280879833e+01,
        1.603521250269791e+01,   1.616611219659749e+01,
        1.629701189049707e+01,   1.655881127829623e+01,
        1.675516081914559e+01,   1.701696020694474e+01,
        1.721330974779411e+01,   1.734420944169368e+01,
        1.754055898254304e+01,   1.780235837034220e+01,
        1.793325806424178e+01,   1.819505745204092e+01,
        1.839140699289028e+01,   1.865320638068941e+01,
        1.884955592153876e+01,   1.904590546238811e+01,
        1.930770485018724e+01,   1.950405439103659e+01,
        1.976585377883574e+01,   1.989675347273532e+01,
        2.015855286053447e+01,   2.035490240138384e+01,
        2.048580209528341e+01,   2.068215163613278e+01,
        2.094395102393193e+01,   2.114030056478129e+01,
        2.140209995258045e+01,   2.153299964648003e+01,
        2.166389934037960e+01,   2.179479903427918e+01,
        2.199114857512855e+01]

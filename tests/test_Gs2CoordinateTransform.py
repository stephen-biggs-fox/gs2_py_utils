"""
Unit tests of Gs2CoordinateTransform

Copyright 2019 Stephen Biggs-Fox

This file is part of tests.

tests is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tests is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with tests.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
from Gs2CoordinateTransform import Gs2CoordinateTransform
import numpy as np
import numpy.testing as np_test


class test_Gs2CoordinateTransform(TestCase):

    def setUp(self):
        self.gct = Gs2CoordinateTransform()
        # Setup dataset, geometry and input_file so code does not try to load
        # from non-existent files
        self.gct.dataset = {
            'q': FakeXarrayDataArray(1.4),
            'ky': FakeXarrayDataArray([0.0, 0.03515625]),
            'kx': FakeXarrayDataArray([0.0, 0.172296722095315]),
            'drhodpsi': FakeXarrayDataArray(2.75700264365173),
            'shat': FakeXarrayDataArray(0.78)
        }
        self.gct.geometry = {
            'theta': np.array([-3.141, 0.0, 3.141]),
            'alpha': np.array([1.0, 2.0, 3.0]),
            'alpha_prime': np.array([1.0, 2.0, 3.0])
        }
        self.gct.input_file = {
            'theta_grid_parameters': {'rhoc': 0.5},
            'kt_grids_box_parameters': {'jtwist': 1}
        }
        # Set default value of n0 (cannot pass to constructor within these
        # tests because we need to fake the dataset as above)
        self.gct.set_n0(20)

    def test_q_rationaliser_already_rational(self):
        # Exercise SUT
        self.gct.ensure_safety_factor_is_rational()
        # Verify
        self.assertEqual(self.gct.geometry['q_rat'], 1.4)

    def test_q_rationaliser_not_already_rational(self):
        # Setup
        self.gct.set_n0(2)
        # Exercise SUT
        self.gct.ensure_safety_factor_is_rational()
        # Verify
        self.assertEqual(self.gct.geometry['q_rat'], 1.5)

    def test_forward_field_is_maintained(self):
        # Exercise SUT
        self.gct.check_magnetic_field_direction()
        # Verify
        np_test.assert_array_equal(self.gct.geometry['alpha_new'],
                                   self.gct.geometry['alpha'])

    def test_backward_field_is_reversed(self):
        # Setup
        self.gct.geometry['alpha'] = np.array([-1.0, -2.0, -3.0])
        # Exercise SUT
        self.gct.check_magnetic_field_direction()
        # Verify
        np_test.assert_array_equal(self.gct.geometry['alpha_new'],
                                   np.array([1.0, 2.0, 3.0]))

    def test_alpha_does_not_change_when_q_is_rational(self):
        # Exercise SUT
        self.gct.correct_alpha_against_q_rat()
        # Verify
        np_test.assert_array_equal(self.gct.geometry['alpha_new'],
                                   self.gct.geometry['alpha'])

    def test_alpha_changes_when_q_is_irrational(self):
        # Setup
        self.gct.set_n0(2)
        # Exercise SUT
        self.gct.correct_alpha_against_q_rat()
        # Verify
        np_test.assert_array_almost_equal(self.gct.geometry['alpha_new'],
                                          np.array([1.0 + (-0.1) * (-3.141),
                                                    2.0,
                                                    3.0 + (-0.1) * 3.141]))

    def test_shat_does_not_change_when_q_is_rational(self):
        # *** UNCLEAR WHAT THE CORRECT BEHAVIOUR IS WHEN Q IS IRRATIONAL! ***
        # Exercise SUT
        self.gct.compute_consistent_shat()
        # Verify
        np_test.assert_almost_equal(self.gct.geometry['shat_new'],
                                    self.gct.dataset['shat'].values)

    def test_alpha_prime_does_not_change_when_q_is_rational(self):
        # Exercise SUT
        self.gct.correct_alpha_prime_against_q_rat()
        # Verify
        np_test.assert_almost_equal(self.gct.geometry['alpha_prime_new'],
                                    self.gct.geometry['alpha_prime'])

    def test_alpha_prime_changes_when_q_is_irrational(self):
        # Setup
        self.gct.set_n0(2)
        # Exercise SUT
        self.gct.correct_alpha_prime_against_q_rat()
        # Verify
        np_test.assert_array_almost_equal(
            self.gct.geometry['alpha_prime_new'],
            np.array([1.0 + (-0.1 * 0.78 / 0.5) * (-3.141), 2.0,
                      3.0 + (-0.1 * 0.78 / 0.5) * 3.141]))


class FakeXarrayDataArray:

    def __init__(self, values):
        self.values = values

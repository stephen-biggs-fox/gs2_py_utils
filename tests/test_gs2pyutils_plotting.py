"""
Test of plotting part of public API for gs2pyutils

Copyright 2017 Steve Biggs

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""


import unittest
import numpy as np
import numpy.testing as nptest
from io import StringIO
import matplotlib as mpl
import matplotlib.pyplot as plt

# add parent directory to python search path so that the modules being
# tested can be found for import
import os
import sys
sys.path.insert(0,
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)),
                        os.pardir)))

# import the modules to be tested
import gs2pyutils as gs2


class testGs2pyutilsPlotting(unittest.TestCase):
    # All tests in one class so setUp that supresses printing can be
    # used by all

    def setUp(self):
        self.output = StringIO()
        self.saved_stdout = sys.stdout
        sys.stdout = self.output

    def tearDown(self):
        sys.stdout = self.saved_stdout
        plt.clf()

    def test_PrintRedirection(self):
        'Tests that the output of print can be catpured and tested'
        # Setup
        test_string = 'ihabgia'  # Random string
        # Exercise system under test
        print(test_string)
        # Verify result
        self.assertEqual(self.output.getvalue(), test_string + '\n')

    def test_singleGammaValuesArePrinted(self):
        'Tests that single values are printed'
        # Setup
        data = {'data': np.array([1.061208387114414]),
                'names': ['gamma']}
        # Exercise system under test
        gs2.render(data)
        # Verify result
        self.assertEqual(self.output.getvalue(),
                         'gamma = 1.061208387114414\n')

    def test_singleGammaValuesAreReturned(self):
        'Tests that single values are printed'
        # Setup
        data = {'data': np.array([1.061208387114414]),
                'names': ['gamma']}
        # Exercise system under test
        actual = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(actual, 'gamma = 1.061208387114414')

    def test_canRenderDirectlyFromDir(self):
        'Tests that render works on a data name and directory'
        # Setup
        test_dir = (
            "/home/steve/Work/phd/kbms/core-to-pedestal/"
            "akappa-const-drive/marconi/p-scans/akappa_1.00/"
            "p_0.000000000/"
        )
        # Exercise system under test
        actual = gs2.render('gamma', test_dir, show=False)
        # Verify result
        self.assertEqual(actual, 'gamma = 1.061208387114414')

    def test_typeErrorWhenDataWrong(self):
        'Tests that render raises a TypeError when data neither dict nor name'
        # Verify result
        self.assertRaises(TypeError, gs2.render, None, show=False)

    def test_typeErrorMessageData(self):
        'Tests that direct render raises a TypeError when dir not given'
        # Exercise system under test
        with self.assertRaises(TypeError) as context:
            gs2.render(None, show=False)
        # Verify result
        self.assertIn(("data must be dict or string"), str(context.exception))

    def test_typeErrorWhenDirUndefined(self):
        'Tests that direct render raises a TypeError when dir not given'
        # Verify result
        self.assertRaises(TypeError, gs2.render, 'gamma', show=False)

    def test_typeErrorMessageDir(self):
        'Tests that direct render raises a TypeError when dir not given'
        # Exercise system under test
        with self.assertRaises(TypeError) as context:
            gs2.render('gamma', show=False)
        # Verify result
        self.assertIn(("dir_name must be specified as a string when using "
                       "direct rendering"), str(context.exception))

    def test_valueErrorWhenDirInvalid(self):
        'Tests that direct render raises a ValueError when dir does not exist'
        # Exercise system under test
        self.assertRaises(ValueError, gs2.render, 'gamma',
                          '/some/random/path/that/does/not/exist/', show=False)

    def test_valueErrorMessage(self):
        'Tests that direct render raises a ValueError when dir does not exist'
        # Exercise system under test
        with self.assertRaises(ValueError) as context:
            gs2.render('gamma', '/some/random/path/that/does/not/exist/',
                       show=False)
        # Verify result
        self.assertIn(("dir_name must be a valid path"), str(context.exception))

    def test_singleOmegaValuesArePrinted(self):
        'Tests that single values are printed'
        # Setup
        data = {'data': np.array([2.013256162968402]),
                'names': ['omega']}
        # Exercise system under test
        gs2.render(data)
        # Verify result
        self.assertEqual(self.output.getvalue(),
                         'omega = 2.013256162968402\n')

    def test_gamma1DPlotIsLine(self):
        'Tests that plot of 1D gamma data is a line plot'
        # Setup
        data = self.get_gamma_1d()
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertIsInstance(line, mpl.lines.Line2D)

    def test_phiSingleFileIsLine(self):
        'Tests that plot of phi data from a single file is a line plot'
        # Setup
        data = self.get_phi_0d()
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertIsInstance(line, mpl.lines.Line2D)

    def test_gamma1DPlotXData(self):
        'Tests that plot of 1D gamma data has correct x values'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'random name with no scaling factor'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_equal(line.get_xdata(), self.get_p())

    def test_gamma1DPlotYData(self):
        'Tests that plot of 1D gamma data has correct y values'
        # Setup
        data = self.get_gamma_1d()
        data['names'][0] = 'random name with no scaling factor'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_equal(line.get_ydata(), self.get_gamma())

    def test_phi1DDataIsNormalised(self):
        'Tests that plot of 1D phi data has correctly normalised y values'
        # Setup
        data = self.get_phi_0d()
        # Specify expected result
        phi = self.get_phi()[0, :]
        index_of_peak = np.where(abs(phi) == max(abs(phi)))[0][0]
        phi_peak = phi[index_of_peak]
        expected = (phi / phi_peak).real
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(line.get_ydata(), expected)

    def test_gamma1DPlotXLabel(self):
        'Tests that plot of 1D gamma data has correct x label'
        # Setup
        data = self.get_gamma_1d()
        name = 'random name with no defined label'
        data['names'][1] = name
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), name)

    def test_gamma1DPlotYLabel(self):
        'Tests that plot of 1D gamma data has correct y label'
        # Setup
        data = self.get_gamma_1d()
        name = 'random name with no defined label'
        data['names'][0] = name
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_ylabel(), name)

    def test_pInPi1D(self):
        'Tests that p is plotted in units of pi for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(line.get_xdata(),
                                         self.get_p() / np.pi)

    def test_theta0InPi1D(self):
        'Tests that theta0 is plotted in units of pi for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'theta0'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(line.get_xdata(),
                                         self.get_p() / np.pi)

    def test_etaInPi1D(self):
        'Tests that eta is plotted in units of pi for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'eta'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(line.get_xdata(),
                                         self.get_p() / np.pi)

    def test_betaLabel1D(self):
        'Tests that data named beta has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'beta'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$\beta$')

    def test_etaLabel1D(self):
        'Tests that data named eta has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'eta'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$\theta\;/\;\pi$')

    def test_gammaLabel1D(self):
        'Tests that data named gamma has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_ylabel(),
                         r'$\gamma_0\;/\;v_{th}/L_{ref}$')

    def test_omegaLabel1D(self):
        'Tests that data named omega has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][0] = 'omega'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_ylabel(),
                         r'$\omega_0\;/\;v_{th}/L_{ref}$')

    def test_phiLabel1D(self):
        'Tests that data named phi has correct label for a 1D plot'
        # Setup
        data = self.get_phi_0d()
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_ylabel(),
                         r'$\Re (\delta\hat\phi_0\;/\;\delta\hat\phi_0|_{peak})$')

    def test_akappaLabel1D(self):
        'Tests that data named akappa has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'akappa'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$\kappa$')

    def test_pLabel1D(self):
        'Tests that data named p has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$\theta_0\;/\;\pi$')

    def test_theta0Label1D(self):
        'Tests that data named theta0 has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'theta0'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$\theta_0\;/\;\pi$')

    def test_xLabel1D(self):
        'Tests that data named x has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'x'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$x\;/\;\rho_{ref}$')

    def test_r0OverALabel1D(self):
        'Tests that data named r0-over-a has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'r0-over-a'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$r_0\;/\;a$')

    def test_triLabel1D(self):
        'Tests that data named tri has correct label for a 1D plot'
        # Setup
        data = self.get_gamma_1d()
        data['names'][1] = 'tri'
        # Exercise system under test
        line, = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(line.axes.get_xlabel(), r'$\arcsin[\delta]$')

    def test_seabornIsLoaded(self):
        'Tests that the seaborn module has been imported'
        # Verify result
        self.assertIn('seaborn', sys.modules, msg='seaborn module not imported')

    def test_seabornFontScale(self):
        'Tests that the seaborn module has a larger font scale set'
        # Setup
        import seaborn as sns
        # Get actual value
        actual = sns.plotting_context()['font.size'] / 12.0
        # Verify result
        self.assertAlmostEqual(actual, 2)

    def test_phi2DPlotIsContour(self):
        'Tests that plot of 1D phi data is a contour plot'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        actual = gs2.render(data, show=False)
        # Verify result
        self.assertIsInstance(actual, mpl.collections.QuadMesh)

    def test_phi2DPlotXData(self):
        'Tests that plot of 1D phi data has correct x values'
        # Setup
        data = self.get_phi_1d()
        data['names'][1] = 'random name with no scaling factor'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_equal(self.get_xdata(plot),
                                  self.get_eta())

    def test_phi2DPlotYData(self):
        'Tests that plot of 1D phi data has correct y values'
        # Setup
        data = self.get_phi_1d()
        data['names'][2] = 'random name with no scaling factor'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_equal(self.get_ydata(plot),
                                  self.get_p())

    def test_phi2DPlotZData(self):
        'Tests that plot of 1D phi data has correct z values'
        # Setup
        data = self.get_phi_1d()
        data['names'][0] = 'random name with no scaling factor'
        # Specify expected result
        phi = self.get_phi().real
        expected = np.reshape(phi, phi.size)
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        # WARNING - fragile test!
        # NB: Only the first eta's worth of values are checked as they
        # start to differ after this. The arrays aren't even the same
        # shape or number of elements! It is not clear why. Probably,
        # pcolormesh is doing some clever stuff under the hood so I'm
        # not worried about it. But there is a chance that this magic
        # could change, thus breaking this test. If so, consider
        # altering this test.
        nptest.assert_array_equal(self.get_zdata(plot)[0:224], expected[0:224])

    def test_phi2DDataIsNormalised(self):
        'Tests that plot of 1D phi data has correctly normalised y values'
        # Setup
        data = self.get_phi_1d()
        # Specify expected result
        phi = self.get_phi()
        for i in range(phi.shape[0]):
            index_of_peak = np.where(
                abs(phi[i, :]) == max(abs(phi[i, :])))[0][0]
            phi_peak = phi[i, index_of_peak]
            phi[i, :] = (phi[i, :] / phi_peak)
        expected = np.reshape(phi.real, phi.size)
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        # WARNING - fragile test!
        # NB: As above
        nptest.assert_array_almost_equal(self.get_zdata(plot)[0:223],
                                         expected[0:223])

    def test_phi2DPlotXLabel(self):
        'Tests that plot of 1D phi data has correct x label'
        # Setup
        data = self.get_phi_1d()
        name = 'random name with no defined label'
        data['names'][1] = name
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_xlabel(), name)

    def test_phi2DPlotYLabel(self):
        'Tests that plot of 1D phi data has correct y label'
        # Setup
        data = self.get_phi_1d()
        name = 'random name with no defined label'
        data['names'][2] = name
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_ylabel(), name)

    def test_pInPi2D(self):
        'Tests that p is plotted in units of pi for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(self.get_ydata(plot),
                                         self.get_p() / np.pi)

    def test_theta0InPi2D(self):
        'Tests that theta0 is plotted in units of pi for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        data['names'][2] = 'theta0'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(self.get_ydata(plot),
                                         self.get_p() / np.pi)

    def test_etaInPi2D(self):
        'Tests that eta is plotted in units of pi for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        nptest.assert_array_almost_equal(self.get_xdata(plot),
                                         self.get_eta() / np.pi)

    def test_betaLabel2D(self):
        'Tests that data named beta has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        data['names'][1] = 'beta'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_xlabel(), r'$\beta$')

    def test_etaLabel2D(self):
        'Tests that data named eta has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_xlabel(), r'$\theta\;/\;\pi$')

    def test_gammaLabel2D(self):
        'Tests that data named gamma has correct label for a 2D plot'
        # Setup
        data = gs2.get('gamma', 'tests/phi3d/')
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.colorbar.ax.yaxis.get_label_text(),
                         r'$\gamma_0\;/\;v_{th}/L_{ref}$')

    def test_omegaLabel2D(self):
        'Tests that data named omega has correct label for a 2D plot'
        # Setup
        data = gs2.get('omega', 'tests/phi3d/')
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.colorbar.ax.yaxis.get_label_text(),
                         r'$\omega_0\;/\;v_{th}/L_{ref}$')

    def test_phiLabel2D(self):
        'Tests that data named phi has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.colorbar.ax.yaxis.get_label_text(),
                         r'$\Re (\delta\hat\phi_0\;/\;\delta\hat\phi_0|_{peak})$')

    def test_akappaLabel2D(self):
        'Tests that data named akappa has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        data['names'][1] = 'akappa'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_xlabel(), r'$\kappa$')

    def test_pLabel2D(self):
        'Tests that data named p has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_ylabel(), r'$\theta_0\;/\;\pi$')

    def test_theta0Label2D(self):
        'Tests that data named theta0 has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        data['names'][2] = 'theta0'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_ylabel(), r'$\theta_0\;/\;\pi$')

    def test_xLabel2D(self):
        'Tests that data named x has correct label for a 2D plot'
        # Setup
        data = self.get_phi_1d()
        data['names'][1] = 'x'
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.axes.get_xlabel(), r'$x\;/\;\rho_{ref}$')

    def test_phi2DIsNotGrey(self):
        'Tests that 2D phi data does not use the default colormap'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertFalse(plot.colorbar.cmap.is_gray())

    def test_phi2DUsesPurpleCmap(self):
        'Tests that 2D phi data uses a green-to-purple colormap'
        # Setup
        data = self.get_phi_1d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.colorbar.cmap.name, 'PiYG_r')

    def test_gamma2DUsesRedCmap(self):
        'Tests that 2D gamma data uses a red colormap'
        # Setup
        data = gs2.get('gamma', 'tests/phi3d/')
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.colorbar.cmap.name, 'RdBu_r')

    def test_omega2DUsesHuslCmap(self):
        'Tests that 2D gamma data uses a green-to-red colormap'
        # Setup
        data = gs2.get('omega', 'tests/phi3d/')
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertEqual(plot.colorbar.cmap.name, 'husl')

    def test_phi3dReturnsFuncAnimation(self):
        'Tests that 3D phi data returns a FuncAnimation object'
        # Setup
        data = self.get_phi_2d()
        # Exercise system under test
        plot = gs2.render(data, show=False)
        # Verify result
        self.assertIsInstance(plot, mpl.animation.FuncAnimation)

    def get_xdata(self, plot):
        return np.array(plot._coordinates[0][:])[:, 0]

    def get_ydata(self, plot):
        return np.array(plot._coordinates)[:, 0][:, 1]

    def get_zdata(self, plot):
        return plot._A

    def get_phi_0d(self):
        return {'data': [self.get_phi()[0, :], self.get_eta()],
                'names': ['phi', 'eta']}

    def get_phi_2d(self):
        phi = np.ndarray((len(self.get_x()), len(self.get_p()),
                          len(self.get_eta())), dtype=complex)
        phi[:, :, :] = self.get_phi()
        return {'data': [phi, self.get_eta(), self.get_p(), self.get_x()],
                'names': ['phi', 'eta', 'p', 'x']}

    def get_phi_1d(self):
        return {'data': [self.get_phi(), self.get_eta(), self.get_p()],
                'names': ['phi', 'eta', 'p']}

    def get_gamma_1d(self):
        return {'data': [self.get_gamma(), self.get_p()],
                'names': ['gamma', 'p']}

    def get_gamma(self):
        return np.array([1.061208387114414, 1.056884931507508,
                         1.043954117129332, 1.022515758302452,
                         0.992736351529373, 0.95484992310918,
                         0.909160097798222, 0.856044222233413,
                         0.795960447693534, 0.729458319272845,
                         0.657192495474573, 0.579937658047576,
                         0.498601072485215, 0.414228990161311,
                         0.328005407851611, 0.241245227213405,
                         0.155382329607499, 0.071939301589857,
                         -0.00755633585176, -0.08174452311945,
                         -0.149668547593544, -0.210955964527597,
                         -0.265820789741248, -0.314890931539372,
                         -0.358980136093668, -0.398911204819141,
                         -0.43542038655085, -0.469123645155234,
                         -0.500517015968169, -0.529991525727535,
                         -0.557851948265498, -0.584334435341289,
                         -0.609621301696344, -0.633853138833125,
                         -0.657139082435244, -0.679566129399704,
                         -0.701207694219295, -0.722131189532161,
                         -0.742403671321544, -0.76209384372986,
                         -0.781271411768838, -0.800003445944085,
                         -0.818348189076546, -0.836349733453522,
                         -0.854035569459193, -0.871420137443364,
                         -0.888499990583167, -0.90526482012912,
                         -0.921701057571236, -0.93779730058371,
                         -0.953541892805583, -0.968922738021965,
                         -0.983930038607283, -0.998550690188956,
                         -1.012758539756387, -1.02652645288463,
                         -1.039836755516897, -1.052702561741812,
                         -1.065190317610879, -1.077428832242033])

    def get_x(self):
        return np.linspace(0., 0.2, num=41)

    def get_p(self):
        return np.array([0., 0.053247333, 0.106494666, 0.159741999,
                         0.212989332, 0.266236666, 0.319483999, 0.372731332,
                         0.425978665, 0.479225998, 0.532473331, 0.585720664,
                         0.638967997, 0.69221533, 0.745462664, 0.798709997,
                         0.85195733, 0.905204663, 0.958451996, 1.011699329,
                         1.064946662, 1.118193995, 1.171441328, 1.224688662,
                         1.277935995, 1.331183328, 1.384430661, 1.437677994,
                         1.490925327, 1.54417266, 1.597419993, 1.650667326,
                         1.70391466, 1.757161993, 1.810409326, 1.863656659,
                         1.916903992, 1.970151325, 2.023398658, 2.076645991,
                         2.129893324, 2.183140658, 2.236387991, 2.289635324,
                         2.342882657, 2.39612999, 2.449377323,  2.502624656,
                         2.555871989, 2.609119322,  2.662366656, 2.715613989,
                         2.768861322,  2.822108655,  2.875355988, 2.928603321,
                         2.981850654,  3.035097987, 3.08834532,  3.141592654])

    def get_eta(self):
        return np.array([
            -2.199114857512855e+01,  -2.179479903427918e+01,
            -2.166389934037960e+01,  -2.153299964648003e+01,
            -2.140209995258045e+01,  -2.114030056478129e+01,
            -2.094395102393193e+01,  -2.068215163613278e+01,
            -2.048580209528341e+01,  -2.035490240138384e+01,
            -2.015855286053447e+01,  -1.989675347273532e+01,
            -1.976585377883574e+01,  -1.950405439103659e+01,
            -1.930770485018724e+01,  -1.904590546238811e+01,
            -1.884955592153876e+01,  -1.865320638068941e+01,
            -1.839140699289028e+01,  -1.819505745204092e+01,
            -1.793325806424178e+01,  -1.780235837034220e+01,
            -1.754055898254304e+01,  -1.734420944169368e+01,
            -1.721330974779411e+01,  -1.701696020694474e+01,
            -1.675516081914559e+01,  -1.655881127829623e+01,
            -1.629701189049707e+01,  -1.616611219659749e+01,
            -1.603521250269791e+01,  -1.590431280879833e+01,
            -1.570796326794897e+01,  -1.551161372709960e+01,
            -1.538071403320002e+01,  -1.524981433930044e+01,
            -1.511891464540086e+01,  -1.485711525760171e+01,
            -1.466076571675234e+01,  -1.439896632895319e+01,
            -1.420261678810383e+01,  -1.407171709420425e+01,
            -1.387536755335489e+01,  -1.361356816555574e+01,
            -1.348266847165616e+01,  -1.322086908385701e+01,
            -1.302451954300765e+01,  -1.276272015520852e+01,
            -1.256637061435917e+01,  -1.237002107350983e+01,
            -1.210822168571069e+01,  -1.191187214486134e+01,
            -1.165007275706219e+01,  -1.151917306316261e+01,
            -1.125737367536346e+01,  -1.106102413451409e+01,
            -1.093012444061452e+01,  -1.073377489976516e+01,
            -1.047197551196600e+01,  -1.027562597111664e+01,
            -1.001382658331748e+01,  -9.882926889417908e+00,
            -9.752027195518327e+00,  -9.621127501618748e+00,
            -9.424777960769379e+00,  -9.228428419920011e+00,
            -9.097528726020432e+00,  -8.966629032120853e+00,
            -8.835729338221274e+00,  -8.573929950422119e+00,
            -8.377580409572754e+00,  -8.115781021773603e+00,
            -7.919431480924240e+00,  -7.788531787024667e+00,
            -7.592182246175302e+00,  -7.330382858376149e+00,
            -7.199483164476570e+00,  -6.937683776677421e+00,
            -6.741334235828067e+00,  -6.479534848028933e+00,
            -6.283185307179586e+00,  -6.086835766330241e+00,
            -5.825036378531106e+00,  -5.628686837681752e+00,
            -5.366887449882602e+00,  -5.235987755983024e+00,
            -4.974188368183871e+00,  -4.777838827334508e+00,
            -4.646939133434933e+00,  -4.450589592585569e+00,
            -4.188790204786418e+00,  -3.992440663937054e+00,
            -3.730641276137899e+00,  -3.599741582238321e+00,
            -3.468841888338742e+00,  -3.337942194439161e+00,
            -3.141592653589793e+00,  -2.945243112740425e+00,
            -2.814343418840846e+00,  -2.683443724941266e+00,
            -2.552544031041688e+00,  -2.290744643242533e+00,
            -2.094395102393169e+00,  -1.832595714594018e+00,
            -1.636246173744654e+00,  -1.505346479845080e+00,
            -1.308996938995716e+00,  -1.047197551196562e+00,
            -9.162978572969835e-01,  -6.544984694978351e-01,
            -4.581489286484802e-01,  -1.963495408493467e-01,
            -4.440892098500626e-16,   1.963495408493459e-01,
            4.581489286484804e-01,   6.544984694978342e-01,
            9.162978572969835e-01,   1.047197551196562e+00,
            1.308996938995715e+00,   1.505346479845078e+00,
            1.636246173744653e+00,   1.832595714594017e+00,
            2.094395102393169e+00,   2.290744643242533e+00,
            2.552544031041688e+00,   2.683443724941265e+00,
            2.814343418840845e+00,   2.945243112740425e+00,
            3.141592653589793e+00,   3.337942194439162e+00,
            3.468841888338741e+00,   3.599741582238321e+00,
            3.730641276137898e+00,   3.992440663937053e+00,
            4.188790204786417e+00,   4.450589592585569e+00,
            4.646939133434932e+00,   4.777838827334506e+00,
            4.974188368183871e+00,   5.235987755983023e+00,
            5.366887449882602e+00,   5.628686837681752e+00,
            5.825036378531106e+00,   6.086835766330239e+00,
            6.283185307179586e+00,   6.479534848028932e+00,
            6.741334235828067e+00,   6.937683776677421e+00,
            7.199483164476570e+00,   7.330382858376148e+00,
            7.592182246175302e+00,   7.788531787024664e+00,
            7.919431480924239e+00,   8.115781021773603e+00,
            8.377580409572754e+00,   8.573929950422119e+00,
            8.835729338221274e+00,   8.966629032120851e+00,
            9.097528726020432e+00,   9.228428419920011e+00,
            9.424777960769379e+00,   9.621127501618748e+00,
            9.752027195518327e+00,   9.882926889417906e+00,
            1.001382658331748e+01,   1.027562597111664e+01,
            1.047197551196600e+01,   1.073377489976516e+01,
            1.093012444061452e+01,   1.106102413451409e+01,
            1.125737367536346e+01,   1.151917306316261e+01,
            1.165007275706219e+01,   1.191187214486134e+01,
            1.210822168571069e+01,   1.237002107350983e+01,
            1.256637061435917e+01,   1.276272015520852e+01,
            1.302451954300765e+01,   1.322086908385701e+01,
            1.348266847165616e+01,   1.361356816555573e+01,
            1.387536755335489e+01,   1.407171709420425e+01,
            1.420261678810382e+01,   1.439896632895319e+01,
            1.466076571675234e+01,   1.485711525760171e+01,
            1.511891464540086e+01,   1.524981433930044e+01,
            1.538071403320002e+01,   1.551161372709960e+01,
            1.570796326794897e+01,   1.590431280879833e+01,
            1.603521250269791e+01,   1.616611219659749e+01,
            1.629701189049707e+01,   1.655881127829623e+01,
            1.675516081914559e+01,   1.701696020694474e+01,
            1.721330974779411e+01,   1.734420944169368e+01,
            1.754055898254304e+01,   1.780235837034220e+01,
            1.793325806424178e+01,   1.819505745204092e+01,
            1.839140699289028e+01,   1.865320638068941e+01,
            1.884955592153876e+01,   1.904590546238811e+01,
            1.930770485018724e+01,   1.950405439103659e+01,
            1.976585377883574e+01,   1.989675347273532e+01,
            2.015855286053447e+01,   2.035490240138384e+01,
            2.048580209528341e+01,   2.068215163613278e+01,
            2.094395102393193e+01,   2.114030056478129e+01,
            2.140209995258045e+01,   2.153299964648003e+01,
            2.166389934037960e+01,   2.179479903427918e+01,
            2.199114857512855e+01])

    def get_phi(self):
        phi_real = self.get_phi_1d_real()
        phi_imag = self.get_phi_1d_imag()
        phi = np.ndarray(phi_real.shape, dtype=complex)
        phi.real = phi_real
        phi.imag = phi_imag
        return phi

    def get_phi_1d_real(self):
        return np.loadtxt('tests/phi-1d-real.txt')

    def get_phi_1d_imag(self):
        return np.loadtxt('tests/phi-1d-imag.txt')

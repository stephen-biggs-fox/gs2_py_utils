"""
Unit tests of CheaseEquilibrium

Copyright 2020 Stephen Biggs-Fox

This file is part of tests.

tests is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tests is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with tests.  If not, see http://www.gnu.org/licenses/.
"""


from CheaseEquilibrium import CheaseEquilibrium
import unittest


class CheaseEquilibriumTest(unittest.TestCase):

    def testInitialiseWithGivenFolder(self):
        inputFolder = 'folder'
        cheaseEquilibrium = CheaseEquilibrium(inputFolder, 'filename')
        self.assertEqual(cheaseEquilibrium.folder, inputFolder)

    def testInitialiseWithGivenFilenameDat(self):
        inputFilename = 'filename.dat'
        cheaseEquilibrium = CheaseEquilibrium('folder', inputFilename)
        self.assertEqual(cheaseEquilibrium.inputFilename, inputFilename)

    @unittest.skip("slow")
    def testLoadFromDatFileScalar(self):
        inputFolder = 'tests/cheaseEquilibrium'
        inputFilename = 'testCheaseEquilibrium.dat'
        cheaseEquilibrium = CheaseEquilibrium(inputFolder, inputFilename)
        cheaseEquilibrium.load()
        self.assertEqual(cheaseEquilibrium.dataset['NPSI'], 181)


if __name__ == "__main__":
    unittest.main()

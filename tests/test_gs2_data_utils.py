"""
Unit tests of gs2_data_utils

Copyright 2018 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import gs2_data_utils as dat


class test_gs2_data_utils(TestCase):

    def test_sort_datafiles_simple(self):
        files = ['./a_3/input.out.nc', './a_1/input.out.nc',
                 './a_5/input.out.nc', './a_4/input.out.nc',
                 './a_2/input.out.nc']
        expect = ['./a_1/input.out.nc', './a_2/input.out.nc',
                  './a_3/input.out.nc', './a_4/input.out.nc',
                  './a_5/input.out.nc']
        actual = dat.sort_datafiles(files)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_simple(self):
        files = ['./a_2/b_1/input.out.nc', './a_1/b_2/input.out.nc',
                 './a_1/b_1/input.out.nc', './a_2/b_2/input.out.nc']
        expect = ['./a_1/b_1/input.out.nc', './a_1/b_2/input.out.nc',
                  './a_2/b_1/input.out.nc', './a_2/b_2/input.out.nc']
        actual = dat.sort_datafiles(files)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_simple_sort_by_second(self):
        files = ['./a_2/b_1/input.out.nc', './a_1/b_2/input.out.nc',
                 './a_1/b_1/input.out.nc', './a_2/b_2/input.out.nc']
        expect = ['./a_1/b_1/input.out.nc', './a_2/b_1/input.out.nc',
                  './a_1/b_2/input.out.nc', './a_2/b_2/input.out.nc']
        actual = dat.sort_datafiles(files, i=2)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_one_level_over_10(self):
        files = ['./a_11/input.out.nc', './a_10/input.out.nc',
                 './a_8/input.out.nc', './a_9/input.out.nc']
        expect = ['./a_8/input.out.nc', './a_9/input.out.nc',
                  './a_10/input.out.nc', './a_11/input.out.nc']
        actual = dat.sort_datafiles(files)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_over_10(self):
        files = ['./a_2/b_9/input.out.nc', './a_1/b_10/input.out.nc',
                 './a_1/b_9/input.out.nc', './a_2/b_10/input.out.nc']
        expect = ['./a_1/b_9/input.out.nc', './a_1/b_10/input.out.nc',
                  './a_2/b_9/input.out.nc', './a_2/b_10/input.out.nc']
        actual = dat.sort_datafiles(files)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_over_10_sort_by_second(self):
        files = ['./a_2/b_9/input.out.nc', './a_1/b_10/input.out.nc',
                 './a_1/b_9/input.out.nc', './a_2/b_10/input.out.nc']
        expect = ['./a_1/b_9/input.out.nc', './a_2/b_9/input.out.nc',
                  './a_1/b_10/input.out.nc',
                  './a_2/b_10/input.out.nc']
        actual = dat.sort_datafiles(files, i=2)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_simple_no_dot(self):
        files = ['a_3/input.out.nc', 'a_1/input.out.nc',
                 'a_5/input.out.nc', 'a_4/input.out.nc',
                 'a_2/input.out.nc']
        expect = ['a_1/input.out.nc', 'a_2/input.out.nc',
                  'a_3/input.out.nc', 'a_4/input.out.nc',
                  'a_5/input.out.nc']
        actual = dat.sort_datafiles(files, i=0)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_simple_no_dot(self):
        files = ['a_2/b_1/input.out.nc', 'a_1/b_2/input.out.nc',
                 'a_1/b_1/input.out.nc', 'a_2/b_2/input.out.nc']
        expect = ['a_1/b_1/input.out.nc', 'a_1/b_2/input.out.nc',
                  'a_2/b_1/input.out.nc', 'a_2/b_2/input.out.nc']
        actual = dat.sort_datafiles(files, i=0)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_simple_sort_by_second_no_dot(self):
        files = ['a_2/b_1/input.out.nc', 'a_1/b_2/input.out.nc',
                 'a_1/b_1/input.out.nc', 'a_2/b_2/input.out.nc']
        expect = ['a_1/b_1/input.out.nc', 'a_2/b_1/input.out.nc',
                  'a_1/b_2/input.out.nc', 'a_2/b_2/input.out.nc']
        actual = dat.sort_datafiles(files)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_one_level_over_10_no_dot(self):
        files = ['a_11/input.out.nc', 'a_10/input.out.nc',
                 'a_8/input.out.nc', 'a_9/input.out.nc']
        expect = ['a_8/input.out.nc', 'a_9/input.out.nc',
                  'a_10/input.out.nc', 'a_11/input.out.nc']
        actual = dat.sort_datafiles(files, i=0)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_over_10_no_dot(self):
        files = ['a_2/b_9/input.out.nc', 'a_1/b_10/input.out.nc',
                 'a_1/b_9/input.out.nc', 'a_2/b_10/input.out.nc']
        expect = ['a_1/b_9/input.out.nc', 'a_1/b_10/input.out.nc',
                  'a_2/b_9/input.out.nc', 'a_2/b_10/input.out.nc']
        actual = dat.sort_datafiles(files, i=0)[0]
        self.assertEqual(actual, expect)

    def test_sort_datafiles_two_levels_over_10_sort_by_second_no_dot(self):
        files = ['a_2/b_9/input.out.nc', 'a_1/b_10/input.out.nc',
                 'a_1/b_9/input.out.nc', 'a_2/b_10/input.out.nc']
        expect = ['a_1/b_9/input.out.nc', 'a_2/b_9/input.out.nc',
                  'a_1/b_10/input.out.nc',
                  'a_2/b_10/input.out.nc']
        actual = dat.sort_datafiles(files)[0]
        self.assertEqual(actual, expect)

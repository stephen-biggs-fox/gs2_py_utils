"""
Class to represent a GS2 Dataset, i.e. a run with or without restarts

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import xarray as _xr
from collections.abc import MutableMapping as _MutableMapping

_time = 't'


class Gs2Dataset(_MutableMapping):
    "A GS2 Dataset, i.e. a NetCDF file or multiple restarts."

    def __init__(self, filenames):
        """filenames is a string for a single file or a sequence of strings
        for a run with restarts where each element of the sequence is a
        filename and they are in run order. The strings themselves are
        absolute or relative paths to the NetCDF files."""
        if type(filenames) is str:
            self._dataset = _Gs2SingleFileDataset(filenames)
        else:
            self._dataset = _Gs2MultiFileDataset(filenames)

    def __len__(self):
        return len(self._dataset)

    def __getitem__(self, key):
        return self._dataset[key]

    def __setitem__(self, key, value):
        self._dataset[key] = value

    def __delitem__(self, key):
        del self._dataset[key]

    def keys(self):
        return self._dataset.keys()

    def __contains__(self, item):
        return item in self._dataset

    def __iter__(self):
        return self.iterkeys()

    def iterkeys(self):
        for i in self._dataset:
            yield i


class _Gs2SingleFileDataset(Gs2Dataset):
    "A GS2 Dataset based on a single file"

    def __init__(self, filename):
        self._dataset = _xr.open_dataset(filename)


class _Gs2MultiFileDataset(Gs2Dataset):
    "A GS2 Dataset based on multiple files"

    # TODO - Some of the other methods (__setitem__, etc) might need some
    # special attention as a key might get added or removed from the master
    # dataset but the others in self._datasetList would be unaffected so they
    # then become inconsistent. Leave it for now a just trust people to use
    # this sensibly!

    def __init__(self, filenames):
        self._datasetList = [_xr.open_dataset(f) for f in filenames]
        self._dataset = self._datasetList[-1]  # Final dataset is the master

    def __getitem__(self, key):
        if _time in self._dataset[key].dims:
            item = _xr.concat([ds[key] for ds in self._datasetList], dim=_time)
        else:
            item = self._dataset[key]
        return item

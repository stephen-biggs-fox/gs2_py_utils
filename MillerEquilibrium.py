"""
This class represents Miller fits to each surface of an equilibrium

Copyright 2019 Stephen Biggs-Fox

This file is part of gs2pyutils.

gs2pyutils is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gs2pyutils is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with gs2pyutils.  If not, see http://www.gnu.org/licenses/.
"""

import numpy as np
from scipy import interpolate as sp_interp
import sys
import xarray as xr
from Equilibrium import Equilibrium
import MillerSurface as ms


class MillerEquilibrium(Equilibrium):
    def __init__(self, inputEquilibrium, outputFolder, outputFilename):
        """
        Initialise the Miller equilibrium
        Inputs:
            - inputEquilibrium - an object that is of a class that subclasses
            the Equilibrium class containing an instance variable called
            dataset that is an xarray Dataset representing the equilibrium
            including the necessary variables for a Miller fit (R, Z and Bp)
            - outputFolder - string indicating the directory where the output
            file is to be saved
            - outputFilename - string indicating the filename to use for the
            Miller equilibrium output file
        """
        super().__init__(outputFolder, outputFilename)
        # Start with input dataset
        self.dataset = inputEquilibrium.dataset
        self.referenceMassAmu = inputEquilibrium.referenceMassAmu
        self.referenceChargeNumber = inputEquilibrium.referenceChargeNumber
        self.referenceSurfaceIndex = inputEquilibrium.referenceSurfaceIndex
        self.radialCoordinateName = inputEquilibrium.radialCoordinateName
        self._referenceSpecies = inputEquilibrium._referenceSpecies
        # Extract variables
        for name in self.dataset.variables.keys():
            setattr(self, name, self.dataset[name])
        # Extract dimensions
        self.npsi = int(self.NPSI)
        self.nchi = int(self.NCHI)
        # Update for different definitions
        self._create("bref", force=True)  # Different definition for Miller
        self._create("rhoref", force=True)  # Depends on bref
        self._create("rhostar", force=True)  # Depends on rhoref
        self._create("beta", force=True)  # Depends on bref
        self._create("beta_prime", force=True)  # Depends on beta

    def computeMillerParameters(self):
        """
        Computes the Miller fit parameters for all surfaces in this
        equilibrium. The computed paramters are written back to the NetCDF file
        so they don't have to be re-computed in future.
        """
        self._computeMissingEquilibriumQuantities()
        self._fitMillerParameters()
        self._computeDerivedQuantities()

    def _computeMissingEquilibriumQuantities(self):
        self._compute(
            [
                "A_geom",
                "akappa_geom",
                "tri_geom",
                "dr_Psi_geom",
                "sk_geom",
                "sd_geom",
                "dr_R0_geom",
                "thetaMiller",
                "R_norm",
                "Z_norm",
                "Bp_norm",
                "dr_Psi_norm_geom",
                "R_geo",
            ]
        )

    def _compute_A_geom(self):
        self._create("R0")
        self._create("r")
        return self.R0 / self.r

    def _compute_R0(self):
        "Compute major radius of each surface centre"
        R0 = self._newDataArrayFunctionOfPsi()
        # Treat surface 0 separately becuase it is the magnetic axis
        R_axis = self.R[dict(PSI=0)]
        assert (R_axis == R_axis[0]).all()
        R0[dict(PSI=0)] = R_axis[0]
        # Loop over other surfaces
        for surfaceIndex in range(1, self.npsi):
            # The following could be in-lined but is kept separate for
            # readability
            R_min_max = self._computeMinMax("R", surfaceIndex)
            R0[dict(PSI=surfaceIndex)] = self._computeMidpoint(*R_min_max)
        return R0

    def _computeMidpoint(self, a, b):
        return np.mean([a, b])

    def _compute_akappa_geom(self):
        self._create("r")
        akappa_geom = xr.DataArray(np.ones(self.npsi), dims="PSI")
        # Start from surface 1 as 0 is axis which has akappa_geom = 1.0 by
        # definition
        for surfaceIndex in range(1, self.npsi):
            # The following could be in-lined but is kept separate for
            # readability
            Z_min_max = self._computeMinMax("Z", surfaceIndex)
            vertical_radius = self._computeHalfDistance(*Z_min_max)
            akappa_geom[dict(PSI=surfaceIndex)] = (
                vertical_radius / self.r[dict(PSI=surfaceIndex)]
            )
        return akappa_geom

    def _compute_tri_geom(self):
        self._create("delta")
        return np.arcsin(self.delta)

    def _compute_delta(self):
        "Compute triangularity of each surface"
        self._create("R0")
        self._create("r")
        delta = self._newDataArrayFunctionOfPsi()
        # Start from surface 1 as 0 is axis which has delta = 0.0 by definition
        for surfaceIndex in range(1, self.npsi):
            # Triangualrity is more involved...
            # First find the chi of the extrema of Z
            # k=4 so derivative is k=3 so we can find roots of derivative
            splineOfZ = sp_interp.InterpolatedUnivariateSpline(
                self.CHI, self.Z[dict(PSI=surfaceIndex)], k=4
            )
            chiMinMaxZ = splineOfZ.derivative().roots()
            assert len(chiMinMaxZ) == 2
            # Next, pick the one corresponding to the maximum
            if splineOfZ(chiMinMaxZ[0]) > splineOfZ(chiMinMaxZ[1]):
                chiMaxZ = chiMinMaxZ[0]
            else:
                chiMaxZ = chiMinMaxZ[1]
            # Then use this with a spline of R to get the R value at this point
            splineOfR = sp_interp.InterpolatedUnivariateSpline(
                self.CHI, self.R[dict(PSI=surfaceIndex)]
            )
            R_max_Z = splineOfR(chiMaxZ)
            # Finally, compute delta
            delta[dict(PSI=surfaceIndex)] = (
                self.R0[dict(PSI=surfaceIndex)] - R_max_Z
            ) / self.r[dict(PSI=surfaceIndex)]
        return delta

    # For the following gradient functions, use np.gradient rather than
    # xr.differentiate becuase derivative is w.r.t. r, which is not a
    # coordinate. If multiplied by a DataArray, this is returned as a DataArray
    # (e.g. sk_geom) but if not then this would be returned as a numpy array so
    # wrap in a DataArray constructor (e.g. dr_Psi_geom)

    def _compute_sk_geom(self):
        self._create("akappa_geom")
        self._create("r")
        return (self.r / self.akappa_geom) * np.gradient(
            self.akappa_geom, self.r, edge_order=2
        )

    def _compute_sd_geom(self):
        self._create("r")
        self._create("delta")
        return (
            self.r
            * np.gradient(self.delta, self.r, edge_order=2)
            / np.sqrt(1 - self.delta ** 2)
        )

    def _compute_dr_Psi_geom(self):
        self._create("r")
        return xr.DataArray(np.gradient(self.PSI, self.r, edge_order=2), dims="PSI")

    def _compute_dr_R0_geom(self):
        self._create("R0")
        self._create("r")
        return xr.DataArray(np.gradient(self.R0, self.r, edge_order=2), dims="PSI")

    def _compute_thetaMiller(self):
        self._create("akappa_geom")
        self._create("r")
        thetaMiller = xr.DataArray(np.zeros(self.B.shape), dims=self.B.dims)
        # Treat surface 0 separately becuase it is the magnetic axis
        thetaMiller[dict(PSI=0)] = self.CHI
        # Loop over other surfaces
        for surfaceIndex in range(1, self.npsi):
            # Clip to -1 to 1 range as this becomes the input to arcsin which
            # is only valid for that range. This is an OK thing to do because
            # most of the values are in that range and the few that aren't are
            # only just outside it so experience a minor correction to the
            # edge of the range.
            Z_over_kappa_r = (
                self.Z[dict(PSI=surfaceIndex)]
                / (
                    self.akappa_geom[dict(PSI=surfaceIndex)]
                    * self.r[dict(PSI=surfaceIndex)]
                )
            ).clip(-1, 1)
            # Compute initial thetaMiller - only initial becase the output of
            # arcsin is restricted to the range -pi/2 to +pi/2 but thetaMiller
            # needs to be in the range 0 to 2 pi
            # NB: np.arcsin(DataArray) returns a DataArray
            thetaMillerInitial = np.arcsin(Z_over_kappa_r)
            # Get indexes of key points. The int(...) wrapper is so that we
            # have integer objects, not DataArrays. We can't use DataArrays
            # because they cannot be interpreted as integers for array indexing
            # purposes. We could use *.values for this but then we get numpy
            # arrays with shape = () and having integers is nicer.
            imax = int(np.argmax(thetaMillerInitial))
            imin = int(np.argmin(thetaMillerInitial))
            i0 = int((np.argmin(np.abs(thetaMillerInitial[imax:imin])) + imax))
            # Modify each section accordingly and re-combine
            thetaMillerFix = np.concatenate(
                (
                    thetaMillerInitial[:imax],
                    -thetaMillerInitial[imax:i0] + np.pi,
                    -thetaMillerInitial[i0:imin] + np.pi,
                    thetaMillerInitial[imin:] + 2 * np.pi,
                )
            )
            # Smooth the above near the key points using a spline
            thetaSpline = sp_interp.UnivariateSpline(self.CHI, thetaMillerFix)
            thetaMiller[dict(PSI=surfaceIndex)] = thetaSpline(self.CHI)
        return thetaMiller

    def _compute_bref(self):
        "Compute the reference magnetic field in Tesla"
        self._create("Bt")
        Bt_axis = self.Bt[dict(PSI=0)]
        B_ref = Bt_axis[dict(CHI=0)]
        assert (Bt_axis == B_ref).all()
        return B_ref

    def _compute_dr_Psi_norm_geom(self):
        self._create("dr_Psi_geom")
        self._create("bref")
        self._create("lref")
        return self.dr_Psi_geom / (self.bref * self.lref)

    def _compute_R_norm(self):
        self._create("lref")
        return self.R / self.lref

    def _compute_Z_norm(self):
        self._create("lref")
        return self.Z / self.lref

    def _compute_Bp_norm(self):
        self._create("Bp")
        self._create("bref")
        return self.Bp / self.bref

    def _compute_R_geo(self):
        """Compute major radius of reference magnetic field location on each
        surface"""
        self._create("bref")
        self._create("Bt")
        self._create("lref")
        R_geo = self._newDataArrayFunctionOfPsi()
        # Only use the upper half of the surface so that Bbt(chi) is single
        # valued
        chi_upper = self.CHI.where(self.CHI < np.pi, drop=True)
        # Treat surface 0 separately becuase it is the magnetic axis
        R_geo[dict(PSI=0)] = self.R0[dict(PSI=0)]
        # Loop over other surfaces
        for surfaceIndex in range(1, self.npsi):
            # Extract Bt on the surface
            Bt_surface = self.Bt[dict(PSI=surfaceIndex)]
            # Only use the upper half so that it is single valued
            Bt_surface_upper = Bt_surface.where(Bt_surface.CHI < np.pi, drop=True)
            # Use a spline to find the chi where Bt_surface == bref
            chi_as_fn_of_Bt = sp_interp.InterpolatedUnivariateSpline(
                Bt_surface_upper, chi_upper
            )
            chi_geo = chi_as_fn_of_Bt(self.bref)
            # Spline R(chi) to get R_geo = R(chi_geo)
            R_as_fn_of_chi = sp_interp.InterpolatedUnivariateSpline(
                self.CHI, self.R[dict(PSI=surfaceIndex)]
            )
            R_geo[dict(PSI=surfaceIndex)] = R_as_fn_of_chi(chi_geo)
        return R_geo / self.lref  # Don't forget to normalise!

    def _fitMillerParameters(self):
        # Make sure we have thetaMiller
        self._create("thetaMiller")
        # Pre-allocate fit parameter output DataArrays
        fitParameters = []
        # `ms.paremeterNames` comes from `import MillerSurface as ms`
        for parameterIndex, parameterName in enumerate(ms.parameterNames):
            self._create(parameterName + "_geom")
            fitParameters.append(self._newDataArrayFunctionOfPsi())
            fitParameters[parameterIndex].name = parameterName
        # Re-define akappa as ones instead of zeros so that values on axis are
        # correct. Necessary as  we are not fitting the axis (impossible?).
        fitParameters[ms.parameterNames.index("akappa")] = xr.DataArray(
            np.ones(self.npsi), dims="PSI", name="akappa"
        )
        # Loop over other surfaces
        print("Fitting surfaces up to {}:".format(self.npsi), end=" ")
        for surfaceIndex in range(1, self.npsi):
            print("{}".format(surfaceIndex), end=" ")
            # Flush immediately so that the above print functions as a progress
            # monitor
            sys.stdout.flush()
            initialGuesses = [
                self.dataset[parameterName + "_geom"][dict(PSI=surfaceIndex)]
                for parameterName in ms.parameterNames
            ]
            # Extra division by rhoc to match Miller method of eliminating r
            # and thus only fitting for A, not R0 and r.
            millerSurface = ms.MillerSurface(
                self.thetaMiller[dict(PSI=surfaceIndex)],
                (self.R_norm / self.rhoc)[dict(PSI=surfaceIndex)],
                (self.Z_norm / self.rhoc)[dict(PSI=surfaceIndex)],
                self.Bp_norm[dict(PSI=surfaceIndex)],
                initialGuesses,
            )
            surfaceFitParameters, isOk = millerSurface.computeMillerParameters()
            if not isOk:
                print(
                    "WARNING: Fit failed for surface "
                    + str(surfaceIndex)
                    + ". Using geometrical parameters instead."
                )
            for parameterIndex in range(len(ms.parameterNames)):
                fitParameters[parameterIndex][
                    dict(PSI=surfaceIndex)
                ] = surfaceFitParameters[parameterIndex]
        print("")  # New line after loop printing without newline
        for parameter in fitParameters:
            self._addToDataset(parameter)
        # Just save once at the end because all the computation is done in one
        # go in the `for surfaceIndex` loop. This `for parameter` loop at the
        # end should be very fast as it just adds the parameters to the
        # MillerEquilibrium Dataset and as instance variables.
        self._save()

    def _computeDerivedQuantities(self):
        self._compute(["Rmaj", "akappri", "tripri"])

    def _compute_Rmaj(self):
        self._create("A")
        self._create("r_norm")
        return self.A * self.r_norm

    def _compute_akappri(self):
        self._create("sk")
        self._create("akappa")
        self._create("rhoc")
        return self.sk * self.akappa / self.rhoc

    def _compute_tripri(self):
        self._create("sd")
        self._create("rhoc")
        return self.sd / self.rhoc
